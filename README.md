# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

My implementation of RESTful Web Services for the sample database "Wide World Importers" using:

- Wildfly
- RESTEasy
- JPA
- SQL Server

![picture](http://tsql.solidq.com/SampleDatabases/Diagrams/Sales.jpg)