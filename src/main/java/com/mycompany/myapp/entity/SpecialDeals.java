/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "SpecialDeals", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SpecialDeals.findAll", query = "SELECT s FROM SpecialDeals s")
    , @NamedQuery(name = "SpecialDeals.findBySpecialDealID", query = "SELECT s FROM SpecialDeals s WHERE s.specialDealID = :specialDealID")
    , @NamedQuery(name = "SpecialDeals.findByStockItemID", query = "SELECT s FROM SpecialDeals s WHERE s.stockItemID = :stockItemID")
    , @NamedQuery(name = "SpecialDeals.findByStockGroupID", query = "SELECT s FROM SpecialDeals s WHERE s.stockGroupID = :stockGroupID")
    , @NamedQuery(name = "SpecialDeals.findByDealDescription", query = "SELECT s FROM SpecialDeals s WHERE s.dealDescription = :dealDescription")
    , @NamedQuery(name = "SpecialDeals.findByStartDate", query = "SELECT s FROM SpecialDeals s WHERE s.startDate = :startDate")
    , @NamedQuery(name = "SpecialDeals.findByEndDate", query = "SELECT s FROM SpecialDeals s WHERE s.endDate = :endDate")
    , @NamedQuery(name = "SpecialDeals.findByDiscountAmount", query = "SELECT s FROM SpecialDeals s WHERE s.discountAmount = :discountAmount")
    , @NamedQuery(name = "SpecialDeals.findByDiscountPercentage", query = "SELECT s FROM SpecialDeals s WHERE s.discountPercentage = :discountPercentage")
    , @NamedQuery(name = "SpecialDeals.findByUnitPrice", query = "SELECT s FROM SpecialDeals s WHERE s.unitPrice = :unitPrice")
    , @NamedQuery(name = "SpecialDeals.findByLastEditedBy", query = "SELECT s FROM SpecialDeals s WHERE s.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "SpecialDeals.findByLastEditedWhen", query = "SELECT s FROM SpecialDeals s WHERE s.lastEditedWhen = :lastEditedWhen")})
public class SpecialDeals implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SpecialDealID")
    private Integer specialDealID;
    @Column(name = "StockItemID")
    private Integer stockItemID;
    @Column(name = "StockGroupID")
    private Integer stockGroupID;
    @Basic(optional = false)
    @Column(name = "DealDescription")
    private String dealDescription;
    @Basic(optional = false)
    @Column(name = "StartDate")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @Column(name = "EndDate")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DiscountAmount")
    private BigDecimal discountAmount;
    @Column(name = "DiscountPercentage")
    private BigDecimal discountPercentage;
    @Column(name = "UnitPrice")
    private BigDecimal unitPrice;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastEditedWhen;
    @JoinColumn(name = "BuyingGroupID", referencedColumnName = "BuyingGroupID")
    @ManyToOne
    private BuyingGroups buyingGroupID;
    @JoinColumn(name = "CustomerCategoryID", referencedColumnName = "CustomerCategoryID")
    @ManyToOne
    private CustomerCategories customerCategoryID;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID")
    @ManyToOne
    private Customers customerID;

    public SpecialDeals() {
    }

    public SpecialDeals(Integer specialDealID) {
        this.specialDealID = specialDealID;
    }

    public SpecialDeals(Integer specialDealID, String dealDescription, Date startDate, Date endDate, int lastEditedBy, Date lastEditedWhen) {
        this.specialDealID = specialDealID;
        this.dealDescription = dealDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

    public Integer getSpecialDealID() {
        return specialDealID;
    }

    public void setSpecialDealID(Integer specialDealID) {
        this.specialDealID = specialDealID;
    }

    public Integer getStockItemID() {
        return stockItemID;
    }

    public void setStockItemID(Integer stockItemID) {
        this.stockItemID = stockItemID;
    }

    public Integer getStockGroupID() {
        return stockGroupID;
    }

    public void setStockGroupID(Integer stockGroupID) {
        this.stockGroupID = stockGroupID;
    }

    public String getDealDescription() {
        return dealDescription;
    }

    public void setDealDescription(String dealDescription) {
        this.dealDescription = dealDescription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getLastEditedWhen() {
        return lastEditedWhen;
    }

    public void setLastEditedWhen(Date lastEditedWhen) {
        this.lastEditedWhen = lastEditedWhen;
    }

    public BuyingGroups getBuyingGroupID() {
        return buyingGroupID;
    }

    public void setBuyingGroupID(BuyingGroups buyingGroupID) {
        this.buyingGroupID = buyingGroupID;
    }

    public CustomerCategories getCustomerCategoryID() {
        return customerCategoryID;
    }

    public void setCustomerCategoryID(CustomerCategories customerCategoryID) {
        this.customerCategoryID = customerCategoryID;
    }

    public Customers getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customers customerID) {
        this.customerID = customerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (specialDealID != null ? specialDealID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SpecialDeals)) {
            return false;
        }
        SpecialDeals other = (SpecialDeals) object;
        if ((this.specialDealID == null && other.specialDealID != null) || (this.specialDealID != null && !this.specialDealID.equals(other.specialDealID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.SpecialDeals[ specialDealID=" + specialDealID + " ]";
    }
    
}
