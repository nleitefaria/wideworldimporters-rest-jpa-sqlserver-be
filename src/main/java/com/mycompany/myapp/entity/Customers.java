/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "Customers", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customers.findAll", query = "SELECT c FROM Customers c")
    , @NamedQuery(name = "Customers.findByCustomerID", query = "SELECT c FROM Customers c WHERE c.customerID = :customerID")
    , @NamedQuery(name = "Customers.findByCustomerName", query = "SELECT c FROM Customers c WHERE c.customerName = :customerName")
    , @NamedQuery(name = "Customers.findByPrimaryContactPersonID", query = "SELECT c FROM Customers c WHERE c.primaryContactPersonID = :primaryContactPersonID")
    , @NamedQuery(name = "Customers.findByAlternateContactPersonID", query = "SELECT c FROM Customers c WHERE c.alternateContactPersonID = :alternateContactPersonID")
    , @NamedQuery(name = "Customers.findByDeliveryMethodID", query = "SELECT c FROM Customers c WHERE c.deliveryMethodID = :deliveryMethodID")
    , @NamedQuery(name = "Customers.findByDeliveryCityID", query = "SELECT c FROM Customers c WHERE c.deliveryCityID = :deliveryCityID")
    , @NamedQuery(name = "Customers.findByPostalCityID", query = "SELECT c FROM Customers c WHERE c.postalCityID = :postalCityID")
    , @NamedQuery(name = "Customers.findByCreditLimit", query = "SELECT c FROM Customers c WHERE c.creditLimit = :creditLimit")
    , @NamedQuery(name = "Customers.findByAccountOpenedDate", query = "SELECT c FROM Customers c WHERE c.accountOpenedDate = :accountOpenedDate")
    , @NamedQuery(name = "Customers.findByStandardDiscountPercentage", query = "SELECT c FROM Customers c WHERE c.standardDiscountPercentage = :standardDiscountPercentage")
    , @NamedQuery(name = "Customers.findByIsStatementSent", query = "SELECT c FROM Customers c WHERE c.isStatementSent = :isStatementSent")
    , @NamedQuery(name = "Customers.findByIsOnCreditHold", query = "SELECT c FROM Customers c WHERE c.isOnCreditHold = :isOnCreditHold")
    , @NamedQuery(name = "Customers.findByPaymentDays", query = "SELECT c FROM Customers c WHERE c.paymentDays = :paymentDays")
    , @NamedQuery(name = "Customers.findByPhoneNumber", query = "SELECT c FROM Customers c WHERE c.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "Customers.findByFaxNumber", query = "SELECT c FROM Customers c WHERE c.faxNumber = :faxNumber")
    , @NamedQuery(name = "Customers.findByDeliveryRun", query = "SELECT c FROM Customers c WHERE c.deliveryRun = :deliveryRun")
    , @NamedQuery(name = "Customers.findByRunPosition", query = "SELECT c FROM Customers c WHERE c.runPosition = :runPosition")
    , @NamedQuery(name = "Customers.findByWebsiteURL", query = "SELECT c FROM Customers c WHERE c.websiteURL = :websiteURL")
    , @NamedQuery(name = "Customers.findByDeliveryAddressLine1", query = "SELECT c FROM Customers c WHERE c.deliveryAddressLine1 = :deliveryAddressLine1")
    , @NamedQuery(name = "Customers.findByDeliveryAddressLine2", query = "SELECT c FROM Customers c WHERE c.deliveryAddressLine2 = :deliveryAddressLine2")
    , @NamedQuery(name = "Customers.findByDeliveryPostalCode", query = "SELECT c FROM Customers c WHERE c.deliveryPostalCode = :deliveryPostalCode")
    , @NamedQuery(name = "Customers.findByPostalAddressLine1", query = "SELECT c FROM Customers c WHERE c.postalAddressLine1 = :postalAddressLine1")
    , @NamedQuery(name = "Customers.findByPostalAddressLine2", query = "SELECT c FROM Customers c WHERE c.postalAddressLine2 = :postalAddressLine2")
    , @NamedQuery(name = "Customers.findByPostalPostalCode", query = "SELECT c FROM Customers c WHERE c.postalPostalCode = :postalPostalCode")
    , @NamedQuery(name = "Customers.findByLastEditedBy", query = "SELECT c FROM Customers c WHERE c.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "Customers.findByValidFrom", query = "SELECT c FROM Customers c WHERE c.validFrom = :validFrom")
    , @NamedQuery(name = "Customers.findByValidTo", query = "SELECT c FROM Customers c WHERE c.validTo = :validTo")})
public class Customers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CustomerID")
    private Integer customerID;
    @Basic(optional = false)
    @Column(name = "CustomerName")
    private String customerName;
    @Basic(optional = false)
    @Column(name = "PrimaryContactPersonID")
    private int primaryContactPersonID;
    @Column(name = "AlternateContactPersonID")
    private Integer alternateContactPersonID;
    @Basic(optional = false)
    @Column(name = "DeliveryMethodID")
    private int deliveryMethodID;
    @Basic(optional = false)
    @Column(name = "DeliveryCityID")
    private int deliveryCityID;
    @Basic(optional = false)
    @Column(name = "PostalCityID")
    private int postalCityID;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CreditLimit")
    private BigDecimal creditLimit;
    @Basic(optional = false)
    @Column(name = "AccountOpenedDate")
    @Temporal(TemporalType.DATE)
    private Date accountOpenedDate;
    @Basic(optional = false)
    @Column(name = "StandardDiscountPercentage")
    private BigDecimal standardDiscountPercentage;
    @Basic(optional = false)
    @Column(name = "IsStatementSent")
    private boolean isStatementSent;
    @Basic(optional = false)
    @Column(name = "IsOnCreditHold")
    private boolean isOnCreditHold;
    @Basic(optional = false)
    @Column(name = "PaymentDays")
    private int paymentDays;
    @Basic(optional = false)
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @Basic(optional = false)
    @Column(name = "FaxNumber")
    private String faxNumber;
    @Column(name = "DeliveryRun")
    private String deliveryRun;
    @Column(name = "RunPosition")
    private String runPosition;
    @Basic(optional = false)
    @Column(name = "WebsiteURL")
    private String websiteURL;
    @Basic(optional = false)
    @Column(name = "DeliveryAddressLine1")
    private String deliveryAddressLine1;
    @Column(name = "DeliveryAddressLine2")
    private String deliveryAddressLine2;
    @Basic(optional = false)
    @Column(name = "DeliveryPostalCode")
    private String deliveryPostalCode;
    @Column(name = "DeliveryLocation")
    private byte[] deliveryLocation;
    @Basic(optional = false)
    @Column(name = "PostalAddressLine1")
    private String postalAddressLine1;
    @Column(name = "PostalAddressLine2")
    private String postalAddressLine2;
    @Basic(optional = false)
    @Column(name = "PostalPostalCode")
    private String postalPostalCode;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "ValidFrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Basic(optional = false)
    @Column(name = "ValidTo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerID")
    private List<Orders> ordersList;
    @JoinColumn(name = "BuyingGroupID", referencedColumnName = "BuyingGroupID")
    @ManyToOne
    private BuyingGroups buyingGroupID;
    @JoinColumn(name = "CustomerCategoryID", referencedColumnName = "CustomerCategoryID")
    @ManyToOne(optional = false)
    private CustomerCategories customerCategoryID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "billToCustomerID")
    private List<Customers> customersList;
    @JoinColumn(name = "BillToCustomerID", referencedColumnName = "CustomerID")
    @ManyToOne(optional = false)
    private Customers billToCustomerID;
    @OneToMany(mappedBy = "customerID")
    private List<SpecialDeals> specialDealsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerID")
    private List<CustomerTransactions> customerTransactionsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerID")
    private List<Invoices> invoicesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "billToCustomerID")
    private List<Invoices> invoicesList1;

    public Customers() {
    }

    public Customers(Integer customerID) {
        this.customerID = customerID;
    }

    public Customers(Integer customerID, String customerName, int primaryContactPersonID, int deliveryMethodID, int deliveryCityID, int postalCityID, Date accountOpenedDate, BigDecimal standardDiscountPercentage, boolean isStatementSent, boolean isOnCreditHold, int paymentDays, String phoneNumber, String faxNumber, String websiteURL, String deliveryAddressLine1, String deliveryPostalCode, String postalAddressLine1, String postalPostalCode, int lastEditedBy, Date validFrom, Date validTo) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.primaryContactPersonID = primaryContactPersonID;
        this.deliveryMethodID = deliveryMethodID;
        this.deliveryCityID = deliveryCityID;
        this.postalCityID = postalCityID;
        this.accountOpenedDate = accountOpenedDate;
        this.standardDiscountPercentage = standardDiscountPercentage;
        this.isStatementSent = isStatementSent;
        this.isOnCreditHold = isOnCreditHold;
        this.paymentDays = paymentDays;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.websiteURL = websiteURL;
        this.deliveryAddressLine1 = deliveryAddressLine1;
        this.deliveryPostalCode = deliveryPostalCode;
        this.postalAddressLine1 = postalAddressLine1;
        this.postalPostalCode = postalPostalCode;
        this.lastEditedBy = lastEditedBy;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getPrimaryContactPersonID() {
        return primaryContactPersonID;
    }

    public void setPrimaryContactPersonID(int primaryContactPersonID) {
        this.primaryContactPersonID = primaryContactPersonID;
    }

    public Integer getAlternateContactPersonID() {
        return alternateContactPersonID;
    }

    public void setAlternateContactPersonID(Integer alternateContactPersonID) {
        this.alternateContactPersonID = alternateContactPersonID;
    }

    public int getDeliveryMethodID() {
        return deliveryMethodID;
    }

    public void setDeliveryMethodID(int deliveryMethodID) {
        this.deliveryMethodID = deliveryMethodID;
    }

    public int getDeliveryCityID() {
        return deliveryCityID;
    }

    public void setDeliveryCityID(int deliveryCityID) {
        this.deliveryCityID = deliveryCityID;
    }

    public int getPostalCityID() {
        return postalCityID;
    }

    public void setPostalCityID(int postalCityID) {
        this.postalCityID = postalCityID;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Date getAccountOpenedDate() {
        return accountOpenedDate;
    }

    public void setAccountOpenedDate(Date accountOpenedDate) {
        this.accountOpenedDate = accountOpenedDate;
    }

    public BigDecimal getStandardDiscountPercentage() {
        return standardDiscountPercentage;
    }

    public void setStandardDiscountPercentage(BigDecimal standardDiscountPercentage) {
        this.standardDiscountPercentage = standardDiscountPercentage;
    }

    public boolean getIsStatementSent() {
        return isStatementSent;
    }

    public void setIsStatementSent(boolean isStatementSent) {
        this.isStatementSent = isStatementSent;
    }

    public boolean getIsOnCreditHold() {
        return isOnCreditHold;
    }

    public void setIsOnCreditHold(boolean isOnCreditHold) {
        this.isOnCreditHold = isOnCreditHold;
    }

    public int getPaymentDays() {
        return paymentDays;
    }

    public void setPaymentDays(int paymentDays) {
        this.paymentDays = paymentDays;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getDeliveryRun() {
        return deliveryRun;
    }

    public void setDeliveryRun(String deliveryRun) {
        this.deliveryRun = deliveryRun;
    }

    public String getRunPosition() {
        return runPosition;
    }

    public void setRunPosition(String runPosition) {
        this.runPosition = runPosition;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getDeliveryAddressLine1() {
        return deliveryAddressLine1;
    }

    public void setDeliveryAddressLine1(String deliveryAddressLine1) {
        this.deliveryAddressLine1 = deliveryAddressLine1;
    }

    public String getDeliveryAddressLine2() {
        return deliveryAddressLine2;
    }

    public void setDeliveryAddressLine2(String deliveryAddressLine2) {
        this.deliveryAddressLine2 = deliveryAddressLine2;
    }

    public String getDeliveryPostalCode() {
        return deliveryPostalCode;
    }

    public void setDeliveryPostalCode(String deliveryPostalCode) {
        this.deliveryPostalCode = deliveryPostalCode;
    }

    public byte[] getDeliveryLocation() {
        return deliveryLocation;
    }

    public void setDeliveryLocation(byte[] deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    public String getPostalAddressLine1() {
        return postalAddressLine1;
    }

    public void setPostalAddressLine1(String postalAddressLine1) {
        this.postalAddressLine1 = postalAddressLine1;
    }

    public String getPostalAddressLine2() {
        return postalAddressLine2;
    }

    public void setPostalAddressLine2(String postalAddressLine2) {
        this.postalAddressLine2 = postalAddressLine2;
    }

    public String getPostalPostalCode() {
        return postalPostalCode;
    }

    public void setPostalPostalCode(String postalPostalCode) {
        this.postalPostalCode = postalPostalCode;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    @XmlTransient
    public List<Orders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    public BuyingGroups getBuyingGroupID() {
        return buyingGroupID;
    }

    public void setBuyingGroupID(BuyingGroups buyingGroupID) {
        this.buyingGroupID = buyingGroupID;
    }

    public CustomerCategories getCustomerCategoryID() {
        return customerCategoryID;
    }

    public void setCustomerCategoryID(CustomerCategories customerCategoryID) {
        this.customerCategoryID = customerCategoryID;
    }

    @XmlTransient
    public List<Customers> getCustomersList() {
        return customersList;
    }

    public void setCustomersList(List<Customers> customersList) {
        this.customersList = customersList;
    }

    public Customers getBillToCustomerID() {
        return billToCustomerID;
    }

    public void setBillToCustomerID(Customers billToCustomerID) {
        this.billToCustomerID = billToCustomerID;
    }

    @XmlTransient
    public List<SpecialDeals> getSpecialDealsList() {
        return specialDealsList;
    }

    public void setSpecialDealsList(List<SpecialDeals> specialDealsList) {
        this.specialDealsList = specialDealsList;
    }

    @XmlTransient
    public List<CustomerTransactions> getCustomerTransactionsList() {
        return customerTransactionsList;
    }

    public void setCustomerTransactionsList(List<CustomerTransactions> customerTransactionsList) {
        this.customerTransactionsList = customerTransactionsList;
    }

    @XmlTransient
    public List<Invoices> getInvoicesList() {
        return invoicesList;
    }

    public void setInvoicesList(List<Invoices> invoicesList) {
        this.invoicesList = invoicesList;
    }

    @XmlTransient
    public List<Invoices> getInvoicesList1() {
        return invoicesList1;
    }

    public void setInvoicesList1(List<Invoices> invoicesList1) {
        this.invoicesList1 = invoicesList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerID != null ? customerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customers)) {
            return false;
        }
        Customers other = (Customers) object;
        if ((this.customerID == null && other.customerID != null) || (this.customerID != null && !this.customerID.equals(other.customerID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.Customers[ customerID=" + customerID + " ]";
    }
    
}
