/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "Orders", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o")
    , @NamedQuery(name = "Orders.findByOrderID", query = "SELECT o FROM Orders o WHERE o.orderID = :orderID")
    , @NamedQuery(name = "Orders.findBySalespersonPersonID", query = "SELECT o FROM Orders o WHERE o.salespersonPersonID = :salespersonPersonID")
    , @NamedQuery(name = "Orders.findByPickedByPersonID", query = "SELECT o FROM Orders o WHERE o.pickedByPersonID = :pickedByPersonID")
    , @NamedQuery(name = "Orders.findByContactPersonID", query = "SELECT o FROM Orders o WHERE o.contactPersonID = :contactPersonID")
    , @NamedQuery(name = "Orders.findByOrderDate", query = "SELECT o FROM Orders o WHERE o.orderDate = :orderDate")
    , @NamedQuery(name = "Orders.findByExpectedDeliveryDate", query = "SELECT o FROM Orders o WHERE o.expectedDeliveryDate = :expectedDeliveryDate")
    , @NamedQuery(name = "Orders.findByCustomerPurchaseOrderNumber", query = "SELECT o FROM Orders o WHERE o.customerPurchaseOrderNumber = :customerPurchaseOrderNumber")
    , @NamedQuery(name = "Orders.findByIsUndersupplyBackordered", query = "SELECT o FROM Orders o WHERE o.isUndersupplyBackordered = :isUndersupplyBackordered")
    , @NamedQuery(name = "Orders.findByComments", query = "SELECT o FROM Orders o WHERE o.comments = :comments")
    , @NamedQuery(name = "Orders.findByDeliveryInstructions", query = "SELECT o FROM Orders o WHERE o.deliveryInstructions = :deliveryInstructions")
    , @NamedQuery(name = "Orders.findByInternalComments", query = "SELECT o FROM Orders o WHERE o.internalComments = :internalComments")
    , @NamedQuery(name = "Orders.findByPickingCompletedWhen", query = "SELECT o FROM Orders o WHERE o.pickingCompletedWhen = :pickingCompletedWhen")
    , @NamedQuery(name = "Orders.findByLastEditedBy", query = "SELECT o FROM Orders o WHERE o.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "Orders.findByLastEditedWhen", query = "SELECT o FROM Orders o WHERE o.lastEditedWhen = :lastEditedWhen")})
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "OrderID")
    private Integer orderID;
    @Basic(optional = false)
    @Column(name = "SalespersonPersonID")
    private int salespersonPersonID;
    @Column(name = "PickedByPersonID")
    private Integer pickedByPersonID;
    @Basic(optional = false)
    @Column(name = "ContactPersonID")
    private int contactPersonID;
    @Basic(optional = false)
    @Column(name = "OrderDate")
    @Temporal(TemporalType.DATE)
    private Date orderDate;
    @Basic(optional = false)
    @Column(name = "ExpectedDeliveryDate")
    @Temporal(TemporalType.DATE)
    private Date expectedDeliveryDate;
    @Column(name = "CustomerPurchaseOrderNumber")
    private String customerPurchaseOrderNumber;
    @Basic(optional = false)
    @Column(name = "IsUndersupplyBackordered")
    private boolean isUndersupplyBackordered;
    @Column(name = "Comments")
    private String comments;
    @Column(name = "DeliveryInstructions")
    private String deliveryInstructions;
    @Column(name = "InternalComments")
    private String internalComments;
    @Column(name = "PickingCompletedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickingCompletedWhen;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastEditedWhen;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID")
    @ManyToOne(optional = false)
    private Customers customerID;
    @OneToMany(mappedBy = "backorderOrderID")
    private List<Orders> ordersList;
    @JoinColumn(name = "BackorderOrderID", referencedColumnName = "OrderID")
    @ManyToOne
    private Orders backorderOrderID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderID")
    private List<OrderLines> orderLinesList;
    @OneToMany(mappedBy = "orderID")
    private List<Invoices> invoicesList;

    public Orders() {
    }

    public Orders(Integer orderID) {
        this.orderID = orderID;
    }

    public Orders(Integer orderID, int salespersonPersonID, int contactPersonID, Date orderDate, Date expectedDeliveryDate, boolean isUndersupplyBackordered, int lastEditedBy, Date lastEditedWhen) {
        this.orderID = orderID;
        this.salespersonPersonID = salespersonPersonID;
        this.contactPersonID = contactPersonID;
        this.orderDate = orderDate;
        this.expectedDeliveryDate = expectedDeliveryDate;
        this.isUndersupplyBackordered = isUndersupplyBackordered;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public int getSalespersonPersonID() {
        return salespersonPersonID;
    }

    public void setSalespersonPersonID(int salespersonPersonID) {
        this.salespersonPersonID = salespersonPersonID;
    }

    public Integer getPickedByPersonID() {
        return pickedByPersonID;
    }

    public void setPickedByPersonID(Integer pickedByPersonID) {
        this.pickedByPersonID = pickedByPersonID;
    }

    public int getContactPersonID() {
        return contactPersonID;
    }

    public void setContactPersonID(int contactPersonID) {
        this.contactPersonID = contactPersonID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getCustomerPurchaseOrderNumber() {
        return customerPurchaseOrderNumber;
    }

    public void setCustomerPurchaseOrderNumber(String customerPurchaseOrderNumber) {
        this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
    }

    public boolean getIsUndersupplyBackordered() {
        return isUndersupplyBackordered;
    }

    public void setIsUndersupplyBackordered(boolean isUndersupplyBackordered) {
        this.isUndersupplyBackordered = isUndersupplyBackordered;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDeliveryInstructions() {
        return deliveryInstructions;
    }

    public void setDeliveryInstructions(String deliveryInstructions) {
        this.deliveryInstructions = deliveryInstructions;
    }

    public String getInternalComments() {
        return internalComments;
    }

    public void setInternalComments(String internalComments) {
        this.internalComments = internalComments;
    }

    public Date getPickingCompletedWhen() {
        return pickingCompletedWhen;
    }

    public void setPickingCompletedWhen(Date pickingCompletedWhen) {
        this.pickingCompletedWhen = pickingCompletedWhen;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getLastEditedWhen() {
        return lastEditedWhen;
    }

    public void setLastEditedWhen(Date lastEditedWhen) {
        this.lastEditedWhen = lastEditedWhen;
    }

    public Customers getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customers customerID) {
        this.customerID = customerID;
    }

    @XmlTransient
    public List<Orders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    public Orders getBackorderOrderID() {
        return backorderOrderID;
    }

    public void setBackorderOrderID(Orders backorderOrderID) {
        this.backorderOrderID = backorderOrderID;
    }

    @XmlTransient
    public List<OrderLines> getOrderLinesList() {
        return orderLinesList;
    }

    public void setOrderLinesList(List<OrderLines> orderLinesList) {
        this.orderLinesList = orderLinesList;
    }

    @XmlTransient
    public List<Invoices> getInvoicesList() {
        return invoicesList;
    }

    public void setInvoicesList(List<Invoices> invoicesList) {
        this.invoicesList = invoicesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderID != null ? orderID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.orderID == null && other.orderID != null) || (this.orderID != null && !this.orderID.equals(other.orderID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.Orders[ orderID=" + orderID + " ]";
    }
    
}
