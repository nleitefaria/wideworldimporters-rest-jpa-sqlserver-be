/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "CustomerCategories", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomerCategories.findAll", query = "SELECT c FROM CustomerCategories c")
    , @NamedQuery(name = "CustomerCategories.findByCustomerCategoryID", query = "SELECT c FROM CustomerCategories c WHERE c.customerCategoryID = :customerCategoryID")
    , @NamedQuery(name = "CustomerCategories.findByCustomerCategoryName", query = "SELECT c FROM CustomerCategories c WHERE c.customerCategoryName = :customerCategoryName")
    , @NamedQuery(name = "CustomerCategories.findByLastEditedBy", query = "SELECT c FROM CustomerCategories c WHERE c.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "CustomerCategories.findByValidFrom", query = "SELECT c FROM CustomerCategories c WHERE c.validFrom = :validFrom")
    , @NamedQuery(name = "CustomerCategories.findByValidTo", query = "SELECT c FROM CustomerCategories c WHERE c.validTo = :validTo")})
public class CustomerCategories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CustomerCategoryID")
    private Integer customerCategoryID;
    @Basic(optional = false)
    @Column(name = "CustomerCategoryName")
    private String customerCategoryName;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "ValidFrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Basic(optional = false)
    @Column(name = "ValidTo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerCategoryID")
    private List<Customers> customersList;
    @OneToMany(mappedBy = "customerCategoryID")
    private List<SpecialDeals> specialDealsList;

    public CustomerCategories() {
    }

    public CustomerCategories(Integer customerCategoryID) {
        this.customerCategoryID = customerCategoryID;
    }

    public CustomerCategories(Integer customerCategoryID, String customerCategoryName, int lastEditedBy, Date validFrom, Date validTo) {
        this.customerCategoryID = customerCategoryID;
        this.customerCategoryName = customerCategoryName;
        this.lastEditedBy = lastEditedBy;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

    public Integer getCustomerCategoryID() {
        return customerCategoryID;
    }

    public void setCustomerCategoryID(Integer customerCategoryID) {
        this.customerCategoryID = customerCategoryID;
    }

    public String getCustomerCategoryName() {
        return customerCategoryName;
    }

    public void setCustomerCategoryName(String customerCategoryName) {
        this.customerCategoryName = customerCategoryName;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    @XmlTransient
    public List<Customers> getCustomersList() {
        return customersList;
    }

    public void setCustomersList(List<Customers> customersList) {
        this.customersList = customersList;
    }

    @XmlTransient
    public List<SpecialDeals> getSpecialDealsList() {
        return specialDealsList;
    }

    public void setSpecialDealsList(List<SpecialDeals> specialDealsList) {
        this.specialDealsList = specialDealsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerCategoryID != null ? customerCategoryID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerCategories)) {
            return false;
        }
        CustomerCategories other = (CustomerCategories) object;
        if ((this.customerCategoryID == null && other.customerCategoryID != null) || (this.customerCategoryID != null && !this.customerCategoryID.equals(other.customerCategoryID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.CustomerCategories[ customerCategoryID=" + customerCategoryID + " ]";
    }
    
}
