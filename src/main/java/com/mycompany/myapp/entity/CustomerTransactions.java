/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "CustomerTransactions", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CustomerTransactions.findAll", query = "SELECT c FROM CustomerTransactions c")
    , @NamedQuery(name = "CustomerTransactions.findByCustomerTransactionID", query = "SELECT c FROM CustomerTransactions c WHERE c.customerTransactionID = :customerTransactionID")
    , @NamedQuery(name = "CustomerTransactions.findByTransactionTypeID", query = "SELECT c FROM CustomerTransactions c WHERE c.transactionTypeID = :transactionTypeID")
    , @NamedQuery(name = "CustomerTransactions.findByPaymentMethodID", query = "SELECT c FROM CustomerTransactions c WHERE c.paymentMethodID = :paymentMethodID")
    , @NamedQuery(name = "CustomerTransactions.findByTransactionDate", query = "SELECT c FROM CustomerTransactions c WHERE c.transactionDate = :transactionDate")
    , @NamedQuery(name = "CustomerTransactions.findByAmountExcludingTax", query = "SELECT c FROM CustomerTransactions c WHERE c.amountExcludingTax = :amountExcludingTax")
    , @NamedQuery(name = "CustomerTransactions.findByTaxAmount", query = "SELECT c FROM CustomerTransactions c WHERE c.taxAmount = :taxAmount")
    , @NamedQuery(name = "CustomerTransactions.findByTransactionAmount", query = "SELECT c FROM CustomerTransactions c WHERE c.transactionAmount = :transactionAmount")
    , @NamedQuery(name = "CustomerTransactions.findByOutstandingBalance", query = "SELECT c FROM CustomerTransactions c WHERE c.outstandingBalance = :outstandingBalance")
    , @NamedQuery(name = "CustomerTransactions.findByFinalizationDate", query = "SELECT c FROM CustomerTransactions c WHERE c.finalizationDate = :finalizationDate")
    , @NamedQuery(name = "CustomerTransactions.findByIsFinalized", query = "SELECT c FROM CustomerTransactions c WHERE c.isFinalized = :isFinalized")
    , @NamedQuery(name = "CustomerTransactions.findByLastEditedBy", query = "SELECT c FROM CustomerTransactions c WHERE c.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "CustomerTransactions.findByLastEditedWhen", query = "SELECT c FROM CustomerTransactions c WHERE c.lastEditedWhen = :lastEditedWhen")})
public class CustomerTransactions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CustomerTransactionID")
    private Integer customerTransactionID;
    @Basic(optional = false)
    @Column(name = "TransactionTypeID")
    private int transactionTypeID;
    @Column(name = "PaymentMethodID")
    private Integer paymentMethodID;
    @Basic(optional = false)
    @Column(name = "TransactionDate")
    @Temporal(TemporalType.DATE)
    private Date transactionDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "AmountExcludingTax")
    private BigDecimal amountExcludingTax;
    @Basic(optional = false)
    @Column(name = "TaxAmount")
    private BigDecimal taxAmount;
    @Basic(optional = false)
    @Column(name = "TransactionAmount")
    private BigDecimal transactionAmount;
    @Basic(optional = false)
    @Column(name = "OutstandingBalance")
    private BigDecimal outstandingBalance;
    @Column(name = "FinalizationDate")
    @Temporal(TemporalType.DATE)
    private Date finalizationDate;
    @Column(name = "IsFinalized")
    private Boolean isFinalized;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastEditedWhen;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID")
    @ManyToOne(optional = false)
    private Customers customerID;
    @JoinColumn(name = "InvoiceID", referencedColumnName = "InvoiceID")
    @ManyToOne
    private Invoices invoiceID;

    public CustomerTransactions() {
    }

    public CustomerTransactions(Integer customerTransactionID) {
        this.customerTransactionID = customerTransactionID;
    }

    public CustomerTransactions(Integer customerTransactionID, int transactionTypeID, Date transactionDate, BigDecimal amountExcludingTax, BigDecimal taxAmount, BigDecimal transactionAmount, BigDecimal outstandingBalance, int lastEditedBy, Date lastEditedWhen) {
        this.customerTransactionID = customerTransactionID;
        this.transactionTypeID = transactionTypeID;
        this.transactionDate = transactionDate;
        this.amountExcludingTax = amountExcludingTax;
        this.taxAmount = taxAmount;
        this.transactionAmount = transactionAmount;
        this.outstandingBalance = outstandingBalance;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

    public Integer getCustomerTransactionID() {
        return customerTransactionID;
    }

    public void setCustomerTransactionID(Integer customerTransactionID) {
        this.customerTransactionID = customerTransactionID;
    }

    public int getTransactionTypeID() {
        return transactionTypeID;
    }

    public void setTransactionTypeID(int transactionTypeID) {
        this.transactionTypeID = transactionTypeID;
    }

    public Integer getPaymentMethodID() {
        return paymentMethodID;
    }

    public void setPaymentMethodID(Integer paymentMethodID) {
        this.paymentMethodID = paymentMethodID;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigDecimal getAmountExcludingTax() {
        return amountExcludingTax;
    }

    public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
        this.amountExcludingTax = amountExcludingTax;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public BigDecimal getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(BigDecimal outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public Date getFinalizationDate() {
        return finalizationDate;
    }

    public void setFinalizationDate(Date finalizationDate) {
        this.finalizationDate = finalizationDate;
    }

    public Boolean getIsFinalized() {
        return isFinalized;
    }

    public void setIsFinalized(Boolean isFinalized) {
        this.isFinalized = isFinalized;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getLastEditedWhen() {
        return lastEditedWhen;
    }

    public void setLastEditedWhen(Date lastEditedWhen) {
        this.lastEditedWhen = lastEditedWhen;
    }

    public Customers getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customers customerID) {
        this.customerID = customerID;
    }

    public Invoices getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(Invoices invoiceID) {
        this.invoiceID = invoiceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerTransactionID != null ? customerTransactionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerTransactions)) {
            return false;
        }
        CustomerTransactions other = (CustomerTransactions) object;
        if ((this.customerTransactionID == null && other.customerTransactionID != null) || (this.customerTransactionID != null && !this.customerTransactionID.equals(other.customerTransactionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.CustomerTransactions[ customerTransactionID=" + customerTransactionID + " ]";
    }
    
}
