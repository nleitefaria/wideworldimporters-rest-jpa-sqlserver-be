/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "OrderLines", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderLines.findAll", query = "SELECT o FROM OrderLines o")
    , @NamedQuery(name = "OrderLines.findByOrderLineID", query = "SELECT o FROM OrderLines o WHERE o.orderLineID = :orderLineID")
    , @NamedQuery(name = "OrderLines.findByStockItemID", query = "SELECT o FROM OrderLines o WHERE o.stockItemID = :stockItemID")
    , @NamedQuery(name = "OrderLines.findByDescription", query = "SELECT o FROM OrderLines o WHERE o.description = :description")
    , @NamedQuery(name = "OrderLines.findByPackageTypeID", query = "SELECT o FROM OrderLines o WHERE o.packageTypeID = :packageTypeID")
    , @NamedQuery(name = "OrderLines.findByQuantity", query = "SELECT o FROM OrderLines o WHERE o.quantity = :quantity")
    , @NamedQuery(name = "OrderLines.findByUnitPrice", query = "SELECT o FROM OrderLines o WHERE o.unitPrice = :unitPrice")
    , @NamedQuery(name = "OrderLines.findByTaxRate", query = "SELECT o FROM OrderLines o WHERE o.taxRate = :taxRate")
    , @NamedQuery(name = "OrderLines.findByPickedQuantity", query = "SELECT o FROM OrderLines o WHERE o.pickedQuantity = :pickedQuantity")
    , @NamedQuery(name = "OrderLines.findByPickingCompletedWhen", query = "SELECT o FROM OrderLines o WHERE o.pickingCompletedWhen = :pickingCompletedWhen")
    , @NamedQuery(name = "OrderLines.findByLastEditedBy", query = "SELECT o FROM OrderLines o WHERE o.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "OrderLines.findByLastEditedWhen", query = "SELECT o FROM OrderLines o WHERE o.lastEditedWhen = :lastEditedWhen")})
public class OrderLines implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "OrderLineID")
    private Integer orderLineID;
    @Basic(optional = false)
    @Column(name = "StockItemID")
    private int stockItemID;
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @Column(name = "PackageTypeID")
    private int packageTypeID;
    @Basic(optional = false)
    @Column(name = "Quantity")
    private int quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UnitPrice")
    private BigDecimal unitPrice;
    @Basic(optional = false)
    @Column(name = "TaxRate")
    private BigDecimal taxRate;
    @Basic(optional = false)
    @Column(name = "PickedQuantity")
    private int pickedQuantity;
    @Column(name = "PickingCompletedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickingCompletedWhen;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastEditedWhen;
    @JoinColumn(name = "OrderID", referencedColumnName = "OrderID")
    @ManyToOne(optional = false)
    private Orders orderID;

    public OrderLines() {
    }

    public OrderLines(Integer orderLineID) {
        this.orderLineID = orderLineID;
    }

    public OrderLines(Integer orderLineID, int stockItemID, String description, int packageTypeID, int quantity, BigDecimal taxRate, int pickedQuantity, int lastEditedBy, Date lastEditedWhen) {
        this.orderLineID = orderLineID;
        this.stockItemID = stockItemID;
        this.description = description;
        this.packageTypeID = packageTypeID;
        this.quantity = quantity;
        this.taxRate = taxRate;
        this.pickedQuantity = pickedQuantity;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

    public Integer getOrderLineID() {
        return orderLineID;
    }

    public void setOrderLineID(Integer orderLineID) {
        this.orderLineID = orderLineID;
    }

    public int getStockItemID() {
        return stockItemID;
    }

    public void setStockItemID(int stockItemID) {
        this.stockItemID = stockItemID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPackageTypeID() {
        return packageTypeID;
    }

    public void setPackageTypeID(int packageTypeID) {
        this.packageTypeID = packageTypeID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public int getPickedQuantity() {
        return pickedQuantity;
    }

    public void setPickedQuantity(int pickedQuantity) {
        this.pickedQuantity = pickedQuantity;
    }

    public Date getPickingCompletedWhen() {
        return pickingCompletedWhen;
    }

    public void setPickingCompletedWhen(Date pickingCompletedWhen) {
        this.pickingCompletedWhen = pickingCompletedWhen;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getLastEditedWhen() {
        return lastEditedWhen;
    }

    public void setLastEditedWhen(Date lastEditedWhen) {
        this.lastEditedWhen = lastEditedWhen;
    }

    public Orders getOrderID() {
        return orderID;
    }

    public void setOrderID(Orders orderID) {
        this.orderID = orderID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderLineID != null ? orderLineID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderLines)) {
            return false;
        }
        OrderLines other = (OrderLines) object;
        if ((this.orderLineID == null && other.orderLineID != null) || (this.orderLineID != null && !this.orderLineID.equals(other.orderLineID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.OrderLines[ orderLineID=" + orderLineID + " ]";
    }
    
}
