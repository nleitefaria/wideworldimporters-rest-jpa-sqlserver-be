/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "BuyingGroups", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BuyingGroups.findAll", query = "SELECT b FROM BuyingGroups b")
    , @NamedQuery(name = "BuyingGroups.findAllOrderDesc", query = "SELECT b FROM BuyingGroups b ORDER BY b.buyingGroupID DESC")
    , @NamedQuery(name = "BuyingGroups.findByBuyingGroupID", query = "SELECT b FROM BuyingGroups b WHERE b.buyingGroupID = :buyingGroupID")
    , @NamedQuery(name = "BuyingGroups.findByBuyingGroupName", query = "SELECT b FROM BuyingGroups b WHERE b.buyingGroupName = :buyingGroupName")
    , @NamedQuery(name = "BuyingGroups.findByLastEditedBy", query = "SELECT b FROM BuyingGroups b WHERE b.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "BuyingGroups.findByValidFrom", query = "SELECT b FROM BuyingGroups b WHERE b.validFrom = :validFrom")
    , @NamedQuery(name = "BuyingGroups.findByValidTo", query = "SELECT b FROM BuyingGroups b WHERE b.validTo = :validTo")})
public class BuyingGroups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "BuyingGroupID")
    private Integer buyingGroupID;
    @Basic(optional = false)
    @Column(name = "BuyingGroupName")
    private String buyingGroupName;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "ValidFrom")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validFrom;
    @Basic(optional = false)
    @Column(name = "ValidTo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTo;
    @OneToMany(mappedBy = "buyingGroupID")
    private List<Customers> customersList;
    @OneToMany(mappedBy = "buyingGroupID")
    private List<SpecialDeals> specialDealsList;

    public BuyingGroups() {
    }

    public BuyingGroups(Integer buyingGroupID) {
        this.buyingGroupID = buyingGroupID;
    }

    public BuyingGroups(Integer buyingGroupID, String buyingGroupName, int lastEditedBy, Date validFrom, Date validTo) {
        this.buyingGroupID = buyingGroupID;
        this.buyingGroupName = buyingGroupName;
        this.lastEditedBy = lastEditedBy;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

    public Integer getBuyingGroupID() {
        return buyingGroupID;
    }

    public void setBuyingGroupID(Integer buyingGroupID) {
        this.buyingGroupID = buyingGroupID;
    }

    public String getBuyingGroupName() {
        return buyingGroupName;
    }

    public void setBuyingGroupName(String buyingGroupName) {
        this.buyingGroupName = buyingGroupName;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    @XmlTransient
    public List<Customers> getCustomersList() {
        return customersList;
    }

    public void setCustomersList(List<Customers> customersList) {
        this.customersList = customersList;
    }

    @XmlTransient
    public List<SpecialDeals> getSpecialDealsList() {
        return specialDealsList;
    }

    public void setSpecialDealsList(List<SpecialDeals> specialDealsList) {
        this.specialDealsList = specialDealsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (buyingGroupID != null ? buyingGroupID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BuyingGroups)) {
            return false;
        }
        BuyingGroups other = (BuyingGroups) object;
        if ((this.buyingGroupID == null && other.buyingGroupID != null) || (this.buyingGroupID != null && !this.buyingGroupID.equals(other.buyingGroupID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.BuyingGroups[ buyingGroupID=" + buyingGroupID + " ]";
    }
    
}
