/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "Invoices", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Invoices.findAll", query = "SELECT i FROM Invoices i")
    , @NamedQuery(name = "Invoices.findByInvoiceID", query = "SELECT i FROM Invoices i WHERE i.invoiceID = :invoiceID")
    , @NamedQuery(name = "Invoices.findByDeliveryMethodID", query = "SELECT i FROM Invoices i WHERE i.deliveryMethodID = :deliveryMethodID")
    , @NamedQuery(name = "Invoices.findByContactPersonID", query = "SELECT i FROM Invoices i WHERE i.contactPersonID = :contactPersonID")
    , @NamedQuery(name = "Invoices.findByAccountsPersonID", query = "SELECT i FROM Invoices i WHERE i.accountsPersonID = :accountsPersonID")
    , @NamedQuery(name = "Invoices.findBySalespersonPersonID", query = "SELECT i FROM Invoices i WHERE i.salespersonPersonID = :salespersonPersonID")
    , @NamedQuery(name = "Invoices.findByPackedByPersonID", query = "SELECT i FROM Invoices i WHERE i.packedByPersonID = :packedByPersonID")
    , @NamedQuery(name = "Invoices.findByInvoiceDate", query = "SELECT i FROM Invoices i WHERE i.invoiceDate = :invoiceDate")
    , @NamedQuery(name = "Invoices.findByCustomerPurchaseOrderNumber", query = "SELECT i FROM Invoices i WHERE i.customerPurchaseOrderNumber = :customerPurchaseOrderNumber")
    , @NamedQuery(name = "Invoices.findByIsCreditNote", query = "SELECT i FROM Invoices i WHERE i.isCreditNote = :isCreditNote")
    , @NamedQuery(name = "Invoices.findByCreditNoteReason", query = "SELECT i FROM Invoices i WHERE i.creditNoteReason = :creditNoteReason")
    , @NamedQuery(name = "Invoices.findByComments", query = "SELECT i FROM Invoices i WHERE i.comments = :comments")
    , @NamedQuery(name = "Invoices.findByDeliveryInstructions", query = "SELECT i FROM Invoices i WHERE i.deliveryInstructions = :deliveryInstructions")
    , @NamedQuery(name = "Invoices.findByInternalComments", query = "SELECT i FROM Invoices i WHERE i.internalComments = :internalComments")
    , @NamedQuery(name = "Invoices.findByTotalDryItems", query = "SELECT i FROM Invoices i WHERE i.totalDryItems = :totalDryItems")
    , @NamedQuery(name = "Invoices.findByTotalChillerItems", query = "SELECT i FROM Invoices i WHERE i.totalChillerItems = :totalChillerItems")
    , @NamedQuery(name = "Invoices.findByDeliveryRun", query = "SELECT i FROM Invoices i WHERE i.deliveryRun = :deliveryRun")
    , @NamedQuery(name = "Invoices.findByRunPosition", query = "SELECT i FROM Invoices i WHERE i.runPosition = :runPosition")
    , @NamedQuery(name = "Invoices.findByReturnedDeliveryData", query = "SELECT i FROM Invoices i WHERE i.returnedDeliveryData = :returnedDeliveryData")
    , @NamedQuery(name = "Invoices.findByConfirmedDeliveryTime", query = "SELECT i FROM Invoices i WHERE i.confirmedDeliveryTime = :confirmedDeliveryTime")
    , @NamedQuery(name = "Invoices.findByConfirmedReceivedBy", query = "SELECT i FROM Invoices i WHERE i.confirmedReceivedBy = :confirmedReceivedBy")
    , @NamedQuery(name = "Invoices.findByLastEditedBy", query = "SELECT i FROM Invoices i WHERE i.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "Invoices.findByLastEditedWhen", query = "SELECT i FROM Invoices i WHERE i.lastEditedWhen = :lastEditedWhen")})
public class Invoices implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "InvoiceID")
    private Integer invoiceID;
    @Basic(optional = false)
    @Column(name = "DeliveryMethodID")
    private int deliveryMethodID;
    @Basic(optional = false)
    @Column(name = "ContactPersonID")
    private int contactPersonID;
    @Basic(optional = false)
    @Column(name = "AccountsPersonID")
    private int accountsPersonID;
    @Basic(optional = false)
    @Column(name = "SalespersonPersonID")
    private int salespersonPersonID;
    @Basic(optional = false)
    @Column(name = "PackedByPersonID")
    private int packedByPersonID;
    @Basic(optional = false)
    @Column(name = "InvoiceDate")
    @Temporal(TemporalType.DATE)
    private Date invoiceDate;
    @Column(name = "CustomerPurchaseOrderNumber")
    private String customerPurchaseOrderNumber;
    @Basic(optional = false)
    @Column(name = "IsCreditNote")
    private boolean isCreditNote;
    @Column(name = "CreditNoteReason")
    private String creditNoteReason;
    @Column(name = "Comments")
    private String comments;
    @Column(name = "DeliveryInstructions")
    private String deliveryInstructions;
    @Column(name = "InternalComments")
    private String internalComments;
    @Basic(optional = false)
    @Column(name = "TotalDryItems")
    private int totalDryItems;
    @Basic(optional = false)
    @Column(name = "TotalChillerItems")
    private int totalChillerItems;
    @Column(name = "DeliveryRun")
    private String deliveryRun;
    @Column(name = "RunPosition")
    private String runPosition;
    @Column(name = "ReturnedDeliveryData")
    private String returnedDeliveryData;
    @Column(name = "ConfirmedDeliveryTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date confirmedDeliveryTime;
    @Column(name = "ConfirmedReceivedBy")
    private String confirmedReceivedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastEditedWhen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceID")
    private List<InvoiceLines> invoiceLinesList;
    @OneToMany(mappedBy = "invoiceID")
    private List<CustomerTransactions> customerTransactionsList;
    @JoinColumn(name = "CustomerID", referencedColumnName = "CustomerID")
    @ManyToOne(optional = false)
    private Customers customerID;
    @JoinColumn(name = "BillToCustomerID", referencedColumnName = "CustomerID")
    @ManyToOne(optional = false)
    private Customers billToCustomerID;
    @JoinColumn(name = "OrderID", referencedColumnName = "OrderID")
    @ManyToOne
    private Orders orderID;

    public Invoices() {
    }

    public Invoices(Integer invoiceID) {
        this.invoiceID = invoiceID;
    }

    public Invoices(Integer invoiceID, int deliveryMethodID, int contactPersonID, int accountsPersonID, int salespersonPersonID, int packedByPersonID, Date invoiceDate, boolean isCreditNote, int totalDryItems, int totalChillerItems, int lastEditedBy, Date lastEditedWhen) {
        this.invoiceID = invoiceID;
        this.deliveryMethodID = deliveryMethodID;
        this.contactPersonID = contactPersonID;
        this.accountsPersonID = accountsPersonID;
        this.salespersonPersonID = salespersonPersonID;
        this.packedByPersonID = packedByPersonID;
        this.invoiceDate = invoiceDate;
        this.isCreditNote = isCreditNote;
        this.totalDryItems = totalDryItems;
        this.totalChillerItems = totalChillerItems;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

    public Integer getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(Integer invoiceID) {
        this.invoiceID = invoiceID;
    }

    public int getDeliveryMethodID() {
        return deliveryMethodID;
    }

    public void setDeliveryMethodID(int deliveryMethodID) {
        this.deliveryMethodID = deliveryMethodID;
    }

    public int getContactPersonID() {
        return contactPersonID;
    }

    public void setContactPersonID(int contactPersonID) {
        this.contactPersonID = contactPersonID;
    }

    public int getAccountsPersonID() {
        return accountsPersonID;
    }

    public void setAccountsPersonID(int accountsPersonID) {
        this.accountsPersonID = accountsPersonID;
    }

    public int getSalespersonPersonID() {
        return salespersonPersonID;
    }

    public void setSalespersonPersonID(int salespersonPersonID) {
        this.salespersonPersonID = salespersonPersonID;
    }

    public int getPackedByPersonID() {
        return packedByPersonID;
    }

    public void setPackedByPersonID(int packedByPersonID) {
        this.packedByPersonID = packedByPersonID;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getCustomerPurchaseOrderNumber() {
        return customerPurchaseOrderNumber;
    }

    public void setCustomerPurchaseOrderNumber(String customerPurchaseOrderNumber) {
        this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
    }

    public boolean getIsCreditNote() {
        return isCreditNote;
    }

    public void setIsCreditNote(boolean isCreditNote) {
        this.isCreditNote = isCreditNote;
    }

    public String getCreditNoteReason() {
        return creditNoteReason;
    }

    public void setCreditNoteReason(String creditNoteReason) {
        this.creditNoteReason = creditNoteReason;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDeliveryInstructions() {
        return deliveryInstructions;
    }

    public void setDeliveryInstructions(String deliveryInstructions) {
        this.deliveryInstructions = deliveryInstructions;
    }

    public String getInternalComments() {
        return internalComments;
    }

    public void setInternalComments(String internalComments) {
        this.internalComments = internalComments;
    }

    public int getTotalDryItems() {
        return totalDryItems;
    }

    public void setTotalDryItems(int totalDryItems) {
        this.totalDryItems = totalDryItems;
    }

    public int getTotalChillerItems() {
        return totalChillerItems;
    }

    public void setTotalChillerItems(int totalChillerItems) {
        this.totalChillerItems = totalChillerItems;
    }

    public String getDeliveryRun() {
        return deliveryRun;
    }

    public void setDeliveryRun(String deliveryRun) {
        this.deliveryRun = deliveryRun;
    }

    public String getRunPosition() {
        return runPosition;
    }

    public void setRunPosition(String runPosition) {
        this.runPosition = runPosition;
    }

    public String getReturnedDeliveryData() {
        return returnedDeliveryData;
    }

    public void setReturnedDeliveryData(String returnedDeliveryData) {
        this.returnedDeliveryData = returnedDeliveryData;
    }

    public Date getConfirmedDeliveryTime() {
        return confirmedDeliveryTime;
    }

    public void setConfirmedDeliveryTime(Date confirmedDeliveryTime) {
        this.confirmedDeliveryTime = confirmedDeliveryTime;
    }

    public String getConfirmedReceivedBy() {
        return confirmedReceivedBy;
    }

    public void setConfirmedReceivedBy(String confirmedReceivedBy) {
        this.confirmedReceivedBy = confirmedReceivedBy;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getLastEditedWhen() {
        return lastEditedWhen;
    }

    public void setLastEditedWhen(Date lastEditedWhen) {
        this.lastEditedWhen = lastEditedWhen;
    }

    @XmlTransient
    public List<InvoiceLines> getInvoiceLinesList() {
        return invoiceLinesList;
    }

    public void setInvoiceLinesList(List<InvoiceLines> invoiceLinesList) {
        this.invoiceLinesList = invoiceLinesList;
    }

    @XmlTransient
    public List<CustomerTransactions> getCustomerTransactionsList() {
        return customerTransactionsList;
    }

    public void setCustomerTransactionsList(List<CustomerTransactions> customerTransactionsList) {
        this.customerTransactionsList = customerTransactionsList;
    }

    public Customers getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customers customerID) {
        this.customerID = customerID;
    }

    public Customers getBillToCustomerID() {
        return billToCustomerID;
    }

    public void setBillToCustomerID(Customers billToCustomerID) {
        this.billToCustomerID = billToCustomerID;
    }

    public Orders getOrderID() {
        return orderID;
    }

    public void setOrderID(Orders orderID) {
        this.orderID = orderID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceID != null ? invoiceID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invoices)) {
            return false;
        }
        Invoices other = (Invoices) object;
        if ((this.invoiceID == null && other.invoiceID != null) || (this.invoiceID != null && !this.invoiceID.equals(other.invoiceID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.Invoices[ invoiceID=" + invoiceID + " ]";
    }
    
}
