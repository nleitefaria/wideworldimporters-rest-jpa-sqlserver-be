/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "InvoiceLines", catalog = "WideWorldImporters", schema = "Sales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InvoiceLines.findAll", query = "SELECT i FROM InvoiceLines i")
    , @NamedQuery(name = "InvoiceLines.findByInvoiceLineID", query = "SELECT i FROM InvoiceLines i WHERE i.invoiceLineID = :invoiceLineID")
    , @NamedQuery(name = "InvoiceLines.findByStockItemID", query = "SELECT i FROM InvoiceLines i WHERE i.stockItemID = :stockItemID")
    , @NamedQuery(name = "InvoiceLines.findByDescription", query = "SELECT i FROM InvoiceLines i WHERE i.description = :description")
    , @NamedQuery(name = "InvoiceLines.findByPackageTypeID", query = "SELECT i FROM InvoiceLines i WHERE i.packageTypeID = :packageTypeID")
    , @NamedQuery(name = "InvoiceLines.findByQuantity", query = "SELECT i FROM InvoiceLines i WHERE i.quantity = :quantity")
    , @NamedQuery(name = "InvoiceLines.findByUnitPrice", query = "SELECT i FROM InvoiceLines i WHERE i.unitPrice = :unitPrice")
    , @NamedQuery(name = "InvoiceLines.findByTaxRate", query = "SELECT i FROM InvoiceLines i WHERE i.taxRate = :taxRate")
    , @NamedQuery(name = "InvoiceLines.findByTaxAmount", query = "SELECT i FROM InvoiceLines i WHERE i.taxAmount = :taxAmount")
    , @NamedQuery(name = "InvoiceLines.findByLineProfit", query = "SELECT i FROM InvoiceLines i WHERE i.lineProfit = :lineProfit")
    , @NamedQuery(name = "InvoiceLines.findByExtendedPrice", query = "SELECT i FROM InvoiceLines i WHERE i.extendedPrice = :extendedPrice")
    , @NamedQuery(name = "InvoiceLines.findByLastEditedBy", query = "SELECT i FROM InvoiceLines i WHERE i.lastEditedBy = :lastEditedBy")
    , @NamedQuery(name = "InvoiceLines.findByLastEditedWhen", query = "SELECT i FROM InvoiceLines i WHERE i.lastEditedWhen = :lastEditedWhen")})
public class InvoiceLines implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "InvoiceLineID")
    private Integer invoiceLineID;
    @Basic(optional = false)
    @Column(name = "StockItemID")
    private int stockItemID;
    @Basic(optional = false)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @Column(name = "PackageTypeID")
    private int packageTypeID;
    @Basic(optional = false)
    @Column(name = "Quantity")
    private int quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UnitPrice")
    private BigDecimal unitPrice;
    @Basic(optional = false)
    @Column(name = "TaxRate")
    private BigDecimal taxRate;
    @Basic(optional = false)
    @Column(name = "TaxAmount")
    private BigDecimal taxAmount;
    @Basic(optional = false)
    @Column(name = "LineProfit")
    private BigDecimal lineProfit;
    @Basic(optional = false)
    @Column(name = "ExtendedPrice")
    private BigDecimal extendedPrice;
    @Basic(optional = false)
    @Column(name = "LastEditedBy")
    private int lastEditedBy;
    @Basic(optional = false)
    @Column(name = "LastEditedWhen")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastEditedWhen;
    @JoinColumn(name = "InvoiceID", referencedColumnName = "InvoiceID")
    @ManyToOne(optional = false)
    private Invoices invoiceID;

    public InvoiceLines() {
    }

    public InvoiceLines(Integer invoiceLineID) {
        this.invoiceLineID = invoiceLineID;
    }

    public InvoiceLines(Integer invoiceLineID, int stockItemID, String description, int packageTypeID, int quantity, BigDecimal taxRate, BigDecimal taxAmount, BigDecimal lineProfit, BigDecimal extendedPrice, int lastEditedBy, Date lastEditedWhen) {
        this.invoiceLineID = invoiceLineID;
        this.stockItemID = stockItemID;
        this.description = description;
        this.packageTypeID = packageTypeID;
        this.quantity = quantity;
        this.taxRate = taxRate;
        this.taxAmount = taxAmount;
        this.lineProfit = lineProfit;
        this.extendedPrice = extendedPrice;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

    public Integer getInvoiceLineID() {
        return invoiceLineID;
    }

    public void setInvoiceLineID(Integer invoiceLineID) {
        this.invoiceLineID = invoiceLineID;
    }

    public int getStockItemID() {
        return stockItemID;
    }

    public void setStockItemID(int stockItemID) {
        this.stockItemID = stockItemID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPackageTypeID() {
        return packageTypeID;
    }

    public void setPackageTypeID(int packageTypeID) {
        this.packageTypeID = packageTypeID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getLineProfit() {
        return lineProfit;
    }

    public void setLineProfit(BigDecimal lineProfit) {
        this.lineProfit = lineProfit;
    }

    public BigDecimal getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(BigDecimal extendedPrice) {
        this.extendedPrice = extendedPrice;
    }

    public int getLastEditedBy() {
        return lastEditedBy;
    }

    public void setLastEditedBy(int lastEditedBy) {
        this.lastEditedBy = lastEditedBy;
    }

    public Date getLastEditedWhen() {
        return lastEditedWhen;
    }

    public void setLastEditedWhen(Date lastEditedWhen) {
        this.lastEditedWhen = lastEditedWhen;
    }

    public Invoices getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(Invoices invoiceID) {
        this.invoiceID = invoiceID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceLineID != null ? invoiceLineID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceLines)) {
            return false;
        }
        InvoiceLines other = (InvoiceLines) object;
        if ((this.invoiceLineID == null && other.invoiceLineID != null) || (this.invoiceLineID != null && !this.invoiceLineID.equals(other.invoiceLineID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jpasqlserver4.InvoiceLines[ invoiceLineID=" + invoiceLineID + " ]";
    }
    
}
