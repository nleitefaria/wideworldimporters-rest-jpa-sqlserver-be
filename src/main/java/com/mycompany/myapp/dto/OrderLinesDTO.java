package com.mycompany.myapp.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OrderLinesDTO 
{
	private Integer orderLineID;
    private String description;
    private int quantity;
    private BigDecimal unitPrice;
    private BigDecimal taxRate;
    private int pickedQuantity;
    private Date pickingCompletedWhen;
    private int lastEditedBy;
    private Date lastEditedWhen;
     
	public OrderLinesDTO() {
		
	}

	public OrderLinesDTO(Integer orderLineID, String description, int quantity, BigDecimal unitPrice,
			BigDecimal taxRate, int pickedQuantity, Date pickingCompletedWhen, int lastEditedBy, Date lastEditedWhen) {	
		this.orderLineID = orderLineID;
		this.description = description;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.taxRate = taxRate;
		this.pickedQuantity = pickedQuantity;
		this.pickingCompletedWhen = pickingCompletedWhen;
		this.lastEditedBy = lastEditedBy;
		this.lastEditedWhen = lastEditedWhen;
	}

	public Integer getOrderLineID() {
		return orderLineID;
	}

	public void setOrderLineID(Integer orderLineID) {
		this.orderLineID = orderLineID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate = taxRate;
	}

	public int getPickedQuantity() {
		return pickedQuantity;
	}

	public void setPickedQuantity(int pickedQuantity) {
		this.pickedQuantity = pickedQuantity;
	}

	public Date getPickingCompletedWhen() {
		return pickingCompletedWhen;
	}

	public void setPickingCompletedWhen(Date pickingCompletedWhen) {
		this.pickingCompletedWhen = pickingCompletedWhen;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getLastEditedWhen() {
		return lastEditedWhen;
	}

	public void setLastEditedWhen(Date lastEditedWhen) {
		this.lastEditedWhen = lastEditedWhen;
	}
}
