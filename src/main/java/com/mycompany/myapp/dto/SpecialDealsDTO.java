package com.mycompany.myapp.dto;

import java.math.BigDecimal;
import java.util.Date;

public class SpecialDealsDTO
{
	private Integer specialDealID;
    private String dealDescription;   
    private Date startDate;   
    private Date endDate;   
    private BigDecimal discountAmount;   
    private BigDecimal discountPercentage;   
    private BigDecimal unitPrice;  
    private int lastEditedBy;  
    private Date lastEditedWhen;
	
    public SpecialDealsDTO()
	{   	
	}
    
    public SpecialDealsDTO(String dealDescription, Date startDate, Date endDate, int lastEditedBy, Date lastEditedWhen)
	{    
        this.dealDescription = dealDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }
	
	public SpecialDealsDTO(Integer specialDealID, String dealDescription, Date startDate, Date endDate, int lastEditedBy, Date lastEditedWhen) 
	{
        this.specialDealID = specialDealID;
        this.dealDescription = dealDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.lastEditedBy = lastEditedBy;
        this.lastEditedWhen = lastEditedWhen;
    }

	public Integer getSpecialDealID() 
	{
		return specialDealID;
	}

	public void setSpecialDealID(Integer specialDealID)
	{
		this.specialDealID = specialDealID;
	}

	public String getDealDescription()
	{
		return dealDescription;
	}

	public void setDealDescription(String dealDescription)
	{
		this.dealDescription = dealDescription;
	}

	public Date getStartDate() 
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public BigDecimal getDiscountAmount() 
	{
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount)
	{
		this.discountAmount = discountAmount;
	}

	public BigDecimal getDiscountPercentage()
	{
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage)
	{
		this.discountPercentage = discountPercentage;
	}

	public BigDecimal getUnitPrice() 
	{
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice)
	{
		this.unitPrice = unitPrice;
	}

	public int getLastEditedBy() 
	{
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy)
	{
		this.lastEditedBy = lastEditedBy;
	}

	public Date getLastEditedWhen()
	{
		return lastEditedWhen;
	}

	public void setLastEditedWhen(Date lastEditedWhen) 
	{
		this.lastEditedWhen = lastEditedWhen;
	}

}
