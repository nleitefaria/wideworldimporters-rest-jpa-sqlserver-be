package com.mycompany.myapp.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CustomersDTO 
{	
	private Integer customerID;
    private String customerName; 
    private int primaryContactPersonID;  
    private Integer alternateContactPersonID;  
    private int deliveryMethodID;  
    private int deliveryCityID;   
    private int postalCityID;   
    private BigDecimal creditLimit;
    private Date accountOpenedDate;
    private BigDecimal standardDiscountPercentage;
    private boolean isStatementSent;
    private boolean isOnCreditHold;
    private int paymentDays;
    private String phoneNumber;    
    private String faxNumber;   
    private String deliveryRun;   
    private String runPosition;   
    private String websiteURL;
    private String deliveryAddressLine1;
    private String deliveryAddressLine2;
    private String deliveryPostalCode;
    private byte[] deliveryLocation;
    private String postalAddressLine1;
    private String postalAddressLine2;  
    private String postalPostalCode; 
    private int lastEditedBy;   
    private Date validFrom;  
    private Date validTo;
	
	public CustomersDTO()
	{	
	}
	
	public CustomersDTO(Integer customerID, String customerName, int primaryContactPersonID, int deliveryMethodID, int deliveryCityID, int postalCityID, Date accountOpenedDate, BigDecimal standardDiscountPercentage, boolean isStatementSent, boolean isOnCreditHold, int paymentDays, String phoneNumber, String faxNumber, String websiteURL, String deliveryAddressLine1, String deliveryPostalCode, String postalAddressLine1, String postalPostalCode, int lastEditedBy, Date validFrom, Date validTo) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.primaryContactPersonID = primaryContactPersonID;
        this.deliveryMethodID = deliveryMethodID;
        this.deliveryCityID = deliveryCityID;
        this.postalCityID = postalCityID;
        this.accountOpenedDate = accountOpenedDate;
        this.standardDiscountPercentage = standardDiscountPercentage;
        this.isStatementSent = isStatementSent;
        this.isOnCreditHold = isOnCreditHold;
        this.paymentDays = paymentDays;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.websiteURL = websiteURL;
        this.deliveryAddressLine1 = deliveryAddressLine1;
        this.deliveryPostalCode = deliveryPostalCode;
        this.postalAddressLine1 = postalAddressLine1;
        this.postalPostalCode = postalPostalCode;
        this.lastEditedBy = lastEditedBy;
        this.validFrom = validFrom;
        this.validTo = validTo;
    }

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getPrimaryContactPersonID() {
		return primaryContactPersonID;
	}

	public void setPrimaryContactPersonID(int primaryContactPersonID) {
		this.primaryContactPersonID = primaryContactPersonID;
	}

	public Integer getAlternateContactPersonID() {
		return alternateContactPersonID;
	}

	public void setAlternateContactPersonID(Integer alternateContactPersonID) {
		this.alternateContactPersonID = alternateContactPersonID;
	}

	public int getDeliveryMethodID() {
		return deliveryMethodID;
	}

	public void setDeliveryMethodID(int deliveryMethodID) {
		this.deliveryMethodID = deliveryMethodID;
	}

	public int getDeliveryCityID() {
		return deliveryCityID;
	}

	public void setDeliveryCityID(int deliveryCityID) {
		this.deliveryCityID = deliveryCityID;
	}

	public int getPostalCityID() {
		return postalCityID;
	}

	public void setPostalCityID(int postalCityID) {
		this.postalCityID = postalCityID;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Date getAccountOpenedDate() {
		return accountOpenedDate;
	}

	public void setAccountOpenedDate(Date accountOpenedDate) {
		this.accountOpenedDate = accountOpenedDate;
	}

	public BigDecimal getStandardDiscountPercentage() {
		return standardDiscountPercentage;
	}

	public void setStandardDiscountPercentage(BigDecimal standardDiscountPercentage) {
		this.standardDiscountPercentage = standardDiscountPercentage;
	}

	public boolean isStatementSent() {
		return isStatementSent;
	}

	public void setStatementSent(boolean isStatementSent) {
		this.isStatementSent = isStatementSent;
	}

	public boolean isOnCreditHold() {
		return isOnCreditHold;
	}

	public void setOnCreditHold(boolean isOnCreditHold) {
		this.isOnCreditHold = isOnCreditHold;
	}

	public int getPaymentDays() {
		return paymentDays;
	}

	public void setPaymentDays(int paymentDays) {
		this.paymentDays = paymentDays;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getDeliveryRun() {
		return deliveryRun;
	}

	public void setDeliveryRun(String deliveryRun) {
		this.deliveryRun = deliveryRun;
	}

	public String getRunPosition() {
		return runPosition;
	}

	public void setRunPosition(String runPosition) {
		this.runPosition = runPosition;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public String getDeliveryAddressLine1() {
		return deliveryAddressLine1;
	}

	public void setDeliveryAddressLine1(String deliveryAddressLine1) {
		this.deliveryAddressLine1 = deliveryAddressLine1;
	}

	public String getDeliveryAddressLine2() {
		return deliveryAddressLine2;
	}

	public void setDeliveryAddressLine2(String deliveryAddressLine2) {
		this.deliveryAddressLine2 = deliveryAddressLine2;
	}

	public String getDeliveryPostalCode() {
		return deliveryPostalCode;
	}

	public void setDeliveryPostalCode(String deliveryPostalCode) {
		this.deliveryPostalCode = deliveryPostalCode;
	}

	public byte[] getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setDeliveryLocation(byte[] deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public String getPostalAddressLine1() {
		return postalAddressLine1;
	}

	public void setPostalAddressLine1(String postalAddressLine1) {
		this.postalAddressLine1 = postalAddressLine1;
	}

	public String getPostalAddressLine2() {
		return postalAddressLine2;
	}

	public void setPostalAddressLine2(String postalAddressLine2) {
		this.postalAddressLine2 = postalAddressLine2;
	}

	public String getPostalPostalCode() {
		return postalPostalCode;
	}

	public void setPostalPostalCode(String postalPostalCode) {
		this.postalPostalCode = postalPostalCode;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
