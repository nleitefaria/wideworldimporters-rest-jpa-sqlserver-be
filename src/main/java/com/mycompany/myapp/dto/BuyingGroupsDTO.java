package com.mycompany.myapp.dto;

import java.util.Date;

public class BuyingGroupsDTO 
{
	private Integer buyingGroupID;
    private String buyingGroupName;
    private int lastEditedBy;
    private Date validFrom;
    private Date validTo;
    
    public BuyingGroupsDTO() 
	{		
	}
    
	public BuyingGroupsDTO(Integer buyingGroupID, String buyingGroupName, int lastEditedBy, Date validFrom, Date validTo) 
	{
		this.buyingGroupID = buyingGroupID;
		this.buyingGroupName = buyingGroupName;
		this.lastEditedBy = lastEditedBy;
		this.validFrom = validFrom;
		this.validTo = validTo;
	}

	public Integer getBuyingGroupID() {
		return buyingGroupID;
	}

	public void setBuyingGroupID(Integer buyingGroupID) {
		this.buyingGroupID = buyingGroupID;
	}

	public String getBuyingGroupName() {
		return buyingGroupName;
	}

	public void setBuyingGroupName(String buyingGroupName) {
		this.buyingGroupName = buyingGroupName;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
