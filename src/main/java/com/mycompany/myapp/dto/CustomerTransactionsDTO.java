package com.mycompany.myapp.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CustomerTransactionsDTO 
{	
	private Integer customerTransactionID;
    private int transactionTypeID;   
    private Integer paymentMethodID;   
    private Date transactionDate;
    private BigDecimal amountExcludingTax;
    private BigDecimal taxAmount;
    private BigDecimal transactionAmount;
    private BigDecimal outstandingBalance;
    private Date finalizationDate;
    private Boolean isFinalized;
    private int lastEditedBy;
    private Date lastEditedWhen;
       
    public CustomerTransactionsDTO()
    {    	
    }

	public CustomerTransactionsDTO(Integer customerTransactionID, int transactionTypeID, Integer paymentMethodID,
			Date transactionDate, BigDecimal amountExcludingTax, BigDecimal taxAmount, BigDecimal transactionAmount,
			BigDecimal outstandingBalance, Date finalizationDate, Boolean isFinalized, int lastEditedBy,
			Date lastEditedWhen) {
		this.customerTransactionID = customerTransactionID;
		this.transactionTypeID = transactionTypeID;
		this.paymentMethodID = paymentMethodID;
		this.transactionDate = transactionDate;
		this.amountExcludingTax = amountExcludingTax;
		this.taxAmount = taxAmount;
		this.transactionAmount = transactionAmount;
		this.outstandingBalance = outstandingBalance;
		this.finalizationDate = finalizationDate;
		this.isFinalized = isFinalized;
		this.lastEditedBy = lastEditedBy;
		this.lastEditedWhen = lastEditedWhen;
	}

	public Integer getCustomerTransactionID() {
		return customerTransactionID;
	}

	public void setCustomerTransactionID(Integer customerTransactionID) {
		this.customerTransactionID = customerTransactionID;
	}

	public int getTransactionTypeID() {
		return transactionTypeID;
	}

	public void setTransactionTypeID(int transactionTypeID) {
		this.transactionTypeID = transactionTypeID;
	}

	public Integer getPaymentMethodID() {
		return paymentMethodID;
	}

	public void setPaymentMethodID(Integer paymentMethodID) {
		this.paymentMethodID = paymentMethodID;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getAmountExcludingTax() {
		return amountExcludingTax;
	}

	public void setAmountExcludingTax(BigDecimal amountExcludingTax) {
		this.amountExcludingTax = amountExcludingTax;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public BigDecimal getOutstandingBalance() {
		return outstandingBalance;
	}

	public void setOutstandingBalance(BigDecimal outstandingBalance) {
		this.outstandingBalance = outstandingBalance;
	}

	public Date getFinalizationDate() {
		return finalizationDate;
	}

	public void setFinalizationDate(Date finalizationDate) {
		this.finalizationDate = finalizationDate;
	}

	public Boolean getIsFinalized() {
		return isFinalized;
	}

	public void setIsFinalized(Boolean isFinalized) {
		this.isFinalized = isFinalized;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getLastEditedWhen() {
		return lastEditedWhen;
	}

	public void setLastEditedWhen(Date lastEditedWhen) {
		this.lastEditedWhen = lastEditedWhen;
	}

}
