package com.mycompany.myapp.dto;

import java.util.Date;

public class CustomerCategoriesDTO {
	
	private Integer customerCategoryID;
    private String customerCategoryName;
    private int lastEditedBy;
    private Date validFrom;
    private Date validTo;
    
    public CustomerCategoriesDTO() 
	{
	}
    
	public CustomerCategoriesDTO(Integer customerCategoryID, String customerCategoryName, int lastEditedBy, Date validFrom, Date validTo) 
	{
		this.customerCategoryID = customerCategoryID;
		this.customerCategoryName = customerCategoryName;
		this.lastEditedBy = lastEditedBy;
		this.validFrom = validFrom;
		this.validTo = validTo;
	}

	public Integer getCustomerCategoryID() {
		return customerCategoryID;
	}

	public void setCustomerCategoryID(Integer customerCategoryID) {
		this.customerCategoryID = customerCategoryID;
	}

	public String getCustomerCategoryName() {
		return customerCategoryName;
	}

	public void setCustomerCategoryName(String customerCategoryName) {
		this.customerCategoryName = customerCategoryName;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
}
