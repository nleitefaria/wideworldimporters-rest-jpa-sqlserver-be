package com.mycompany.myapp.dto;

import java.util.Date;

public class InvoicesDTO 
{	
	private Integer invoiceID;
    private Date invoiceDate;
    private String customerPurchaseOrderNumber;
    private boolean isCreditNote;
    private String creditNoteReason;
    private String comments;
    private String deliveryInstructions;
    private String internalComments;
    private int totalDryItems;
    private int totalChillerItems;
    private String deliveryRun;   
    private String runPosition; 
    private String returnedDeliveryData;  
    private Date confirmedDeliveryTime;   
    private String confirmedReceivedBy;  
    private int lastEditedBy;   
    private Date lastEditedWhen;
    
	public InvoicesDTO() 
	{	
	}

	public InvoicesDTO(Integer invoiceID, Date invoiceDate, String customerPurchaseOrderNumber, boolean isCreditNote,
			String creditNoteReason, String comments, String deliveryInstructions, String internalComments,
			int totalDryItems, int totalChillerItems, String deliveryRun, String runPosition,
			String returnedDeliveryData, Date confirmedDeliveryTime, String confirmedReceivedBy, int lastEditedBy,
			Date lastEditedWhen) {
		
		this.invoiceID = invoiceID;
		this.invoiceDate = invoiceDate;
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
		this.isCreditNote = isCreditNote;
		this.creditNoteReason = creditNoteReason;
		this.comments = comments;
		this.deliveryInstructions = deliveryInstructions;
		this.internalComments = internalComments;
		this.totalDryItems = totalDryItems;
		this.totalChillerItems = totalChillerItems;
		this.deliveryRun = deliveryRun;
		this.runPosition = runPosition;
		this.returnedDeliveryData = returnedDeliveryData;
		this.confirmedDeliveryTime = confirmedDeliveryTime;
		this.confirmedReceivedBy = confirmedReceivedBy;
		this.lastEditedBy = lastEditedBy;
		this.lastEditedWhen = lastEditedWhen;
	}



	public Integer getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(Integer invoiceID) {
		this.invoiceID = invoiceID;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getCustomerPurchaseOrderNumber() {
		return customerPurchaseOrderNumber;
	}

	public void setCustomerPurchaseOrderNumber(String customerPurchaseOrderNumber) {
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
	}

	public boolean isCreditNote() {
		return isCreditNote;
	}

	public void setCreditNote(boolean isCreditNote) {
		this.isCreditNote = isCreditNote;
	}

	public String getCreditNoteReason() {
		return creditNoteReason;
	}

	public void setCreditNoteReason(String creditNoteReason) {
		this.creditNoteReason = creditNoteReason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDeliveryInstructions() {
		return deliveryInstructions;
	}

	public void setDeliveryInstructions(String deliveryInstructions) {
		this.deliveryInstructions = deliveryInstructions;
	}

	public String getInternalComments() {
		return internalComments;
	}

	public void setInternalComments(String internalComments) {
		this.internalComments = internalComments;
	}

	public int getTotalDryItems() {
		return totalDryItems;
	}

	public void setTotalDryItems(int totalDryItems) {
		this.totalDryItems = totalDryItems;
	}

	public int getTotalChillerItems() {
		return totalChillerItems;
	}

	public void setTotalChillerItems(int totalChillerItems) {
		this.totalChillerItems = totalChillerItems;
	}

	public String getDeliveryRun() {
		return deliveryRun;
	}

	public void setDeliveryRun(String deliveryRun) {
		this.deliveryRun = deliveryRun;
	}

	public String getRunPosition() {
		return runPosition;
	}

	public void setRunPosition(String runPosition) {
		this.runPosition = runPosition;
	}

	public String getReturnedDeliveryData() {
		return returnedDeliveryData;
	}

	public void setReturnedDeliveryData(String returnedDeliveryData) {
		this.returnedDeliveryData = returnedDeliveryData;
	}

	public Date getConfirmedDeliveryTime() {
		return confirmedDeliveryTime;
	}

	public void setConfirmedDeliveryTime(Date confirmedDeliveryTime) {
		this.confirmedDeliveryTime = confirmedDeliveryTime;
	}

	public String getConfirmedReceivedBy() {
		return confirmedReceivedBy;
	}

	public void setConfirmedReceivedBy(String confirmedReceivedBy) {
		this.confirmedReceivedBy = confirmedReceivedBy;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getLastEditedWhen() {
		return lastEditedWhen;
	}

	public void setLastEditedWhen(Date lastEditedWhen) {
		this.lastEditedWhen = lastEditedWhen;
	}
}
