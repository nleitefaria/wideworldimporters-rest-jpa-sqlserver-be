package com.mycompany.myapp.dto;

import java.util.List;

public class PagingDTO {
	
	private List content;				
	private int totalElements;
	
	public PagingDTO()
	{		
	}
	
	public PagingDTO(List content, int totalElements)
	{
		this.content = content;
		this.totalElements = totalElements;
	}
	
	public List getContent() {
		return content;
	}
	
	public void setContent(List content) {
		this.content = content;
	}
	
	public int getTotalElements() {
		return totalElements;
	}
	
	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
	
	

}
