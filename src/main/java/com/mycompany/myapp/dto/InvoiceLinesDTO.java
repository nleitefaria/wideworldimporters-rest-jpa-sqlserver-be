package com.mycompany.myapp.dto;

import java.math.BigDecimal;
import java.util.Date;


public class InvoiceLinesDTO 
{	
	private Integer invoiceLineID;
    private String description;
    private int packageTypeID;
    private int quantity;
    private BigDecimal unitPrice;
    private BigDecimal taxRate;
    private BigDecimal taxAmount;
    private BigDecimal lineProfit;
    private BigDecimal extendedPrice;
    private int lastEditedBy;
    private Date lastEditedWhen;
       
	public InvoiceLinesDTO()
	{		
	}

	public InvoiceLinesDTO(Integer invoiceLineID, String description, int packageTypeID, int quantity, BigDecimal unitPrice, BigDecimal taxRate, BigDecimal taxAmount, BigDecimal lineProfit, BigDecimal extendedPrice, int lastEditedBy, Date lastEditedWhen) 
	{		
		this.invoiceLineID = invoiceLineID;
		this.description = description;
		this.packageTypeID = packageTypeID;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.taxRate = taxRate;
		this.taxAmount = taxAmount;
		this.lineProfit = lineProfit;
		this.extendedPrice = extendedPrice;
		this.lastEditedBy = lastEditedBy;
		this.lastEditedWhen = lastEditedWhen;
	}

	public Integer getInvoiceLineID() {
		return invoiceLineID;
	}

	public void setInvoiceLineID(Integer invoiceLineID) {
		this.invoiceLineID = invoiceLineID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPackageTypeID() {
		return packageTypeID;
	}

	public void setPackageTypeID(int packageTypeID) {
		this.packageTypeID = packageTypeID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public BigDecimal getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate = taxRate;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getLineProfit() {
		return lineProfit;
	}

	public void setLineProfit(BigDecimal lineProfit) {
		this.lineProfit = lineProfit;
	}

	public BigDecimal getExtendedPrice() {
		return extendedPrice;
	}

	public void setExtendedPrice(BigDecimal extendedPrice) {
		this.extendedPrice = extendedPrice;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getLastEditedWhen() {
		return lastEditedWhen;
	}

	public void setLastEditedWhen(Date lastEditedWhen) {
		this.lastEditedWhen = lastEditedWhen;
	}

}
