package com.mycompany.myapp.dto;

import java.util.Date;


public class OrdersDTO 
{

	private Integer orderID;
    private Date orderDate;
    private Date expectedDeliveryDate;
    private String customerPurchaseOrderNumber;
    private boolean isUndersupplyBackordered;    
    private String comments;    
    private String deliveryInstructions;   
    private String internalComments;    
    private Date pickingCompletedWhen;   
    private int lastEditedBy;   
    private Date lastEditedWhen;
     
	public OrdersDTO() {
		
	}

	public OrdersDTO(Integer orderID, Date orderDate, Date expectedDeliveryDate, String customerPurchaseOrderNumber,
			boolean isUndersupplyBackordered, String comments, String deliveryInstructions, String internalComments,
			Date pickingCompletedWhen, int lastEditedBy, Date lastEditedWhen) {
		
		this.orderID = orderID;
		this.orderDate = orderDate;
		this.expectedDeliveryDate = expectedDeliveryDate;
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
		this.isUndersupplyBackordered = isUndersupplyBackordered;
		this.comments = comments;
		this.deliveryInstructions = deliveryInstructions;
		this.internalComments = internalComments;
		this.pickingCompletedWhen = pickingCompletedWhen;
		this.lastEditedBy = lastEditedBy;
		this.lastEditedWhen = lastEditedWhen;
	}

	public Integer getOrderID() {
		return orderID;
	}

	public void setOrderID(Integer orderID) {
		this.orderID = orderID;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}

	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}

	public String getCustomerPurchaseOrderNumber() {
		return customerPurchaseOrderNumber;
	}

	public void setCustomerPurchaseOrderNumber(String customerPurchaseOrderNumber) {
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
	}

	public boolean isUndersupplyBackordered() {
		return isUndersupplyBackordered;
	}

	public void setUndersupplyBackordered(boolean isUndersupplyBackordered) {
		this.isUndersupplyBackordered = isUndersupplyBackordered;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDeliveryInstructions() {
		return deliveryInstructions;
	}

	public void setDeliveryInstructions(String deliveryInstructions) {
		this.deliveryInstructions = deliveryInstructions;
	}

	public String getInternalComments() {
		return internalComments;
	}

	public void setInternalComments(String internalComments) {
		this.internalComments = internalComments;
	}

	public Date getPickingCompletedWhen() {
		return pickingCompletedWhen;
	}

	public void setPickingCompletedWhen(Date pickingCompletedWhen) {
		this.pickingCompletedWhen = pickingCompletedWhen;
	}

	public int getLastEditedBy() {
		return lastEditedBy;
	}

	public void setLastEditedBy(int lastEditedBy) {
		this.lastEditedBy = lastEditedBy;
	}

	public Date getLastEditedWhen() {
		return lastEditedWhen;
	}

	public void setLastEditedWhen(Date lastEditedWhen) {
		this.lastEditedWhen = lastEditedWhen;
	}

}
