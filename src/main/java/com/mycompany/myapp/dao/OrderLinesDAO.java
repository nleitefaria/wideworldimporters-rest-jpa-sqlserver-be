package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.OrderLines;

public interface OrderLinesDAO {
	
	int getOrderLinesCount();
	OrderLines findOrderLines(Integer id);
	List<OrderLines> findOrderLinesEntitiesWithPaging(int pageNo);

}
