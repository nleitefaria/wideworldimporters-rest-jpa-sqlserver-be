package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.BuyingGroups;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

public interface BuyingGroupsDAO 
{	
	int getBuyingGroupsCount();
	BuyingGroups findBuyingGroups(Integer id);
	List<BuyingGroups> findBuyingGroupsEntities();
	List<BuyingGroups> findSpecialDealsEntitiesWithPaging(int pageNo);
	void create(BuyingGroups buyingGroups) throws PreexistingEntityException, Exception;

}
