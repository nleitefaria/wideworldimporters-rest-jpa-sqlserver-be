package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.CustomerCategories;

public interface CustomerCategoriesDAO 
{
	int getCustomerCategoriesCount();
	CustomerCategories findCustomerCategories(Integer id);
	List<CustomerCategories> findCustomerCategoriesEntities();

}
