package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.InvoiceLines;

public interface InvoiceLinesDAO {
	
	int getInvoiceLinesCount();
	InvoiceLines findInvoiceLines(Integer id);
	List<InvoiceLines> findInvoiceLinesEntities();
	List<InvoiceLines> findInvoiceLinesEntities(int maxResults, int firstResult);

}
