package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Orders;

public interface OrdersDAO 
{
	int getOrdersCount();
	Orders findOrders(Integer id);
	List<Orders> findOrdersEntitiesWithPaging(int pageNo);

}
