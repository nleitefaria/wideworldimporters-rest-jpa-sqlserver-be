package com.mycompany.myapp.dao;

import java.util.List;


import com.mycompany.myapp.entity.Invoices;

public interface InvoicesDAO 
{	
	int getInvoicesCount();
	Invoices findInvoices(Integer id);
	List<Invoices> findInvoicesEntities();
	List<Invoices> findInvoicesEntitiesWithPaging(int pageNo);


}
