/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.OrdersDAO;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.Invoices;
import com.mycompany.myapp.entity.OrderLines;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.exceptions.IllegalOrphanException;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author nleit_000
 */
public class OrdersDAOImpl implements OrdersDAO, Serializable {

    public OrdersDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Orders orders) throws PreexistingEntityException, Exception {
        if (orders.getOrdersList() == null) {
            orders.setOrdersList(new ArrayList<Orders>());
        }
        if (orders.getOrderLinesList() == null) {
            orders.setOrderLinesList(new ArrayList<OrderLines>());
        }
        if (orders.getInvoicesList() == null) {
            orders.setInvoicesList(new ArrayList<Invoices>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customers customerID = orders.getCustomerID();
            if (customerID != null) {
                customerID = em.getReference(customerID.getClass(), customerID.getCustomerID());
                orders.setCustomerID(customerID);
            }
            Orders backorderOrderID = orders.getBackorderOrderID();
            if (backorderOrderID != null) {
                backorderOrderID = em.getReference(backorderOrderID.getClass(), backorderOrderID.getOrderID());
                orders.setBackorderOrderID(backorderOrderID);
            }
            List<Orders> attachedOrdersList = new ArrayList<Orders>();
            for (Orders ordersListOrdersToAttach : orders.getOrdersList()) {
                ordersListOrdersToAttach = em.getReference(ordersListOrdersToAttach.getClass(), ordersListOrdersToAttach.getOrderID());
                attachedOrdersList.add(ordersListOrdersToAttach);
            }
            orders.setOrdersList(attachedOrdersList);
            List<OrderLines> attachedOrderLinesList = new ArrayList<OrderLines>();
            for (OrderLines orderLinesListOrderLinesToAttach : orders.getOrderLinesList()) {
                orderLinesListOrderLinesToAttach = em.getReference(orderLinesListOrderLinesToAttach.getClass(), orderLinesListOrderLinesToAttach.getOrderLineID());
                attachedOrderLinesList.add(orderLinesListOrderLinesToAttach);
            }
            orders.setOrderLinesList(attachedOrderLinesList);
            List<Invoices> attachedInvoicesList = new ArrayList<Invoices>();
            for (Invoices invoicesListInvoicesToAttach : orders.getInvoicesList()) {
                invoicesListInvoicesToAttach = em.getReference(invoicesListInvoicesToAttach.getClass(), invoicesListInvoicesToAttach.getInvoiceID());
                attachedInvoicesList.add(invoicesListInvoicesToAttach);
            }
            orders.setInvoicesList(attachedInvoicesList);
            em.persist(orders);
            if (customerID != null) {
                customerID.getOrdersList().add(orders);
                customerID = em.merge(customerID);
            }
            if (backorderOrderID != null) {
                backorderOrderID.getOrdersList().add(orders);
                backorderOrderID = em.merge(backorderOrderID);
            }
            for (Orders ordersListOrders : orders.getOrdersList()) {
                Orders oldBackorderOrderIDOfOrdersListOrders = ordersListOrders.getBackorderOrderID();
                ordersListOrders.setBackorderOrderID(orders);
                ordersListOrders = em.merge(ordersListOrders);
                if (oldBackorderOrderIDOfOrdersListOrders != null) {
                    oldBackorderOrderIDOfOrdersListOrders.getOrdersList().remove(ordersListOrders);
                    oldBackorderOrderIDOfOrdersListOrders = em.merge(oldBackorderOrderIDOfOrdersListOrders);
                }
            }
            for (OrderLines orderLinesListOrderLines : orders.getOrderLinesList()) {
                Orders oldOrderIDOfOrderLinesListOrderLines = orderLinesListOrderLines.getOrderID();
                orderLinesListOrderLines.setOrderID(orders);
                orderLinesListOrderLines = em.merge(orderLinesListOrderLines);
                if (oldOrderIDOfOrderLinesListOrderLines != null) {
                    oldOrderIDOfOrderLinesListOrderLines.getOrderLinesList().remove(orderLinesListOrderLines);
                    oldOrderIDOfOrderLinesListOrderLines = em.merge(oldOrderIDOfOrderLinesListOrderLines);
                }
            }
            for (Invoices invoicesListInvoices : orders.getInvoicesList()) {
                Orders oldOrderIDOfInvoicesListInvoices = invoicesListInvoices.getOrderID();
                invoicesListInvoices.setOrderID(orders);
                invoicesListInvoices = em.merge(invoicesListInvoices);
                if (oldOrderIDOfInvoicesListInvoices != null) {
                    oldOrderIDOfInvoicesListInvoices.getInvoicesList().remove(invoicesListInvoices);
                    oldOrderIDOfInvoicesListInvoices = em.merge(oldOrderIDOfInvoicesListInvoices);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOrders(orders.getOrderID()) != null) {
                throw new PreexistingEntityException("Orders " + orders + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Orders orders) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Orders persistentOrders = em.find(Orders.class, orders.getOrderID());
            Customers customerIDOld = persistentOrders.getCustomerID();
            Customers customerIDNew = orders.getCustomerID();
            Orders backorderOrderIDOld = persistentOrders.getBackorderOrderID();
            Orders backorderOrderIDNew = orders.getBackorderOrderID();
            List<Orders> ordersListOld = persistentOrders.getOrdersList();
            List<Orders> ordersListNew = orders.getOrdersList();
            List<OrderLines> orderLinesListOld = persistentOrders.getOrderLinesList();
            List<OrderLines> orderLinesListNew = orders.getOrderLinesList();
            List<Invoices> invoicesListOld = persistentOrders.getInvoicesList();
            List<Invoices> invoicesListNew = orders.getInvoicesList();
            List<String> illegalOrphanMessages = null;
            for (OrderLines orderLinesListOldOrderLines : orderLinesListOld) {
                if (!orderLinesListNew.contains(orderLinesListOldOrderLines)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OrderLines " + orderLinesListOldOrderLines + " since its orderID field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (customerIDNew != null) {
                customerIDNew = em.getReference(customerIDNew.getClass(), customerIDNew.getCustomerID());
                orders.setCustomerID(customerIDNew);
            }
            if (backorderOrderIDNew != null) {
                backorderOrderIDNew = em.getReference(backorderOrderIDNew.getClass(), backorderOrderIDNew.getOrderID());
                orders.setBackorderOrderID(backorderOrderIDNew);
            }
            List<Orders> attachedOrdersListNew = new ArrayList<Orders>();
            for (Orders ordersListNewOrdersToAttach : ordersListNew) {
                ordersListNewOrdersToAttach = em.getReference(ordersListNewOrdersToAttach.getClass(), ordersListNewOrdersToAttach.getOrderID());
                attachedOrdersListNew.add(ordersListNewOrdersToAttach);
            }
            ordersListNew = attachedOrdersListNew;
            orders.setOrdersList(ordersListNew);
            List<OrderLines> attachedOrderLinesListNew = new ArrayList<OrderLines>();
            for (OrderLines orderLinesListNewOrderLinesToAttach : orderLinesListNew) {
                orderLinesListNewOrderLinesToAttach = em.getReference(orderLinesListNewOrderLinesToAttach.getClass(), orderLinesListNewOrderLinesToAttach.getOrderLineID());
                attachedOrderLinesListNew.add(orderLinesListNewOrderLinesToAttach);
            }
            orderLinesListNew = attachedOrderLinesListNew;
            orders.setOrderLinesList(orderLinesListNew);
            List<Invoices> attachedInvoicesListNew = new ArrayList<Invoices>();
            for (Invoices invoicesListNewInvoicesToAttach : invoicesListNew) {
                invoicesListNewInvoicesToAttach = em.getReference(invoicesListNewInvoicesToAttach.getClass(), invoicesListNewInvoicesToAttach.getInvoiceID());
                attachedInvoicesListNew.add(invoicesListNewInvoicesToAttach);
            }
            invoicesListNew = attachedInvoicesListNew;
            orders.setInvoicesList(invoicesListNew);
            orders = em.merge(orders);
            if (customerIDOld != null && !customerIDOld.equals(customerIDNew)) {
                customerIDOld.getOrdersList().remove(orders);
                customerIDOld = em.merge(customerIDOld);
            }
            if (customerIDNew != null && !customerIDNew.equals(customerIDOld)) {
                customerIDNew.getOrdersList().add(orders);
                customerIDNew = em.merge(customerIDNew);
            }
            if (backorderOrderIDOld != null && !backorderOrderIDOld.equals(backorderOrderIDNew)) {
                backorderOrderIDOld.getOrdersList().remove(orders);
                backorderOrderIDOld = em.merge(backorderOrderIDOld);
            }
            if (backorderOrderIDNew != null && !backorderOrderIDNew.equals(backorderOrderIDOld)) {
                backorderOrderIDNew.getOrdersList().add(orders);
                backorderOrderIDNew = em.merge(backorderOrderIDNew);
            }
            for (Orders ordersListOldOrders : ordersListOld) {
                if (!ordersListNew.contains(ordersListOldOrders)) {
                    ordersListOldOrders.setBackorderOrderID(null);
                    ordersListOldOrders = em.merge(ordersListOldOrders);
                }
            }
            for (Orders ordersListNewOrders : ordersListNew) {
                if (!ordersListOld.contains(ordersListNewOrders)) {
                    Orders oldBackorderOrderIDOfOrdersListNewOrders = ordersListNewOrders.getBackorderOrderID();
                    ordersListNewOrders.setBackorderOrderID(orders);
                    ordersListNewOrders = em.merge(ordersListNewOrders);
                    if (oldBackorderOrderIDOfOrdersListNewOrders != null && !oldBackorderOrderIDOfOrdersListNewOrders.equals(orders)) {
                        oldBackorderOrderIDOfOrdersListNewOrders.getOrdersList().remove(ordersListNewOrders);
                        oldBackorderOrderIDOfOrdersListNewOrders = em.merge(oldBackorderOrderIDOfOrdersListNewOrders);
                    }
                }
            }
            for (OrderLines orderLinesListNewOrderLines : orderLinesListNew) {
                if (!orderLinesListOld.contains(orderLinesListNewOrderLines)) {
                    Orders oldOrderIDOfOrderLinesListNewOrderLines = orderLinesListNewOrderLines.getOrderID();
                    orderLinesListNewOrderLines.setOrderID(orders);
                    orderLinesListNewOrderLines = em.merge(orderLinesListNewOrderLines);
                    if (oldOrderIDOfOrderLinesListNewOrderLines != null && !oldOrderIDOfOrderLinesListNewOrderLines.equals(orders)) {
                        oldOrderIDOfOrderLinesListNewOrderLines.getOrderLinesList().remove(orderLinesListNewOrderLines);
                        oldOrderIDOfOrderLinesListNewOrderLines = em.merge(oldOrderIDOfOrderLinesListNewOrderLines);
                    }
                }
            }
            for (Invoices invoicesListOldInvoices : invoicesListOld) {
                if (!invoicesListNew.contains(invoicesListOldInvoices)) {
                    invoicesListOldInvoices.setOrderID(null);
                    invoicesListOldInvoices = em.merge(invoicesListOldInvoices);
                }
            }
            for (Invoices invoicesListNewInvoices : invoicesListNew) {
                if (!invoicesListOld.contains(invoicesListNewInvoices)) {
                    Orders oldOrderIDOfInvoicesListNewInvoices = invoicesListNewInvoices.getOrderID();
                    invoicesListNewInvoices.setOrderID(orders);
                    invoicesListNewInvoices = em.merge(invoicesListNewInvoices);
                    if (oldOrderIDOfInvoicesListNewInvoices != null && !oldOrderIDOfInvoicesListNewInvoices.equals(orders)) {
                        oldOrderIDOfInvoicesListNewInvoices.getInvoicesList().remove(invoicesListNewInvoices);
                        oldOrderIDOfInvoicesListNewInvoices = em.merge(oldOrderIDOfInvoicesListNewInvoices);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = orders.getOrderID();
                if (findOrders(id) == null) {
                    throw new NonexistentEntityException("The orders with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Orders orders;
            try {
                orders = em.getReference(Orders.class, id);
                orders.getOrderID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orders with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<OrderLines> orderLinesListOrphanCheck = orders.getOrderLinesList();
            for (OrderLines orderLinesListOrphanCheckOrderLines : orderLinesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Orders (" + orders + ") cannot be destroyed since the OrderLines " + orderLinesListOrphanCheckOrderLines + " in its orderLinesList field has a non-nullable orderID field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Customers customerID = orders.getCustomerID();
            if (customerID != null) {
                customerID.getOrdersList().remove(orders);
                customerID = em.merge(customerID);
            }
            Orders backorderOrderID = orders.getBackorderOrderID();
            if (backorderOrderID != null) {
                backorderOrderID.getOrdersList().remove(orders);
                backorderOrderID = em.merge(backorderOrderID);
            }
            List<Orders> ordersList = orders.getOrdersList();
            for (Orders ordersListOrders : ordersList) {
                ordersListOrders.setBackorderOrderID(null);
                ordersListOrders = em.merge(ordersListOrders);
            }
            List<Invoices> invoicesList = orders.getInvoicesList();
            for (Invoices invoicesListInvoices : invoicesList) {
                invoicesListInvoices.setOrderID(null);
                invoicesListInvoices = em.merge(invoicesListInvoices);
            }
            em.remove(orders);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Orders> findOrdersEntities() {
        return findOrdersEntities(true, -1, -1);
    }

    public List<Orders> findOrdersEntities(int maxResults, int firstResult) {
        return findOrdersEntities(false, maxResults, firstResult);
    }
    
    public List<Orders> findOrdersEntitiesWithPaging(int pageNo)
    { 	
    	return findOrdersEntities(false, 10, pageNo * 10);    
    }


    private List<Orders> findOrdersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Orders.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Orders findOrders(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Orders.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrdersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Orders> rt = cq.from(Orders.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
