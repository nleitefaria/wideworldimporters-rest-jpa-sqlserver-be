/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.CustomerTransactionsDAO;
import com.mycompany.myapp.entity.CustomerTransactions;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.Invoices;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;


/**
 *
 * @author nleit_000
 */
public class CustomerTransactionsDAOImpl implements CustomerTransactionsDAO, Serializable {

    public CustomerTransactionsDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CustomerTransactions customerTransactions) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customers customerID = customerTransactions.getCustomerID();
            if (customerID != null) {
                customerID = em.getReference(customerID.getClass(), customerID.getCustomerID());
                customerTransactions.setCustomerID(customerID);
            }
            Invoices invoiceID = customerTransactions.getInvoiceID();
            if (invoiceID != null) {
                invoiceID = em.getReference(invoiceID.getClass(), invoiceID.getInvoiceID());
                customerTransactions.setInvoiceID(invoiceID);
            }
            em.persist(customerTransactions);
            if (customerID != null) {
                customerID.getCustomerTransactionsList().add(customerTransactions);
                customerID = em.merge(customerID);
            }
            if (invoiceID != null) {
                invoiceID.getCustomerTransactionsList().add(customerTransactions);
                invoiceID = em.merge(invoiceID);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCustomerTransactions(customerTransactions.getCustomerTransactionID()) != null) {
                throw new PreexistingEntityException("CustomerTransactions " + customerTransactions + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CustomerTransactions customerTransactions) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerTransactions persistentCustomerTransactions = em.find(CustomerTransactions.class, customerTransactions.getCustomerTransactionID());
            Customers customerIDOld = persistentCustomerTransactions.getCustomerID();
            Customers customerIDNew = customerTransactions.getCustomerID();
            Invoices invoiceIDOld = persistentCustomerTransactions.getInvoiceID();
            Invoices invoiceIDNew = customerTransactions.getInvoiceID();
            if (customerIDNew != null) {
                customerIDNew = em.getReference(customerIDNew.getClass(), customerIDNew.getCustomerID());
                customerTransactions.setCustomerID(customerIDNew);
            }
            if (invoiceIDNew != null) {
                invoiceIDNew = em.getReference(invoiceIDNew.getClass(), invoiceIDNew.getInvoiceID());
                customerTransactions.setInvoiceID(invoiceIDNew);
            }
            customerTransactions = em.merge(customerTransactions);
            if (customerIDOld != null && !customerIDOld.equals(customerIDNew)) {
                customerIDOld.getCustomerTransactionsList().remove(customerTransactions);
                customerIDOld = em.merge(customerIDOld);
            }
            if (customerIDNew != null && !customerIDNew.equals(customerIDOld)) {
                customerIDNew.getCustomerTransactionsList().add(customerTransactions);
                customerIDNew = em.merge(customerIDNew);
            }
            if (invoiceIDOld != null && !invoiceIDOld.equals(invoiceIDNew)) {
                invoiceIDOld.getCustomerTransactionsList().remove(customerTransactions);
                invoiceIDOld = em.merge(invoiceIDOld);
            }
            if (invoiceIDNew != null && !invoiceIDNew.equals(invoiceIDOld)) {
                invoiceIDNew.getCustomerTransactionsList().add(customerTransactions);
                invoiceIDNew = em.merge(invoiceIDNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = customerTransactions.getCustomerTransactionID();
                if (findCustomerTransactions(id) == null) {
                    throw new NonexistentEntityException("The customerTransactions with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerTransactions customerTransactions;
            try {
                customerTransactions = em.getReference(CustomerTransactions.class, id);
                customerTransactions.getCustomerTransactionID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The customerTransactions with id " + id + " no longer exists.", enfe);
            }
            Customers customerID = customerTransactions.getCustomerID();
            if (customerID != null) {
                customerID.getCustomerTransactionsList().remove(customerTransactions);
                customerID = em.merge(customerID);
            }
            Invoices invoiceID = customerTransactions.getInvoiceID();
            if (invoiceID != null) {
                invoiceID.getCustomerTransactionsList().remove(customerTransactions);
                invoiceID = em.merge(invoiceID);
            }
            em.remove(customerTransactions);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CustomerTransactions> findCustomerTransactionsEntities() {
        return findCustomerTransactionsEntities(true, -1, -1);
    }

    public List<CustomerTransactions> findCustomerTransactionsEntities(int maxResults, int firstResult) {
        return findCustomerTransactionsEntities(false, maxResults, firstResult);
    }

    private List<CustomerTransactions> findCustomerTransactionsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CustomerTransactions.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CustomerTransactions findCustomerTransactions(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CustomerTransactions.class, id);
        } finally {
            em.close();
        }
    }

    public int getCustomerTransactionsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CustomerTransactions> rt = cq.from(CustomerTransactions.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
