/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.CustomersDAO;
import com.mycompany.myapp.entity.BuyingGroups;
import com.mycompany.myapp.entity.CustomerCategories;
import com.mycompany.myapp.entity.CustomerTransactions;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.Invoices;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.entity.SpecialDeals;
import com.mycompany.myapp.exceptions.IllegalOrphanException;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;


/**
 *
 * @author nleit_000
 */
public class CustomersDAOImpl implements Serializable, CustomersDAO  {

    public CustomersDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Customers customers) throws PreexistingEntityException, Exception {
        if (customers.getOrdersList() == null) {
            customers.setOrdersList(new ArrayList<Orders>());
        }
        if (customers.getCustomersList() == null) {
            customers.setCustomersList(new ArrayList<Customers>());
        }
        if (customers.getSpecialDealsList() == null) {
            customers.setSpecialDealsList(new ArrayList<SpecialDeals>());
        }
        if (customers.getCustomerTransactionsList() == null) {
            customers.setCustomerTransactionsList(new ArrayList<CustomerTransactions>());
        }
        if (customers.getInvoicesList() == null) {
            customers.setInvoicesList(new ArrayList<Invoices>());
        }
        if (customers.getInvoicesList1() == null) {
            customers.setInvoicesList1(new ArrayList<Invoices>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BuyingGroups buyingGroupID = customers.getBuyingGroupID();
            if (buyingGroupID != null) {
                buyingGroupID = em.getReference(buyingGroupID.getClass(), buyingGroupID.getBuyingGroupID());
                customers.setBuyingGroupID(buyingGroupID);
            }
            CustomerCategories customerCategoryID = customers.getCustomerCategoryID();
            if (customerCategoryID != null) {
                customerCategoryID = em.getReference(customerCategoryID.getClass(), customerCategoryID.getCustomerCategoryID());
                customers.setCustomerCategoryID(customerCategoryID);
            }
            Customers billToCustomerID = customers.getBillToCustomerID();
            if (billToCustomerID != null) {
                billToCustomerID = em.getReference(billToCustomerID.getClass(), billToCustomerID.getCustomerID());
                customers.setBillToCustomerID(billToCustomerID);
            }
            List<Orders> attachedOrdersList = new ArrayList<Orders>();
            for (Orders ordersListOrdersToAttach : customers.getOrdersList()) {
                ordersListOrdersToAttach = em.getReference(ordersListOrdersToAttach.getClass(), ordersListOrdersToAttach.getOrderID());
                attachedOrdersList.add(ordersListOrdersToAttach);
            }
            customers.setOrdersList(attachedOrdersList);
            List<Customers> attachedCustomersList = new ArrayList<Customers>();
            for (Customers customersListCustomersToAttach : customers.getCustomersList()) {
                customersListCustomersToAttach = em.getReference(customersListCustomersToAttach.getClass(), customersListCustomersToAttach.getCustomerID());
                attachedCustomersList.add(customersListCustomersToAttach);
            }
            customers.setCustomersList(attachedCustomersList);
            List<SpecialDeals> attachedSpecialDealsList = new ArrayList<SpecialDeals>();
            for (SpecialDeals specialDealsListSpecialDealsToAttach : customers.getSpecialDealsList()) {
                specialDealsListSpecialDealsToAttach = em.getReference(specialDealsListSpecialDealsToAttach.getClass(), specialDealsListSpecialDealsToAttach.getSpecialDealID());
                attachedSpecialDealsList.add(specialDealsListSpecialDealsToAttach);
            }
            customers.setSpecialDealsList(attachedSpecialDealsList);
            List<CustomerTransactions> attachedCustomerTransactionsList = new ArrayList<CustomerTransactions>();
            for (CustomerTransactions customerTransactionsListCustomerTransactionsToAttach : customers.getCustomerTransactionsList()) {
                customerTransactionsListCustomerTransactionsToAttach = em.getReference(customerTransactionsListCustomerTransactionsToAttach.getClass(), customerTransactionsListCustomerTransactionsToAttach.getCustomerTransactionID());
                attachedCustomerTransactionsList.add(customerTransactionsListCustomerTransactionsToAttach);
            }
            customers.setCustomerTransactionsList(attachedCustomerTransactionsList);
            List<Invoices> attachedInvoicesList = new ArrayList<Invoices>();
            for (Invoices invoicesListInvoicesToAttach : customers.getInvoicesList()) {
                invoicesListInvoicesToAttach = em.getReference(invoicesListInvoicesToAttach.getClass(), invoicesListInvoicesToAttach.getInvoiceID());
                attachedInvoicesList.add(invoicesListInvoicesToAttach);
            }
            customers.setInvoicesList(attachedInvoicesList);
            List<Invoices> attachedInvoicesList1 = new ArrayList<Invoices>();
            for (Invoices invoicesList1InvoicesToAttach : customers.getInvoicesList1()) {
                invoicesList1InvoicesToAttach = em.getReference(invoicesList1InvoicesToAttach.getClass(), invoicesList1InvoicesToAttach.getInvoiceID());
                attachedInvoicesList1.add(invoicesList1InvoicesToAttach);
            }
            customers.setInvoicesList1(attachedInvoicesList1);
            em.persist(customers);
            if (buyingGroupID != null) {
                buyingGroupID.getCustomersList().add(customers);
                buyingGroupID = em.merge(buyingGroupID);
            }
            if (customerCategoryID != null) {
                customerCategoryID.getCustomersList().add(customers);
                customerCategoryID = em.merge(customerCategoryID);
            }
            if (billToCustomerID != null) {
                billToCustomerID.getCustomersList().add(customers);
                billToCustomerID = em.merge(billToCustomerID);
            }
            for (Orders ordersListOrders : customers.getOrdersList()) {
                Customers oldCustomerIDOfOrdersListOrders = ordersListOrders.getCustomerID();
                ordersListOrders.setCustomerID(customers);
                ordersListOrders = em.merge(ordersListOrders);
                if (oldCustomerIDOfOrdersListOrders != null) {
                    oldCustomerIDOfOrdersListOrders.getOrdersList().remove(ordersListOrders);
                    oldCustomerIDOfOrdersListOrders = em.merge(oldCustomerIDOfOrdersListOrders);
                }
            }
            for (Customers customersListCustomers : customers.getCustomersList()) {
                Customers oldBillToCustomerIDOfCustomersListCustomers = customersListCustomers.getBillToCustomerID();
                customersListCustomers.setBillToCustomerID(customers);
                customersListCustomers = em.merge(customersListCustomers);
                if (oldBillToCustomerIDOfCustomersListCustomers != null) {
                    oldBillToCustomerIDOfCustomersListCustomers.getCustomersList().remove(customersListCustomers);
                    oldBillToCustomerIDOfCustomersListCustomers = em.merge(oldBillToCustomerIDOfCustomersListCustomers);
                }
            }
            for (SpecialDeals specialDealsListSpecialDeals : customers.getSpecialDealsList()) {
                Customers oldCustomerIDOfSpecialDealsListSpecialDeals = specialDealsListSpecialDeals.getCustomerID();
                specialDealsListSpecialDeals.setCustomerID(customers);
                specialDealsListSpecialDeals = em.merge(specialDealsListSpecialDeals);
                if (oldCustomerIDOfSpecialDealsListSpecialDeals != null) {
                    oldCustomerIDOfSpecialDealsListSpecialDeals.getSpecialDealsList().remove(specialDealsListSpecialDeals);
                    oldCustomerIDOfSpecialDealsListSpecialDeals = em.merge(oldCustomerIDOfSpecialDealsListSpecialDeals);
                }
            }
            for (CustomerTransactions customerTransactionsListCustomerTransactions : customers.getCustomerTransactionsList()) {
                Customers oldCustomerIDOfCustomerTransactionsListCustomerTransactions = customerTransactionsListCustomerTransactions.getCustomerID();
                customerTransactionsListCustomerTransactions.setCustomerID(customers);
                customerTransactionsListCustomerTransactions = em.merge(customerTransactionsListCustomerTransactions);
                if (oldCustomerIDOfCustomerTransactionsListCustomerTransactions != null) {
                    oldCustomerIDOfCustomerTransactionsListCustomerTransactions.getCustomerTransactionsList().remove(customerTransactionsListCustomerTransactions);
                    oldCustomerIDOfCustomerTransactionsListCustomerTransactions = em.merge(oldCustomerIDOfCustomerTransactionsListCustomerTransactions);
                }
            }
            for (Invoices invoicesListInvoices : customers.getInvoicesList()) {
                Customers oldCustomerIDOfInvoicesListInvoices = invoicesListInvoices.getCustomerID();
                invoicesListInvoices.setCustomerID(customers);
                invoicesListInvoices = em.merge(invoicesListInvoices);
                if (oldCustomerIDOfInvoicesListInvoices != null) {
                    oldCustomerIDOfInvoicesListInvoices.getInvoicesList().remove(invoicesListInvoices);
                    oldCustomerIDOfInvoicesListInvoices = em.merge(oldCustomerIDOfInvoicesListInvoices);
                }
            }
            for (Invoices invoicesList1Invoices : customers.getInvoicesList1()) {
                Customers oldBillToCustomerIDOfInvoicesList1Invoices = invoicesList1Invoices.getBillToCustomerID();
                invoicesList1Invoices.setBillToCustomerID(customers);
                invoicesList1Invoices = em.merge(invoicesList1Invoices);
                if (oldBillToCustomerIDOfInvoicesList1Invoices != null) {
                    oldBillToCustomerIDOfInvoicesList1Invoices.getInvoicesList1().remove(invoicesList1Invoices);
                    oldBillToCustomerIDOfInvoicesList1Invoices = em.merge(oldBillToCustomerIDOfInvoicesList1Invoices);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCustomers(customers.getCustomerID()) != null) {
                throw new PreexistingEntityException("Customers " + customers + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Customers customers) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customers persistentCustomers = em.find(Customers.class, customers.getCustomerID());
            BuyingGroups buyingGroupIDOld = persistentCustomers.getBuyingGroupID();
            BuyingGroups buyingGroupIDNew = customers.getBuyingGroupID();
            CustomerCategories customerCategoryIDOld = persistentCustomers.getCustomerCategoryID();
            CustomerCategories customerCategoryIDNew = customers.getCustomerCategoryID();
            Customers billToCustomerIDOld = persistentCustomers.getBillToCustomerID();
            Customers billToCustomerIDNew = customers.getBillToCustomerID();
            List<Orders> ordersListOld = persistentCustomers.getOrdersList();
            List<Orders> ordersListNew = customers.getOrdersList();
            List<Customers> customersListOld = persistentCustomers.getCustomersList();
            List<Customers> customersListNew = customers.getCustomersList();
            List<SpecialDeals> specialDealsListOld = persistentCustomers.getSpecialDealsList();
            List<SpecialDeals> specialDealsListNew = customers.getSpecialDealsList();
            List<CustomerTransactions> customerTransactionsListOld = persistentCustomers.getCustomerTransactionsList();
            List<CustomerTransactions> customerTransactionsListNew = customers.getCustomerTransactionsList();
            List<Invoices> invoicesListOld = persistentCustomers.getInvoicesList();
            List<Invoices> invoicesListNew = customers.getInvoicesList();
            List<Invoices> invoicesList1Old = persistentCustomers.getInvoicesList1();
            List<Invoices> invoicesList1New = customers.getInvoicesList1();
            List<String> illegalOrphanMessages = null;
            for (Orders ordersListOldOrders : ordersListOld) {
                if (!ordersListNew.contains(ordersListOldOrders)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Orders " + ordersListOldOrders + " since its customerID field is not nullable.");
                }
            }
            for (Customers customersListOldCustomers : customersListOld) {
                if (!customersListNew.contains(customersListOldCustomers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Customers " + customersListOldCustomers + " since its billToCustomerID field is not nullable.");
                }
            }
            for (CustomerTransactions customerTransactionsListOldCustomerTransactions : customerTransactionsListOld) {
                if (!customerTransactionsListNew.contains(customerTransactionsListOldCustomerTransactions)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CustomerTransactions " + customerTransactionsListOldCustomerTransactions + " since its customerID field is not nullable.");
                }
            }
            for (Invoices invoicesListOldInvoices : invoicesListOld) {
                if (!invoicesListNew.contains(invoicesListOldInvoices)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Invoices " + invoicesListOldInvoices + " since its customerID field is not nullable.");
                }
            }
            for (Invoices invoicesList1OldInvoices : invoicesList1Old) {
                if (!invoicesList1New.contains(invoicesList1OldInvoices)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Invoices " + invoicesList1OldInvoices + " since its billToCustomerID field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (buyingGroupIDNew != null) {
                buyingGroupIDNew = em.getReference(buyingGroupIDNew.getClass(), buyingGroupIDNew.getBuyingGroupID());
                customers.setBuyingGroupID(buyingGroupIDNew);
            }
            if (customerCategoryIDNew != null) {
                customerCategoryIDNew = em.getReference(customerCategoryIDNew.getClass(), customerCategoryIDNew.getCustomerCategoryID());
                customers.setCustomerCategoryID(customerCategoryIDNew);
            }
            if (billToCustomerIDNew != null) {
                billToCustomerIDNew = em.getReference(billToCustomerIDNew.getClass(), billToCustomerIDNew.getCustomerID());
                customers.setBillToCustomerID(billToCustomerIDNew);
            }
            List<Orders> attachedOrdersListNew = new ArrayList<Orders>();
            for (Orders ordersListNewOrdersToAttach : ordersListNew) {
                ordersListNewOrdersToAttach = em.getReference(ordersListNewOrdersToAttach.getClass(), ordersListNewOrdersToAttach.getOrderID());
                attachedOrdersListNew.add(ordersListNewOrdersToAttach);
            }
            ordersListNew = attachedOrdersListNew;
            customers.setOrdersList(ordersListNew);
            List<Customers> attachedCustomersListNew = new ArrayList<Customers>();
            for (Customers customersListNewCustomersToAttach : customersListNew) {
                customersListNewCustomersToAttach = em.getReference(customersListNewCustomersToAttach.getClass(), customersListNewCustomersToAttach.getCustomerID());
                attachedCustomersListNew.add(customersListNewCustomersToAttach);
            }
            customersListNew = attachedCustomersListNew;
            customers.setCustomersList(customersListNew);
            List<SpecialDeals> attachedSpecialDealsListNew = new ArrayList<SpecialDeals>();
            for (SpecialDeals specialDealsListNewSpecialDealsToAttach : specialDealsListNew) {
                specialDealsListNewSpecialDealsToAttach = em.getReference(specialDealsListNewSpecialDealsToAttach.getClass(), specialDealsListNewSpecialDealsToAttach.getSpecialDealID());
                attachedSpecialDealsListNew.add(specialDealsListNewSpecialDealsToAttach);
            }
            specialDealsListNew = attachedSpecialDealsListNew;
            customers.setSpecialDealsList(specialDealsListNew);
            List<CustomerTransactions> attachedCustomerTransactionsListNew = new ArrayList<CustomerTransactions>();
            for (CustomerTransactions customerTransactionsListNewCustomerTransactionsToAttach : customerTransactionsListNew) {
                customerTransactionsListNewCustomerTransactionsToAttach = em.getReference(customerTransactionsListNewCustomerTransactionsToAttach.getClass(), customerTransactionsListNewCustomerTransactionsToAttach.getCustomerTransactionID());
                attachedCustomerTransactionsListNew.add(customerTransactionsListNewCustomerTransactionsToAttach);
            }
            customerTransactionsListNew = attachedCustomerTransactionsListNew;
            customers.setCustomerTransactionsList(customerTransactionsListNew);
            List<Invoices> attachedInvoicesListNew = new ArrayList<Invoices>();
            for (Invoices invoicesListNewInvoicesToAttach : invoicesListNew) {
                invoicesListNewInvoicesToAttach = em.getReference(invoicesListNewInvoicesToAttach.getClass(), invoicesListNewInvoicesToAttach.getInvoiceID());
                attachedInvoicesListNew.add(invoicesListNewInvoicesToAttach);
            }
            invoicesListNew = attachedInvoicesListNew;
            customers.setInvoicesList(invoicesListNew);
            List<Invoices> attachedInvoicesList1New = new ArrayList<Invoices>();
            for (Invoices invoicesList1NewInvoicesToAttach : invoicesList1New) {
                invoicesList1NewInvoicesToAttach = em.getReference(invoicesList1NewInvoicesToAttach.getClass(), invoicesList1NewInvoicesToAttach.getInvoiceID());
                attachedInvoicesList1New.add(invoicesList1NewInvoicesToAttach);
            }
            invoicesList1New = attachedInvoicesList1New;
            customers.setInvoicesList1(invoicesList1New);
            customers = em.merge(customers);
            if (buyingGroupIDOld != null && !buyingGroupIDOld.equals(buyingGroupIDNew)) {
                buyingGroupIDOld.getCustomersList().remove(customers);
                buyingGroupIDOld = em.merge(buyingGroupIDOld);
            }
            if (buyingGroupIDNew != null && !buyingGroupIDNew.equals(buyingGroupIDOld)) {
                buyingGroupIDNew.getCustomersList().add(customers);
                buyingGroupIDNew = em.merge(buyingGroupIDNew);
            }
            if (customerCategoryIDOld != null && !customerCategoryIDOld.equals(customerCategoryIDNew)) {
                customerCategoryIDOld.getCustomersList().remove(customers);
                customerCategoryIDOld = em.merge(customerCategoryIDOld);
            }
            if (customerCategoryIDNew != null && !customerCategoryIDNew.equals(customerCategoryIDOld)) {
                customerCategoryIDNew.getCustomersList().add(customers);
                customerCategoryIDNew = em.merge(customerCategoryIDNew);
            }
            if (billToCustomerIDOld != null && !billToCustomerIDOld.equals(billToCustomerIDNew)) {
                billToCustomerIDOld.getCustomersList().remove(customers);
                billToCustomerIDOld = em.merge(billToCustomerIDOld);
            }
            if (billToCustomerIDNew != null && !billToCustomerIDNew.equals(billToCustomerIDOld)) {
                billToCustomerIDNew.getCustomersList().add(customers);
                billToCustomerIDNew = em.merge(billToCustomerIDNew);
            }
            for (Orders ordersListNewOrders : ordersListNew) {
                if (!ordersListOld.contains(ordersListNewOrders)) {
                    Customers oldCustomerIDOfOrdersListNewOrders = ordersListNewOrders.getCustomerID();
                    ordersListNewOrders.setCustomerID(customers);
                    ordersListNewOrders = em.merge(ordersListNewOrders);
                    if (oldCustomerIDOfOrdersListNewOrders != null && !oldCustomerIDOfOrdersListNewOrders.equals(customers)) {
                        oldCustomerIDOfOrdersListNewOrders.getOrdersList().remove(ordersListNewOrders);
                        oldCustomerIDOfOrdersListNewOrders = em.merge(oldCustomerIDOfOrdersListNewOrders);
                    }
                }
            }
            for (Customers customersListNewCustomers : customersListNew) {
                if (!customersListOld.contains(customersListNewCustomers)) {
                    Customers oldBillToCustomerIDOfCustomersListNewCustomers = customersListNewCustomers.getBillToCustomerID();
                    customersListNewCustomers.setBillToCustomerID(customers);
                    customersListNewCustomers = em.merge(customersListNewCustomers);
                    if (oldBillToCustomerIDOfCustomersListNewCustomers != null && !oldBillToCustomerIDOfCustomersListNewCustomers.equals(customers)) {
                        oldBillToCustomerIDOfCustomersListNewCustomers.getCustomersList().remove(customersListNewCustomers);
                        oldBillToCustomerIDOfCustomersListNewCustomers = em.merge(oldBillToCustomerIDOfCustomersListNewCustomers);
                    }
                }
            }
            for (SpecialDeals specialDealsListOldSpecialDeals : specialDealsListOld) {
                if (!specialDealsListNew.contains(specialDealsListOldSpecialDeals)) {
                    specialDealsListOldSpecialDeals.setCustomerID(null);
                    specialDealsListOldSpecialDeals = em.merge(specialDealsListOldSpecialDeals);
                }
            }
            for (SpecialDeals specialDealsListNewSpecialDeals : specialDealsListNew) {
                if (!specialDealsListOld.contains(specialDealsListNewSpecialDeals)) {
                    Customers oldCustomerIDOfSpecialDealsListNewSpecialDeals = specialDealsListNewSpecialDeals.getCustomerID();
                    specialDealsListNewSpecialDeals.setCustomerID(customers);
                    specialDealsListNewSpecialDeals = em.merge(specialDealsListNewSpecialDeals);
                    if (oldCustomerIDOfSpecialDealsListNewSpecialDeals != null && !oldCustomerIDOfSpecialDealsListNewSpecialDeals.equals(customers)) {
                        oldCustomerIDOfSpecialDealsListNewSpecialDeals.getSpecialDealsList().remove(specialDealsListNewSpecialDeals);
                        oldCustomerIDOfSpecialDealsListNewSpecialDeals = em.merge(oldCustomerIDOfSpecialDealsListNewSpecialDeals);
                    }
                }
            }
            for (CustomerTransactions customerTransactionsListNewCustomerTransactions : customerTransactionsListNew) {
                if (!customerTransactionsListOld.contains(customerTransactionsListNewCustomerTransactions)) {
                    Customers oldCustomerIDOfCustomerTransactionsListNewCustomerTransactions = customerTransactionsListNewCustomerTransactions.getCustomerID();
                    customerTransactionsListNewCustomerTransactions.setCustomerID(customers);
                    customerTransactionsListNewCustomerTransactions = em.merge(customerTransactionsListNewCustomerTransactions);
                    if (oldCustomerIDOfCustomerTransactionsListNewCustomerTransactions != null && !oldCustomerIDOfCustomerTransactionsListNewCustomerTransactions.equals(customers)) {
                        oldCustomerIDOfCustomerTransactionsListNewCustomerTransactions.getCustomerTransactionsList().remove(customerTransactionsListNewCustomerTransactions);
                        oldCustomerIDOfCustomerTransactionsListNewCustomerTransactions = em.merge(oldCustomerIDOfCustomerTransactionsListNewCustomerTransactions);
                    }
                }
            }
            for (Invoices invoicesListNewInvoices : invoicesListNew) {
                if (!invoicesListOld.contains(invoicesListNewInvoices)) {
                    Customers oldCustomerIDOfInvoicesListNewInvoices = invoicesListNewInvoices.getCustomerID();
                    invoicesListNewInvoices.setCustomerID(customers);
                    invoicesListNewInvoices = em.merge(invoicesListNewInvoices);
                    if (oldCustomerIDOfInvoicesListNewInvoices != null && !oldCustomerIDOfInvoicesListNewInvoices.equals(customers)) {
                        oldCustomerIDOfInvoicesListNewInvoices.getInvoicesList().remove(invoicesListNewInvoices);
                        oldCustomerIDOfInvoicesListNewInvoices = em.merge(oldCustomerIDOfInvoicesListNewInvoices);
                    }
                }
            }
            for (Invoices invoicesList1NewInvoices : invoicesList1New) {
                if (!invoicesList1Old.contains(invoicesList1NewInvoices)) {
                    Customers oldBillToCustomerIDOfInvoicesList1NewInvoices = invoicesList1NewInvoices.getBillToCustomerID();
                    invoicesList1NewInvoices.setBillToCustomerID(customers);
                    invoicesList1NewInvoices = em.merge(invoicesList1NewInvoices);
                    if (oldBillToCustomerIDOfInvoicesList1NewInvoices != null && !oldBillToCustomerIDOfInvoicesList1NewInvoices.equals(customers)) {
                        oldBillToCustomerIDOfInvoicesList1NewInvoices.getInvoicesList1().remove(invoicesList1NewInvoices);
                        oldBillToCustomerIDOfInvoicesList1NewInvoices = em.merge(oldBillToCustomerIDOfInvoicesList1NewInvoices);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = customers.getCustomerID();
                if (findCustomers(id) == null) {
                    throw new NonexistentEntityException("The customers with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customers customers;
            try {
                customers = em.getReference(Customers.class, id);
                customers.getCustomerID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The customers with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Orders> ordersListOrphanCheck = customers.getOrdersList();
            for (Orders ordersListOrphanCheckOrders : ordersListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customers (" + customers + ") cannot be destroyed since the Orders " + ordersListOrphanCheckOrders + " in its ordersList field has a non-nullable customerID field.");
            }
            List<Customers> customersListOrphanCheck = customers.getCustomersList();
            for (Customers customersListOrphanCheckCustomers : customersListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customers (" + customers + ") cannot be destroyed since the Customers " + customersListOrphanCheckCustomers + " in its customersList field has a non-nullable billToCustomerID field.");
            }
            List<CustomerTransactions> customerTransactionsListOrphanCheck = customers.getCustomerTransactionsList();
            for (CustomerTransactions customerTransactionsListOrphanCheckCustomerTransactions : customerTransactionsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customers (" + customers + ") cannot be destroyed since the CustomerTransactions " + customerTransactionsListOrphanCheckCustomerTransactions + " in its customerTransactionsList field has a non-nullable customerID field.");
            }
            List<Invoices> invoicesListOrphanCheck = customers.getInvoicesList();
            for (Invoices invoicesListOrphanCheckInvoices : invoicesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customers (" + customers + ") cannot be destroyed since the Invoices " + invoicesListOrphanCheckInvoices + " in its invoicesList field has a non-nullable customerID field.");
            }
            List<Invoices> invoicesList1OrphanCheck = customers.getInvoicesList1();
            for (Invoices invoicesList1OrphanCheckInvoices : invoicesList1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customers (" + customers + ") cannot be destroyed since the Invoices " + invoicesList1OrphanCheckInvoices + " in its invoicesList1 field has a non-nullable billToCustomerID field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            BuyingGroups buyingGroupID = customers.getBuyingGroupID();
            if (buyingGroupID != null) {
                buyingGroupID.getCustomersList().remove(customers);
                buyingGroupID = em.merge(buyingGroupID);
            }
            CustomerCategories customerCategoryID = customers.getCustomerCategoryID();
            if (customerCategoryID != null) {
                customerCategoryID.getCustomersList().remove(customers);
                customerCategoryID = em.merge(customerCategoryID);
            }
            Customers billToCustomerID = customers.getBillToCustomerID();
            if (billToCustomerID != null) {
                billToCustomerID.getCustomersList().remove(customers);
                billToCustomerID = em.merge(billToCustomerID);
            }
            List<SpecialDeals> specialDealsList = customers.getSpecialDealsList();
            for (SpecialDeals specialDealsListSpecialDeals : specialDealsList) {
                specialDealsListSpecialDeals.setCustomerID(null);
                specialDealsListSpecialDeals = em.merge(specialDealsListSpecialDeals);
            }
            em.remove(customers);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Customers> findCustomersEntities() {
        return findCustomersEntities(true, -1, -1);
    }

    public List<Customers> findCustomersEntities(int maxResults, int firstResult) {
        return findCustomersEntities(false, maxResults, firstResult);
    }

    private List<Customers> findCustomersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Customers.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Customers findCustomers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Customers.class, id);
        } finally {
            em.close();
        }
    }

    public int getCustomersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Customers> rt = cq.from(Customers.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
