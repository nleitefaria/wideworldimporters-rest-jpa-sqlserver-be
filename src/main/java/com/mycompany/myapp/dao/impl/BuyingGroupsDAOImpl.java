/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.BuyingGroupsDAO;
import com.mycompany.myapp.entity.BuyingGroups;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.SpecialDeals;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class BuyingGroupsDAOImpl implements Serializable, BuyingGroupsDAO 
{
	private static final long serialVersionUID = 1L;	
	private EntityManagerFactory emf;
	
    public BuyingGroupsDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(BuyingGroups buyingGroups) throws PreexistingEntityException, Exception {
        if (buyingGroups.getCustomersList() == null) {
            buyingGroups.setCustomersList(new ArrayList<Customers>());
        }
        if (buyingGroups.getSpecialDealsList() == null) {
            buyingGroups.setSpecialDealsList(new ArrayList<SpecialDeals>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Customers> attachedCustomersList = new ArrayList<Customers>();
            for (Customers customersListCustomersToAttach : buyingGroups.getCustomersList()) {
                customersListCustomersToAttach = em.getReference(customersListCustomersToAttach.getClass(), customersListCustomersToAttach.getCustomerID());
                attachedCustomersList.add(customersListCustomersToAttach);
            }
            buyingGroups.setCustomersList(attachedCustomersList);
            List<SpecialDeals> attachedSpecialDealsList = new ArrayList<SpecialDeals>();
            for (SpecialDeals specialDealsListSpecialDealsToAttach : buyingGroups.getSpecialDealsList()) {
                specialDealsListSpecialDealsToAttach = em.getReference(specialDealsListSpecialDealsToAttach.getClass(), specialDealsListSpecialDealsToAttach.getSpecialDealID());
                attachedSpecialDealsList.add(specialDealsListSpecialDealsToAttach);
            }
            buyingGroups.setSpecialDealsList(attachedSpecialDealsList);
            em.persist(buyingGroups);
            for (Customers customersListCustomers : buyingGroups.getCustomersList()) {
                BuyingGroups oldBuyingGroupIDOfCustomersListCustomers = customersListCustomers.getBuyingGroupID();
                customersListCustomers.setBuyingGroupID(buyingGroups);
                customersListCustomers = em.merge(customersListCustomers);
                if (oldBuyingGroupIDOfCustomersListCustomers != null) {
                    oldBuyingGroupIDOfCustomersListCustomers.getCustomersList().remove(customersListCustomers);
                    oldBuyingGroupIDOfCustomersListCustomers = em.merge(oldBuyingGroupIDOfCustomersListCustomers);
                }
            }
            for (SpecialDeals specialDealsListSpecialDeals : buyingGroups.getSpecialDealsList()) {
                BuyingGroups oldBuyingGroupIDOfSpecialDealsListSpecialDeals = specialDealsListSpecialDeals.getBuyingGroupID();
                specialDealsListSpecialDeals.setBuyingGroupID(buyingGroups);
                specialDealsListSpecialDeals = em.merge(specialDealsListSpecialDeals);
                if (oldBuyingGroupIDOfSpecialDealsListSpecialDeals != null) {
                    oldBuyingGroupIDOfSpecialDealsListSpecialDeals.getSpecialDealsList().remove(specialDealsListSpecialDeals);
                    oldBuyingGroupIDOfSpecialDealsListSpecialDeals = em.merge(oldBuyingGroupIDOfSpecialDealsListSpecialDeals);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBuyingGroups(buyingGroups.getBuyingGroupID()) != null) {
                throw new PreexistingEntityException("BuyingGroups " + buyingGroups + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(BuyingGroups buyingGroups) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BuyingGroups persistentBuyingGroups = em.find(BuyingGroups.class, buyingGroups.getBuyingGroupID());
            List<Customers> customersListOld = persistentBuyingGroups.getCustomersList();
            List<Customers> customersListNew = buyingGroups.getCustomersList();
            List<SpecialDeals> specialDealsListOld = persistentBuyingGroups.getSpecialDealsList();
            List<SpecialDeals> specialDealsListNew = buyingGroups.getSpecialDealsList();
            List<Customers> attachedCustomersListNew = new ArrayList<Customers>();
            for (Customers customersListNewCustomersToAttach : customersListNew) {
                customersListNewCustomersToAttach = em.getReference(customersListNewCustomersToAttach.getClass(), customersListNewCustomersToAttach.getCustomerID());
                attachedCustomersListNew.add(customersListNewCustomersToAttach);
            }
            customersListNew = attachedCustomersListNew;
            buyingGroups.setCustomersList(customersListNew);
            List<SpecialDeals> attachedSpecialDealsListNew = new ArrayList<SpecialDeals>();
            for (SpecialDeals specialDealsListNewSpecialDealsToAttach : specialDealsListNew) {
                specialDealsListNewSpecialDealsToAttach = em.getReference(specialDealsListNewSpecialDealsToAttach.getClass(), specialDealsListNewSpecialDealsToAttach.getSpecialDealID());
                attachedSpecialDealsListNew.add(specialDealsListNewSpecialDealsToAttach);
            }
            specialDealsListNew = attachedSpecialDealsListNew;
            buyingGroups.setSpecialDealsList(specialDealsListNew);
            buyingGroups = em.merge(buyingGroups);
            for (Customers customersListOldCustomers : customersListOld) {
                if (!customersListNew.contains(customersListOldCustomers)) {
                    customersListOldCustomers.setBuyingGroupID(null);
                    customersListOldCustomers = em.merge(customersListOldCustomers);
                }
            }
            for (Customers customersListNewCustomers : customersListNew) {
                if (!customersListOld.contains(customersListNewCustomers)) {
                    BuyingGroups oldBuyingGroupIDOfCustomersListNewCustomers = customersListNewCustomers.getBuyingGroupID();
                    customersListNewCustomers.setBuyingGroupID(buyingGroups);
                    customersListNewCustomers = em.merge(customersListNewCustomers);
                    if (oldBuyingGroupIDOfCustomersListNewCustomers != null && !oldBuyingGroupIDOfCustomersListNewCustomers.equals(buyingGroups)) {
                        oldBuyingGroupIDOfCustomersListNewCustomers.getCustomersList().remove(customersListNewCustomers);
                        oldBuyingGroupIDOfCustomersListNewCustomers = em.merge(oldBuyingGroupIDOfCustomersListNewCustomers);
                    }
                }
            }
            for (SpecialDeals specialDealsListOldSpecialDeals : specialDealsListOld) {
                if (!specialDealsListNew.contains(specialDealsListOldSpecialDeals)) {
                    specialDealsListOldSpecialDeals.setBuyingGroupID(null);
                    specialDealsListOldSpecialDeals = em.merge(specialDealsListOldSpecialDeals);
                }
            }
            for (SpecialDeals specialDealsListNewSpecialDeals : specialDealsListNew) {
                if (!specialDealsListOld.contains(specialDealsListNewSpecialDeals)) {
                    BuyingGroups oldBuyingGroupIDOfSpecialDealsListNewSpecialDeals = specialDealsListNewSpecialDeals.getBuyingGroupID();
                    specialDealsListNewSpecialDeals.setBuyingGroupID(buyingGroups);
                    specialDealsListNewSpecialDeals = em.merge(specialDealsListNewSpecialDeals);
                    if (oldBuyingGroupIDOfSpecialDealsListNewSpecialDeals != null && !oldBuyingGroupIDOfSpecialDealsListNewSpecialDeals.equals(buyingGroups)) {
                        oldBuyingGroupIDOfSpecialDealsListNewSpecialDeals.getSpecialDealsList().remove(specialDealsListNewSpecialDeals);
                        oldBuyingGroupIDOfSpecialDealsListNewSpecialDeals = em.merge(oldBuyingGroupIDOfSpecialDealsListNewSpecialDeals);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = buyingGroups.getBuyingGroupID();
                if (findBuyingGroups(id) == null) {
                    throw new NonexistentEntityException("The buyingGroups with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BuyingGroups buyingGroups;
            try {
                buyingGroups = em.getReference(BuyingGroups.class, id);
                buyingGroups.getBuyingGroupID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The buyingGroups with id " + id + " no longer exists.", enfe);
            }
            List<Customers> customersList = buyingGroups.getCustomersList();
            for (Customers customersListCustomers : customersList) {
                customersListCustomers.setBuyingGroupID(null);
                customersListCustomers = em.merge(customersListCustomers);
            }
            List<SpecialDeals> specialDealsList = buyingGroups.getSpecialDealsList();
            for (SpecialDeals specialDealsListSpecialDeals : specialDealsList) {
                specialDealsListSpecialDeals.setBuyingGroupID(null);
                specialDealsListSpecialDeals = em.merge(specialDealsListSpecialDeals);
            }
            em.remove(buyingGroups);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<BuyingGroups> findBuyingGroupsEntities() {
        return findBuyingGroupsEntities(true, -1, -1);
    }

    public List<BuyingGroups> findBuyingGroupsEntities(int maxResults, int firstResult) {
        return findBuyingGroupsEntities(false, maxResults, firstResult);
    }
    
    public List<BuyingGroups> findSpecialDealsEntitiesWithPaging(int pageNo)
    { 	
    	return findBuyingGroupsEntities(false, 10, pageNo * 10);    
    }

    private List<BuyingGroups> findBuyingGroupsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(BuyingGroups.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public BuyingGroups findBuyingGroups(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(BuyingGroups.class, id);
        } finally {
            em.close();
        }
    }

    public int getBuyingGroupsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<BuyingGroups> rt = cq.from(BuyingGroups.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
