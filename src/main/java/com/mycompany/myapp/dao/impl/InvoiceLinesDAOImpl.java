/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.InvoiceLinesDAO;
import com.mycompany.myapp.entity.InvoiceLines;
import com.mycompany.myapp.entity.Invoices;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

/**
 *
 * @author nleit_000
 */
public class InvoiceLinesDAOImpl implements InvoiceLinesDAO, Serializable {

    public InvoiceLinesDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InvoiceLines invoiceLines) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Invoices invoiceID = invoiceLines.getInvoiceID();
            if (invoiceID != null) {
                invoiceID = em.getReference(invoiceID.getClass(), invoiceID.getInvoiceID());
                invoiceLines.setInvoiceID(invoiceID);
            }
            em.persist(invoiceLines);
            if (invoiceID != null) {
                invoiceID.getInvoiceLinesList().add(invoiceLines);
                invoiceID = em.merge(invoiceID);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInvoiceLines(invoiceLines.getInvoiceLineID()) != null) {
                throw new PreexistingEntityException("InvoiceLines " + invoiceLines + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InvoiceLines invoiceLines) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InvoiceLines persistentInvoiceLines = em.find(InvoiceLines.class, invoiceLines.getInvoiceLineID());
            Invoices invoiceIDOld = persistentInvoiceLines.getInvoiceID();
            Invoices invoiceIDNew = invoiceLines.getInvoiceID();
            if (invoiceIDNew != null) {
                invoiceIDNew = em.getReference(invoiceIDNew.getClass(), invoiceIDNew.getInvoiceID());
                invoiceLines.setInvoiceID(invoiceIDNew);
            }
            invoiceLines = em.merge(invoiceLines);
            if (invoiceIDOld != null && !invoiceIDOld.equals(invoiceIDNew)) {
                invoiceIDOld.getInvoiceLinesList().remove(invoiceLines);
                invoiceIDOld = em.merge(invoiceIDOld);
            }
            if (invoiceIDNew != null && !invoiceIDNew.equals(invoiceIDOld)) {
                invoiceIDNew.getInvoiceLinesList().add(invoiceLines);
                invoiceIDNew = em.merge(invoiceIDNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = invoiceLines.getInvoiceLineID();
                if (findInvoiceLines(id) == null) {
                    throw new NonexistentEntityException("The invoiceLines with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InvoiceLines invoiceLines;
            try {
                invoiceLines = em.getReference(InvoiceLines.class, id);
                invoiceLines.getInvoiceLineID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The invoiceLines with id " + id + " no longer exists.", enfe);
            }
            Invoices invoiceID = invoiceLines.getInvoiceID();
            if (invoiceID != null) {
                invoiceID.getInvoiceLinesList().remove(invoiceLines);
                invoiceID = em.merge(invoiceID);
            }
            em.remove(invoiceLines);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InvoiceLines> findInvoiceLinesEntities() {
        return findInvoiceLinesEntities(true, -1, -1);
    }

    public List<InvoiceLines> findInvoiceLinesEntities(int maxResults, int firstResult) {
        return findInvoiceLinesEntities(false, maxResults, firstResult);
    }

    private List<InvoiceLines> findInvoiceLinesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InvoiceLines.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }                  
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public InvoiceLines findInvoiceLines(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InvoiceLines.class, id);
        } finally {
            em.close();
        }
    }

    public int getInvoiceLinesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InvoiceLines> rt = cq.from(InvoiceLines.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
