/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.OrderLinesDAO;
import com.mycompany.myapp.entity.OrderLines;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;


/**
 *
 * @author nleit_000
 */
public class OrderLinesDAOImpl implements OrderLinesDAO, Serializable {

    public OrderLinesDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OrderLines orderLines) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Orders orderID = orderLines.getOrderID();
            if (orderID != null) {
                orderID = em.getReference(orderID.getClass(), orderID.getOrderID());
                orderLines.setOrderID(orderID);
            }
            em.persist(orderLines);
            if (orderID != null) {
                orderID.getOrderLinesList().add(orderLines);
                orderID = em.merge(orderID);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOrderLines(orderLines.getOrderLineID()) != null) {
                throw new PreexistingEntityException("OrderLines " + orderLines + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OrderLines orderLines) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OrderLines persistentOrderLines = em.find(OrderLines.class, orderLines.getOrderLineID());
            Orders orderIDOld = persistentOrderLines.getOrderID();
            Orders orderIDNew = orderLines.getOrderID();
            if (orderIDNew != null) {
                orderIDNew = em.getReference(orderIDNew.getClass(), orderIDNew.getOrderID());
                orderLines.setOrderID(orderIDNew);
            }
            orderLines = em.merge(orderLines);
            if (orderIDOld != null && !orderIDOld.equals(orderIDNew)) {
                orderIDOld.getOrderLinesList().remove(orderLines);
                orderIDOld = em.merge(orderIDOld);
            }
            if (orderIDNew != null && !orderIDNew.equals(orderIDOld)) {
                orderIDNew.getOrderLinesList().add(orderLines);
                orderIDNew = em.merge(orderIDNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = orderLines.getOrderLineID();
                if (findOrderLines(id) == null) {
                    throw new NonexistentEntityException("The orderLines with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OrderLines orderLines;
            try {
                orderLines = em.getReference(OrderLines.class, id);
                orderLines.getOrderLineID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orderLines with id " + id + " no longer exists.", enfe);
            }
            Orders orderID = orderLines.getOrderID();
            if (orderID != null) {
                orderID.getOrderLinesList().remove(orderLines);
                orderID = em.merge(orderID);
            }
            em.remove(orderLines);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OrderLines> findOrderLinesEntities() {
        return findOrderLinesEntities(true, -1, -1);
    }

    public List<OrderLines> findOrderLinesEntities(int maxResults, int firstResult) {
        return findOrderLinesEntities(false, maxResults, firstResult);
    }
    
    public List<OrderLines> findOrderLinesEntitiesWithPaging(int pageNo)
    { 	
    	return findOrderLinesEntities(false, 10, pageNo * 10);    
    }


    private List<OrderLines> findOrderLinesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OrderLines.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OrderLines findOrderLines(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OrderLines.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrderLinesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OrderLines> rt = cq.from(OrderLines.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
