package com.mycompany.myapp.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.CustomerCategoriesDAO;
import com.mycompany.myapp.entity.CustomerCategories;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.SpecialDeals;
import com.mycompany.myapp.exceptions.IllegalOrphanException;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

import java.util.ArrayList;
import java.util.List;

public class CustomerCategoriesDAOImpl implements CustomerCategoriesDAO 
{
	private static final long serialVersionUID = 1L;	
	private EntityManagerFactory emf;
	
    public CustomerCategoriesDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void create(CustomerCategories customerCategories) throws PreexistingEntityException, Exception {
        if (customerCategories.getCustomersList() == null) {
            customerCategories.setCustomersList(new ArrayList<Customers>());
        }
        if (customerCategories.getSpecialDealsList() == null) {
            customerCategories.setSpecialDealsList(new ArrayList<SpecialDeals>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Customers> attachedCustomersList = new ArrayList<Customers>();
            for (Customers customersListCustomersToAttach : customerCategories.getCustomersList()) {
                customersListCustomersToAttach = em.getReference(customersListCustomersToAttach.getClass(), customersListCustomersToAttach.getCustomerID());
                attachedCustomersList.add(customersListCustomersToAttach);
            }
            customerCategories.setCustomersList(attachedCustomersList);
            List<SpecialDeals> attachedSpecialDealsList = new ArrayList<SpecialDeals>();
            for (SpecialDeals specialDealsListSpecialDealsToAttach : customerCategories.getSpecialDealsList()) {
                specialDealsListSpecialDealsToAttach = em.getReference(specialDealsListSpecialDealsToAttach.getClass(), specialDealsListSpecialDealsToAttach.getSpecialDealID());
                attachedSpecialDealsList.add(specialDealsListSpecialDealsToAttach);
            }
            customerCategories.setSpecialDealsList(attachedSpecialDealsList);
            em.persist(customerCategories);
            for (Customers customersListCustomers : customerCategories.getCustomersList()) {
                CustomerCategories oldCustomerCategoryIDOfCustomersListCustomers = customersListCustomers.getCustomerCategoryID();
                customersListCustomers.setCustomerCategoryID(customerCategories);
                customersListCustomers = em.merge(customersListCustomers);
                if (oldCustomerCategoryIDOfCustomersListCustomers != null) {
                    oldCustomerCategoryIDOfCustomersListCustomers.getCustomersList().remove(customersListCustomers);
                    oldCustomerCategoryIDOfCustomersListCustomers = em.merge(oldCustomerCategoryIDOfCustomersListCustomers);
                }
            }
            for (SpecialDeals specialDealsListSpecialDeals : customerCategories.getSpecialDealsList()) {
                CustomerCategories oldCustomerCategoryIDOfSpecialDealsListSpecialDeals = specialDealsListSpecialDeals.getCustomerCategoryID();
                specialDealsListSpecialDeals.setCustomerCategoryID(customerCategories);
                specialDealsListSpecialDeals = em.merge(specialDealsListSpecialDeals);
                if (oldCustomerCategoryIDOfSpecialDealsListSpecialDeals != null) {
                    oldCustomerCategoryIDOfSpecialDealsListSpecialDeals.getSpecialDealsList().remove(specialDealsListSpecialDeals);
                    oldCustomerCategoryIDOfSpecialDealsListSpecialDeals = em.merge(oldCustomerCategoryIDOfSpecialDealsListSpecialDeals);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCustomerCategories(customerCategories.getCustomerCategoryID()) != null) {
                throw new PreexistingEntityException("CustomerCategories " + customerCategories + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CustomerCategories customerCategories) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerCategories persistentCustomerCategories = em.find(CustomerCategories.class, customerCategories.getCustomerCategoryID());
            List<Customers> customersListOld = persistentCustomerCategories.getCustomersList();
            List<Customers> customersListNew = customerCategories.getCustomersList();
            List<SpecialDeals> specialDealsListOld = persistentCustomerCategories.getSpecialDealsList();
            List<SpecialDeals> specialDealsListNew = customerCategories.getSpecialDealsList();
            List<String> illegalOrphanMessages = null;
            for (Customers customersListOldCustomers : customersListOld) {
                if (!customersListNew.contains(customersListOldCustomers)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Customers " + customersListOldCustomers + " since its customerCategoryID field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Customers> attachedCustomersListNew = new ArrayList<Customers>();
            for (Customers customersListNewCustomersToAttach : customersListNew) {
                customersListNewCustomersToAttach = em.getReference(customersListNewCustomersToAttach.getClass(), customersListNewCustomersToAttach.getCustomerID());
                attachedCustomersListNew.add(customersListNewCustomersToAttach);
            }
            customersListNew = attachedCustomersListNew;
            customerCategories.setCustomersList(customersListNew);
            List<SpecialDeals> attachedSpecialDealsListNew = new ArrayList<SpecialDeals>();
            for (SpecialDeals specialDealsListNewSpecialDealsToAttach : specialDealsListNew) {
                specialDealsListNewSpecialDealsToAttach = em.getReference(specialDealsListNewSpecialDealsToAttach.getClass(), specialDealsListNewSpecialDealsToAttach.getSpecialDealID());
                attachedSpecialDealsListNew.add(specialDealsListNewSpecialDealsToAttach);
            }
            specialDealsListNew = attachedSpecialDealsListNew;
            customerCategories.setSpecialDealsList(specialDealsListNew);
            customerCategories = em.merge(customerCategories);
            for (Customers customersListNewCustomers : customersListNew) {
                if (!customersListOld.contains(customersListNewCustomers)) {
                    CustomerCategories oldCustomerCategoryIDOfCustomersListNewCustomers = customersListNewCustomers.getCustomerCategoryID();
                    customersListNewCustomers.setCustomerCategoryID(customerCategories);
                    customersListNewCustomers = em.merge(customersListNewCustomers);
                    if (oldCustomerCategoryIDOfCustomersListNewCustomers != null && !oldCustomerCategoryIDOfCustomersListNewCustomers.equals(customerCategories)) {
                        oldCustomerCategoryIDOfCustomersListNewCustomers.getCustomersList().remove(customersListNewCustomers);
                        oldCustomerCategoryIDOfCustomersListNewCustomers = em.merge(oldCustomerCategoryIDOfCustomersListNewCustomers);
                    }
                }
            }
            for (SpecialDeals specialDealsListOldSpecialDeals : specialDealsListOld) {
                if (!specialDealsListNew.contains(specialDealsListOldSpecialDeals)) {
                    specialDealsListOldSpecialDeals.setCustomerCategoryID(null);
                    specialDealsListOldSpecialDeals = em.merge(specialDealsListOldSpecialDeals);
                }
            }
            for (SpecialDeals specialDealsListNewSpecialDeals : specialDealsListNew) {
                if (!specialDealsListOld.contains(specialDealsListNewSpecialDeals)) {
                    CustomerCategories oldCustomerCategoryIDOfSpecialDealsListNewSpecialDeals = specialDealsListNewSpecialDeals.getCustomerCategoryID();
                    specialDealsListNewSpecialDeals.setCustomerCategoryID(customerCategories);
                    specialDealsListNewSpecialDeals = em.merge(specialDealsListNewSpecialDeals);
                    if (oldCustomerCategoryIDOfSpecialDealsListNewSpecialDeals != null && !oldCustomerCategoryIDOfSpecialDealsListNewSpecialDeals.equals(customerCategories)) {
                        oldCustomerCategoryIDOfSpecialDealsListNewSpecialDeals.getSpecialDealsList().remove(specialDealsListNewSpecialDeals);
                        oldCustomerCategoryIDOfSpecialDealsListNewSpecialDeals = em.merge(oldCustomerCategoryIDOfSpecialDealsListNewSpecialDeals);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = customerCategories.getCustomerCategoryID();
                if (findCustomerCategories(id) == null) {
                    throw new NonexistentEntityException("The customerCategories with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CustomerCategories customerCategories;
            try {
                customerCategories = em.getReference(CustomerCategories.class, id);
                customerCategories.getCustomerCategoryID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The customerCategories with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Customers> customersListOrphanCheck = customerCategories.getCustomersList();
            for (Customers customersListOrphanCheckCustomers : customersListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CustomerCategories (" + customerCategories + ") cannot be destroyed since the Customers " + customersListOrphanCheckCustomers + " in its customersList field has a non-nullable customerCategoryID field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SpecialDeals> specialDealsList = customerCategories.getSpecialDealsList();
            for (SpecialDeals specialDealsListSpecialDeals : specialDealsList) {
                specialDealsListSpecialDeals.setCustomerCategoryID(null);
                specialDealsListSpecialDeals = em.merge(specialDealsListSpecialDeals);
            }
            em.remove(customerCategories);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CustomerCategories> findCustomerCategoriesEntities() {
        return findCustomerCategoriesEntities(true, -1, -1);
    }

    public List<CustomerCategories> findCustomerCategoriesEntities(int maxResults, int firstResult) {
        return findCustomerCategoriesEntities(false, maxResults, firstResult);
    }

    private List<CustomerCategories> findCustomerCategoriesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CustomerCategories.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CustomerCategories findCustomerCategories(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CustomerCategories.class, id);
        } finally {
            em.close();
        }
    }

    public int getCustomerCategoriesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CustomerCategories> rt = cq.from(CustomerCategories.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
	
	

}
