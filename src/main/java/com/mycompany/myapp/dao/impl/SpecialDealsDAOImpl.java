/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.SpecialDealsDAO;
import com.mycompany.myapp.entity.BuyingGroups;
import com.mycompany.myapp.entity.CustomerCategories;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.SpecialDeals;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;


/**
 *
 * @author nleit_000
 */
public class SpecialDealsDAOImpl implements Serializable, SpecialDealsDAO {

    public SpecialDealsDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SpecialDeals specialDeals) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            BuyingGroups buyingGroupID = specialDeals.getBuyingGroupID();
            if (buyingGroupID != null) {
                buyingGroupID = em.getReference(buyingGroupID.getClass(), buyingGroupID.getBuyingGroupID());
                specialDeals.setBuyingGroupID(buyingGroupID);
            }
            CustomerCategories customerCategoryID = specialDeals.getCustomerCategoryID();
            if (customerCategoryID != null) {
                customerCategoryID = em.getReference(customerCategoryID.getClass(), customerCategoryID.getCustomerCategoryID());
                specialDeals.setCustomerCategoryID(customerCategoryID);
            }
            Customers customerID = specialDeals.getCustomerID();
            if (customerID != null) {
                customerID = em.getReference(customerID.getClass(), customerID.getCustomerID());
                specialDeals.setCustomerID(customerID);
            }
            em.persist(specialDeals);
            if (buyingGroupID != null) {
                buyingGroupID.getSpecialDealsList().add(specialDeals);
                buyingGroupID = em.merge(buyingGroupID);
            }
            if (customerCategoryID != null) {
                customerCategoryID.getSpecialDealsList().add(specialDeals);
                customerCategoryID = em.merge(customerCategoryID);
            }
            if (customerID != null) {
                customerID.getSpecialDealsList().add(specialDeals);
                customerID = em.merge(customerID);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSpecialDeals(specialDeals.getSpecialDealID()) != null) {
                throw new PreexistingEntityException("SpecialDeals " + specialDeals + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SpecialDeals specialDeals) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SpecialDeals persistentSpecialDeals = em.find(SpecialDeals.class, specialDeals.getSpecialDealID());
            BuyingGroups buyingGroupIDOld = persistentSpecialDeals.getBuyingGroupID();
            BuyingGroups buyingGroupIDNew = specialDeals.getBuyingGroupID();
            CustomerCategories customerCategoryIDOld = persistentSpecialDeals.getCustomerCategoryID();
            CustomerCategories customerCategoryIDNew = specialDeals.getCustomerCategoryID();
            Customers customerIDOld = persistentSpecialDeals.getCustomerID();
            Customers customerIDNew = specialDeals.getCustomerID();
            if (buyingGroupIDNew != null) {
                buyingGroupIDNew = em.getReference(buyingGroupIDNew.getClass(), buyingGroupIDNew.getBuyingGroupID());
                specialDeals.setBuyingGroupID(buyingGroupIDNew);
            }
            if (customerCategoryIDNew != null) {
                customerCategoryIDNew = em.getReference(customerCategoryIDNew.getClass(), customerCategoryIDNew.getCustomerCategoryID());
                specialDeals.setCustomerCategoryID(customerCategoryIDNew);
            }
            if (customerIDNew != null) {
                customerIDNew = em.getReference(customerIDNew.getClass(), customerIDNew.getCustomerID());
                specialDeals.setCustomerID(customerIDNew);
            }
            specialDeals = em.merge(specialDeals);
            if (buyingGroupIDOld != null && !buyingGroupIDOld.equals(buyingGroupIDNew)) {
                buyingGroupIDOld.getSpecialDealsList().remove(specialDeals);
                buyingGroupIDOld = em.merge(buyingGroupIDOld);
            }
            if (buyingGroupIDNew != null && !buyingGroupIDNew.equals(buyingGroupIDOld)) {
                buyingGroupIDNew.getSpecialDealsList().add(specialDeals);
                buyingGroupIDNew = em.merge(buyingGroupIDNew);
            }
            if (customerCategoryIDOld != null && !customerCategoryIDOld.equals(customerCategoryIDNew)) {
                customerCategoryIDOld.getSpecialDealsList().remove(specialDeals);
                customerCategoryIDOld = em.merge(customerCategoryIDOld);
            }
            if (customerCategoryIDNew != null && !customerCategoryIDNew.equals(customerCategoryIDOld)) {
                customerCategoryIDNew.getSpecialDealsList().add(specialDeals);
                customerCategoryIDNew = em.merge(customerCategoryIDNew);
            }
            if (customerIDOld != null && !customerIDOld.equals(customerIDNew)) {
                customerIDOld.getSpecialDealsList().remove(specialDeals);
                customerIDOld = em.merge(customerIDOld);
            }
            if (customerIDNew != null && !customerIDNew.equals(customerIDOld)) {
                customerIDNew.getSpecialDealsList().add(specialDeals);
                customerIDNew = em.merge(customerIDNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = specialDeals.getSpecialDealID();
                if (findSpecialDeals(id) == null) {
                    throw new NonexistentEntityException("The specialDeals with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SpecialDeals specialDeals;
            try {
                specialDeals = em.getReference(SpecialDeals.class, id);
                specialDeals.getSpecialDealID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The specialDeals with id " + id + " no longer exists.", enfe);
            }
            BuyingGroups buyingGroupID = specialDeals.getBuyingGroupID();
            if (buyingGroupID != null) {
                buyingGroupID.getSpecialDealsList().remove(specialDeals);
                buyingGroupID = em.merge(buyingGroupID);
            }
            CustomerCategories customerCategoryID = specialDeals.getCustomerCategoryID();
            if (customerCategoryID != null) {
                customerCategoryID.getSpecialDealsList().remove(specialDeals);
                customerCategoryID = em.merge(customerCategoryID);
            }
            Customers customerID = specialDeals.getCustomerID();
            if (customerID != null) {
                customerID.getSpecialDealsList().remove(specialDeals);
                customerID = em.merge(customerID);
            }
            em.remove(specialDeals);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SpecialDeals> findSpecialDealsEntities() {
        return findSpecialDealsEntities(true, -1, -1);
    }

    public List<SpecialDeals> findSpecialDealsEntities(int maxResults, int firstResult) 
    {	
    	return findSpecialDealsEntities(false, maxResults, firstResult);
    }
    
    public List<SpecialDeals> findSpecialDealsEntitiesWithPaging(int pageNo)
    { 	
    	return findSpecialDealsEntities(false, 10, pageNo * 10);    
    }

    private List<SpecialDeals> findSpecialDealsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SpecialDeals.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SpecialDeals findSpecialDeals(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SpecialDeals.class, id);
        } finally {
            em.close();
        }
    }

    public int getSpecialDealsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SpecialDeals> rt = cq.from(SpecialDeals.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
