/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dao.InvoicesDAO;
import com.mycompany.myapp.entity.CustomerTransactions;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.entity.InvoiceLines;
import com.mycompany.myapp.entity.Invoices;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.entity.SpecialDeals;
import com.mycompany.myapp.exceptions.IllegalOrphanException;
import com.mycompany.myapp.exceptions.NonexistentEntityException;
import com.mycompany.myapp.exceptions.PreexistingEntityException;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author nleit_000
 */
public class InvoicesDAOImpl implements InvoicesDAO, Serializable {

    public InvoicesDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Invoices invoices) throws PreexistingEntityException, Exception {
        if (invoices.getInvoiceLinesList() == null) {
            invoices.setInvoiceLinesList(new ArrayList<InvoiceLines>());
        }
        if (invoices.getCustomerTransactionsList() == null) {
            invoices.setCustomerTransactionsList(new ArrayList<CustomerTransactions>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customers customerID = invoices.getCustomerID();
            if (customerID != null) {
                customerID = em.getReference(customerID.getClass(), customerID.getCustomerID());
                invoices.setCustomerID(customerID);
            }
            Customers billToCustomerID = invoices.getBillToCustomerID();
            if (billToCustomerID != null) {
                billToCustomerID = em.getReference(billToCustomerID.getClass(), billToCustomerID.getCustomerID());
                invoices.setBillToCustomerID(billToCustomerID);
            }
            Orders orderID = invoices.getOrderID();
            if (orderID != null) {
                orderID = em.getReference(orderID.getClass(), orderID.getOrderID());
                invoices.setOrderID(orderID);
            }
            List<InvoiceLines> attachedInvoiceLinesList = new ArrayList<InvoiceLines>();
            for (InvoiceLines invoiceLinesListInvoiceLinesToAttach : invoices.getInvoiceLinesList()) {
                invoiceLinesListInvoiceLinesToAttach = em.getReference(invoiceLinesListInvoiceLinesToAttach.getClass(), invoiceLinesListInvoiceLinesToAttach.getInvoiceLineID());
                attachedInvoiceLinesList.add(invoiceLinesListInvoiceLinesToAttach);
            }
            invoices.setInvoiceLinesList(attachedInvoiceLinesList);
            List<CustomerTransactions> attachedCustomerTransactionsList = new ArrayList<CustomerTransactions>();
            for (CustomerTransactions customerTransactionsListCustomerTransactionsToAttach : invoices.getCustomerTransactionsList()) {
                customerTransactionsListCustomerTransactionsToAttach = em.getReference(customerTransactionsListCustomerTransactionsToAttach.getClass(), customerTransactionsListCustomerTransactionsToAttach.getCustomerTransactionID());
                attachedCustomerTransactionsList.add(customerTransactionsListCustomerTransactionsToAttach);
            }
            invoices.setCustomerTransactionsList(attachedCustomerTransactionsList);
            em.persist(invoices);
            if (customerID != null) {
                customerID.getInvoicesList().add(invoices);
                customerID = em.merge(customerID);
            }
            if (billToCustomerID != null) {
                billToCustomerID.getInvoicesList().add(invoices);
                billToCustomerID = em.merge(billToCustomerID);
            }
            if (orderID != null) {
                orderID.getInvoicesList().add(invoices);
                orderID = em.merge(orderID);
            }
            for (InvoiceLines invoiceLinesListInvoiceLines : invoices.getInvoiceLinesList()) {
                Invoices oldInvoiceIDOfInvoiceLinesListInvoiceLines = invoiceLinesListInvoiceLines.getInvoiceID();
                invoiceLinesListInvoiceLines.setInvoiceID(invoices);
                invoiceLinesListInvoiceLines = em.merge(invoiceLinesListInvoiceLines);
                if (oldInvoiceIDOfInvoiceLinesListInvoiceLines != null) {
                    oldInvoiceIDOfInvoiceLinesListInvoiceLines.getInvoiceLinesList().remove(invoiceLinesListInvoiceLines);
                    oldInvoiceIDOfInvoiceLinesListInvoiceLines = em.merge(oldInvoiceIDOfInvoiceLinesListInvoiceLines);
                }
            }
            for (CustomerTransactions customerTransactionsListCustomerTransactions : invoices.getCustomerTransactionsList()) {
                Invoices oldInvoiceIDOfCustomerTransactionsListCustomerTransactions = customerTransactionsListCustomerTransactions.getInvoiceID();
                customerTransactionsListCustomerTransactions.setInvoiceID(invoices);
                customerTransactionsListCustomerTransactions = em.merge(customerTransactionsListCustomerTransactions);
                if (oldInvoiceIDOfCustomerTransactionsListCustomerTransactions != null) {
                    oldInvoiceIDOfCustomerTransactionsListCustomerTransactions.getCustomerTransactionsList().remove(customerTransactionsListCustomerTransactions);
                    oldInvoiceIDOfCustomerTransactionsListCustomerTransactions = em.merge(oldInvoiceIDOfCustomerTransactionsListCustomerTransactions);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInvoices(invoices.getInvoiceID()) != null) {
                throw new PreexistingEntityException("Invoices " + invoices + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Invoices invoices) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Invoices persistentInvoices = em.find(Invoices.class, invoices.getInvoiceID());
            Customers customerIDOld = persistentInvoices.getCustomerID();
            Customers customerIDNew = invoices.getCustomerID();
            Customers billToCustomerIDOld = persistentInvoices.getBillToCustomerID();
            Customers billToCustomerIDNew = invoices.getBillToCustomerID();
            Orders orderIDOld = persistentInvoices.getOrderID();
            Orders orderIDNew = invoices.getOrderID();
            List<InvoiceLines> invoiceLinesListOld = persistentInvoices.getInvoiceLinesList();
            List<InvoiceLines> invoiceLinesListNew = invoices.getInvoiceLinesList();
            List<CustomerTransactions> customerTransactionsListOld = persistentInvoices.getCustomerTransactionsList();
            List<CustomerTransactions> customerTransactionsListNew = invoices.getCustomerTransactionsList();
            List<String> illegalOrphanMessages = null;
            for (InvoiceLines invoiceLinesListOldInvoiceLines : invoiceLinesListOld) {
                if (!invoiceLinesListNew.contains(invoiceLinesListOldInvoiceLines)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain InvoiceLines " + invoiceLinesListOldInvoiceLines + " since its invoiceID field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (customerIDNew != null) {
                customerIDNew = em.getReference(customerIDNew.getClass(), customerIDNew.getCustomerID());
                invoices.setCustomerID(customerIDNew);
            }
            if (billToCustomerIDNew != null) {
                billToCustomerIDNew = em.getReference(billToCustomerIDNew.getClass(), billToCustomerIDNew.getCustomerID());
                invoices.setBillToCustomerID(billToCustomerIDNew);
            }
            if (orderIDNew != null) {
                orderIDNew = em.getReference(orderIDNew.getClass(), orderIDNew.getOrderID());
                invoices.setOrderID(orderIDNew);
            }
            List<InvoiceLines> attachedInvoiceLinesListNew = new ArrayList<InvoiceLines>();
            for (InvoiceLines invoiceLinesListNewInvoiceLinesToAttach : invoiceLinesListNew) {
                invoiceLinesListNewInvoiceLinesToAttach = em.getReference(invoiceLinesListNewInvoiceLinesToAttach.getClass(), invoiceLinesListNewInvoiceLinesToAttach.getInvoiceLineID());
                attachedInvoiceLinesListNew.add(invoiceLinesListNewInvoiceLinesToAttach);
            }
            invoiceLinesListNew = attachedInvoiceLinesListNew;
            invoices.setInvoiceLinesList(invoiceLinesListNew);
            List<CustomerTransactions> attachedCustomerTransactionsListNew = new ArrayList<CustomerTransactions>();
            for (CustomerTransactions customerTransactionsListNewCustomerTransactionsToAttach : customerTransactionsListNew) {
                customerTransactionsListNewCustomerTransactionsToAttach = em.getReference(customerTransactionsListNewCustomerTransactionsToAttach.getClass(), customerTransactionsListNewCustomerTransactionsToAttach.getCustomerTransactionID());
                attachedCustomerTransactionsListNew.add(customerTransactionsListNewCustomerTransactionsToAttach);
            }
            customerTransactionsListNew = attachedCustomerTransactionsListNew;
            invoices.setCustomerTransactionsList(customerTransactionsListNew);
            invoices = em.merge(invoices);
            if (customerIDOld != null && !customerIDOld.equals(customerIDNew)) {
                customerIDOld.getInvoicesList().remove(invoices);
                customerIDOld = em.merge(customerIDOld);
            }
            if (customerIDNew != null && !customerIDNew.equals(customerIDOld)) {
                customerIDNew.getInvoicesList().add(invoices);
                customerIDNew = em.merge(customerIDNew);
            }
            if (billToCustomerIDOld != null && !billToCustomerIDOld.equals(billToCustomerIDNew)) {
                billToCustomerIDOld.getInvoicesList().remove(invoices);
                billToCustomerIDOld = em.merge(billToCustomerIDOld);
            }
            if (billToCustomerIDNew != null && !billToCustomerIDNew.equals(billToCustomerIDOld)) {
                billToCustomerIDNew.getInvoicesList().add(invoices);
                billToCustomerIDNew = em.merge(billToCustomerIDNew);
            }
            if (orderIDOld != null && !orderIDOld.equals(orderIDNew)) {
                orderIDOld.getInvoicesList().remove(invoices);
                orderIDOld = em.merge(orderIDOld);
            }
            if (orderIDNew != null && !orderIDNew.equals(orderIDOld)) {
                orderIDNew.getInvoicesList().add(invoices);
                orderIDNew = em.merge(orderIDNew);
            }
            for (InvoiceLines invoiceLinesListNewInvoiceLines : invoiceLinesListNew) {
                if (!invoiceLinesListOld.contains(invoiceLinesListNewInvoiceLines)) {
                    Invoices oldInvoiceIDOfInvoiceLinesListNewInvoiceLines = invoiceLinesListNewInvoiceLines.getInvoiceID();
                    invoiceLinesListNewInvoiceLines.setInvoiceID(invoices);
                    invoiceLinesListNewInvoiceLines = em.merge(invoiceLinesListNewInvoiceLines);
                    if (oldInvoiceIDOfInvoiceLinesListNewInvoiceLines != null && !oldInvoiceIDOfInvoiceLinesListNewInvoiceLines.equals(invoices)) {
                        oldInvoiceIDOfInvoiceLinesListNewInvoiceLines.getInvoiceLinesList().remove(invoiceLinesListNewInvoiceLines);
                        oldInvoiceIDOfInvoiceLinesListNewInvoiceLines = em.merge(oldInvoiceIDOfInvoiceLinesListNewInvoiceLines);
                    }
                }
            }
            for (CustomerTransactions customerTransactionsListOldCustomerTransactions : customerTransactionsListOld) {
                if (!customerTransactionsListNew.contains(customerTransactionsListOldCustomerTransactions)) {
                    customerTransactionsListOldCustomerTransactions.setInvoiceID(null);
                    customerTransactionsListOldCustomerTransactions = em.merge(customerTransactionsListOldCustomerTransactions);
                }
            }
            for (CustomerTransactions customerTransactionsListNewCustomerTransactions : customerTransactionsListNew) {
                if (!customerTransactionsListOld.contains(customerTransactionsListNewCustomerTransactions)) {
                    Invoices oldInvoiceIDOfCustomerTransactionsListNewCustomerTransactions = customerTransactionsListNewCustomerTransactions.getInvoiceID();
                    customerTransactionsListNewCustomerTransactions.setInvoiceID(invoices);
                    customerTransactionsListNewCustomerTransactions = em.merge(customerTransactionsListNewCustomerTransactions);
                    if (oldInvoiceIDOfCustomerTransactionsListNewCustomerTransactions != null && !oldInvoiceIDOfCustomerTransactionsListNewCustomerTransactions.equals(invoices)) {
                        oldInvoiceIDOfCustomerTransactionsListNewCustomerTransactions.getCustomerTransactionsList().remove(customerTransactionsListNewCustomerTransactions);
                        oldInvoiceIDOfCustomerTransactionsListNewCustomerTransactions = em.merge(oldInvoiceIDOfCustomerTransactionsListNewCustomerTransactions);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = invoices.getInvoiceID();
                if (findInvoices(id) == null) {
                    throw new NonexistentEntityException("The invoices with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Invoices invoices;
            try {
                invoices = em.getReference(Invoices.class, id);
                invoices.getInvoiceID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The invoices with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<InvoiceLines> invoiceLinesListOrphanCheck = invoices.getInvoiceLinesList();
            for (InvoiceLines invoiceLinesListOrphanCheckInvoiceLines : invoiceLinesListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Invoices (" + invoices + ") cannot be destroyed since the InvoiceLines " + invoiceLinesListOrphanCheckInvoiceLines + " in its invoiceLinesList field has a non-nullable invoiceID field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Customers customerID = invoices.getCustomerID();
            if (customerID != null) {
                customerID.getInvoicesList().remove(invoices);
                customerID = em.merge(customerID);
            }
            Customers billToCustomerID = invoices.getBillToCustomerID();
            if (billToCustomerID != null) {
                billToCustomerID.getInvoicesList().remove(invoices);
                billToCustomerID = em.merge(billToCustomerID);
            }
            Orders orderID = invoices.getOrderID();
            if (orderID != null) {
                orderID.getInvoicesList().remove(invoices);
                orderID = em.merge(orderID);
            }
            List<CustomerTransactions> customerTransactionsList = invoices.getCustomerTransactionsList();
            for (CustomerTransactions customerTransactionsListCustomerTransactions : customerTransactionsList) {
                customerTransactionsListCustomerTransactions.setInvoiceID(null);
                customerTransactionsListCustomerTransactions = em.merge(customerTransactionsListCustomerTransactions);
            }
            em.remove(invoices);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Invoices> findInvoicesEntities() {
        return findInvoicesEntities(true, -1, -1);
    }

    public List<Invoices> findInvoicesEntities(int maxResults, int firstResult) {
        return findInvoicesEntities(false, maxResults, firstResult);
    }
    
    public List<Invoices> findInvoicesEntitiesWithPaging(int pageNo)
    { 	
    	return findInvoicesEntities(false, 10, pageNo * 10);    
    }

    private List<Invoices> findInvoicesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Invoices.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Invoices findInvoices(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Invoices.class, id);
        } finally {
            em.close();
        }
    }

    public int getInvoicesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Invoices> rt = cq.from(Invoices.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
