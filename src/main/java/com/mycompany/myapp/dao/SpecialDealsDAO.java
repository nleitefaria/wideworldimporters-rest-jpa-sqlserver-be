package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.SpecialDeals;

public interface SpecialDealsDAO {
	
	int getSpecialDealsCount();
	SpecialDeals findSpecialDeals(Integer id);
	List<SpecialDeals> findSpecialDealsEntities();
	List<SpecialDeals> findSpecialDealsEntities(int maxResults, int firstResult);
	List<SpecialDeals> findSpecialDealsEntitiesWithPaging(int pageNo);

}
