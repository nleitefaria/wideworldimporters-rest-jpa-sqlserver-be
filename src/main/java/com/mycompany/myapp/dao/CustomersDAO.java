package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.Customers;

public interface CustomersDAO
{
	int getCustomersCount();
	Customers findCustomers(Integer id);
	List<Customers> findCustomersEntities();

}
