package com.mycompany.myapp.dao;

import java.util.List;

import com.mycompany.myapp.entity.CustomerTransactions;

public interface CustomerTransactionsDAO 
{	
	int getCustomerTransactionsCount();
	CustomerTransactions findCustomerTransactions(Integer id);
	List<CustomerTransactions> findCustomerTransactionsEntities();
	List<CustomerTransactions> findCustomerTransactionsEntities(int maxResults, int firstResult);

}
