package com.mycompany.myapp;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.mycompany.myapp.rws.BuyingGroupsRWS;

public class MyAppApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();

	public MyAppApplication() {
		singletons.add(new BuyingGroupsRWS());
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
