package com.mycompany.myapp.service;

import java.util.List;
import com.mycompany.myapp.dto.CustomersDTO;

public interface CustomersService 
{
	int count();
	CustomersDTO findCustomer(Integer id);
	List<CustomersDTO> findCustomersEntities();
	
	

}
