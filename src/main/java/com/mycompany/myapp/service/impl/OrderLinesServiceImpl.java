package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.OrderLinesDAO;
import com.mycompany.myapp.dao.impl.OrderLinesDAOImpl;
import com.mycompany.myapp.dto.OrderLinesDTO;
import com.mycompany.myapp.dto.PagingDTO;
import com.mycompany.myapp.entity.OrderLines;
import com.mycompany.myapp.service.OrderLinesService;

public class OrderLinesServiceImpl implements OrderLinesService
{
	private EntityManagerFactory emf;
	OrderLinesDAO orderLinesDAO;
	
	public OrderLinesServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		orderLinesDAO = new OrderLinesDAOImpl(emf);		
	}

	public int getOrderLinesCount()
	{
		return orderLinesDAO.getOrderLinesCount();		
	}
	
	public OrderLinesDTO findOrderLines(Integer id)
	{
		OrderLines orderLines = orderLinesDAO.findOrderLines(id);
		OrderLinesDTO orderLinesDTO = null;
		if(orderLines != null)
		{
			orderLinesDTO = new OrderLinesDTO(orderLines.getOrderLineID(), orderLines.getDescription(), orderLines.getQuantity(), orderLines.getUnitPrice(), orderLines.getTaxRate(), orderLines.getPickedQuantity(), orderLines.getPickingCompletedWhen(), orderLines.getLastEditedBy(), orderLines.getLastEditedWhen());
		}
		return orderLinesDTO;
	}
	
	public PagingDTO findOrderLinesEntitiesWithPaging(int pageNo)
	{
		PagingDTO ret = new PagingDTO(); 
		List<OrderLinesDTO> content = new ArrayList<OrderLinesDTO>();		
		OrderLinesDTO orderLinesDTO = null;
		for(OrderLines orderLines : orderLinesDAO.findOrderLinesEntitiesWithPaging(pageNo -1))
		{
			orderLinesDTO = new OrderLinesDTO(orderLines.getOrderLineID(), orderLines.getDescription(), orderLines.getQuantity(), orderLines.getUnitPrice(), orderLines.getTaxRate(), orderLines.getPickedQuantity(), orderLines.getPickingCompletedWhen(), orderLines.getLastEditedBy(), orderLines.getLastEditedWhen());
			content.add(orderLinesDTO);
		}
		ret.setContent(content);
		ret.setTotalElements(orderLinesDAO.getOrderLinesCount());
		return ret;	
	}
}
