package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.InvoicesDAO;
import com.mycompany.myapp.dao.impl.InvoicesDAOImpl;
import com.mycompany.myapp.dto.InvoicesDTO;
import com.mycompany.myapp.dto.PagingDTO;
import com.mycompany.myapp.entity.Invoices;
import com.mycompany.myapp.service.InvoicesService;

public class InvoicesServiceImpl implements InvoicesService
{
	private EntityManagerFactory emf;
	InvoicesDAO invoicesDAO;
	
	public InvoicesServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		invoicesDAO = new InvoicesDAOImpl(emf);		
	}
		
	public int count()
	{		
		return invoicesDAO.getInvoicesCount();		
	}
	
	public InvoicesDTO findInvoice(Integer id)
	{
		Invoices invoice = invoicesDAO.findInvoices(id);
		InvoicesDTO invoiceDTO = null;
		if(invoice != null)
		{
			invoiceDTO = new InvoicesDTO(invoice.getInvoiceID(), invoice.getInvoiceDate(), invoice.getCustomerPurchaseOrderNumber(), invoice.getIsCreditNote(), invoice.getCreditNoteReason(), invoice.getComments(), invoice.getDeliveryInstructions(), invoice.getInternalComments(), invoice.getTotalDryItems(), invoice.getTotalChillerItems(), invoice.getDeliveryRun(), invoice.getRunPosition(), invoice.getReturnedDeliveryData(), invoice.getConfirmedDeliveryTime(), invoice.getConfirmedReceivedBy(), invoice.getLastEditedBy(), invoice.getLastEditedWhen());
		}
		return invoiceDTO;
	}
	
	public PagingDTO findInvoicesWithPaging(int pageNo)
	{
		PagingDTO ret = new PagingDTO(); 
		List<InvoicesDTO> content = new ArrayList<InvoicesDTO>();		
		InvoicesDTO invoicesDTO = null;
		for(Invoices invoice : invoicesDAO.findInvoicesEntitiesWithPaging(pageNo -1))
		{
			invoicesDTO = new InvoicesDTO(invoice.getInvoiceID(), invoice.getInvoiceDate(), invoice.getCustomerPurchaseOrderNumber(), invoice.getIsCreditNote(), invoice.getCreditNoteReason(), invoice.getComments(), invoice.getDeliveryInstructions(), invoice.getInternalComments(), invoice.getTotalDryItems(), invoice.getTotalChillerItems(), invoice.getDeliveryRun(), invoice.getRunPosition(), invoice.getReturnedDeliveryData(), invoice.getConfirmedDeliveryTime(), invoice.getConfirmedReceivedBy(), invoice.getLastEditedBy(), invoice.getLastEditedWhen());
			content.add(invoicesDTO);
		}
		ret.setContent(content);
		ret.setTotalElements(invoicesDAO.getInvoicesCount());
		return ret;	
	}
	
	
	

}
