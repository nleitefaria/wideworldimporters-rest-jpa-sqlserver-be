package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.OrdersDAO;
import com.mycompany.myapp.dao.impl.OrdersDAOImpl;
import com.mycompany.myapp.dto.OrdersDTO;
import com.mycompany.myapp.dto.PagingDTO;
import com.mycompany.myapp.entity.Orders;
import com.mycompany.myapp.service.OrdersService;

public class OrdersServiceImpl implements  OrdersService
{
	private EntityManagerFactory emf;
	OrdersDAO ordersDAO;
	
	public OrdersServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		ordersDAO = new OrdersDAOImpl(emf);		
	}
	
	public int getOrdersCount()
	{
		return ordersDAO.getOrdersCount();		
	}
	
	public OrdersDTO findOrders(Integer id)
	{
		Orders orders = ordersDAO.findOrders(id);
		OrdersDTO ordersDTO = null;
		if(orders != null)
		{
			ordersDTO = new OrdersDTO(orders.getOrderID(), orders.getOrderDate(), orders.getExpectedDeliveryDate(), orders.getCustomerPurchaseOrderNumber(), orders.getIsUndersupplyBackordered(), orders.getComments(), orders.getDeliveryInstructions(), orders.getInternalComments(), orders.getPickingCompletedWhen(), orders.getLastEditedBy(), orders.getLastEditedWhen());
		}
		return ordersDTO;
	}
	
	public PagingDTO findOrderLinesEntitiesWithPaging(int pageNo)
	{
		PagingDTO ret = new PagingDTO(); 
		List<OrdersDTO> content = new ArrayList<OrdersDTO>();		
		OrdersDTO ordersDTO = null;
		for(Orders orders : ordersDAO.findOrdersEntitiesWithPaging(pageNo -1))
		{
			ordersDTO = new OrdersDTO(orders.getOrderID(), orders.getOrderDate(), orders.getExpectedDeliveryDate(), orders.getCustomerPurchaseOrderNumber(), orders.getIsUndersupplyBackordered(), orders.getComments(), orders.getDeliveryInstructions(), orders.getInternalComments(), orders.getPickingCompletedWhen(), orders.getLastEditedBy(), orders.getLastEditedWhen());
			content.add(ordersDTO);
		}
		ret.setContent(content);
		ret.setTotalElements(ordersDAO.getOrdersCount());
		return ret;	
	}

}
