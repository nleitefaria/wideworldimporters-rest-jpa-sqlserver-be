package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.SpecialDealsDAO;
import com.mycompany.myapp.dao.impl.SpecialDealsDAOImpl;
import com.mycompany.myapp.dto.PagingDTO;
import com.mycompany.myapp.dto.SpecialDealsDTO;
import com.mycompany.myapp.entity.SpecialDeals;
import com.mycompany.myapp.service.SpecialDealsService;

public class SpecialDealsServiceImpl implements SpecialDealsService 
{
	private EntityManagerFactory emf;
	SpecialDealsDAO specialDealsDAO;
	
	public SpecialDealsServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		specialDealsDAO = new SpecialDealsDAOImpl(emf);		
	}

	public int getSpecialDealsCount()
	{
		return specialDealsDAO.getSpecialDealsCount();		
	}

	public SpecialDealsDTO findSpecialDeals(Integer id)
	{
		SpecialDeals specialDeal = specialDealsDAO.findSpecialDeals(id);
		SpecialDealsDTO specialDealsDTO = null;
		if(specialDeal != null)
		{
			specialDealsDTO = new SpecialDealsDTO(specialDeal.getSpecialDealID(), specialDeal.getDealDescription(), specialDeal.getStartDate(), specialDeal.getEndDate(), specialDeal.getLastEditedBy(), specialDeal.getLastEditedWhen());			
		}
		return specialDealsDTO;
	}

	public List<SpecialDealsDTO> findSpecialDealsEntities()
	{		
		List<SpecialDealsDTO> ret = new ArrayList<SpecialDealsDTO>();
		SpecialDealsDTO specialDealsDTO = null;
		for(SpecialDeals specialDeal : specialDealsDAO.findSpecialDealsEntities())
		{
			specialDealsDTO = new SpecialDealsDTO(specialDeal.getSpecialDealID(), specialDeal.getDealDescription(), specialDeal.getStartDate(), specialDeal.getEndDate(), specialDeal.getLastEditedBy(), specialDeal.getLastEditedWhen());
			ret.add(specialDealsDTO);		
		}		
		return ret;	
	}
	
	public List<SpecialDealsDTO> findSpecialDealsWithPaging(int maxResults, int firstResult)
	{		
		List<SpecialDealsDTO> ret = new ArrayList<SpecialDealsDTO>();
		SpecialDealsDTO specialDealsDTO = null;
		for(SpecialDeals specialDeal : specialDealsDAO.findSpecialDealsEntities(maxResults, firstResult))
		{
			specialDealsDTO = new SpecialDealsDTO(specialDeal.getSpecialDealID(), specialDeal.getDealDescription(), specialDeal.getStartDate(), specialDeal.getEndDate(), specialDeal.getLastEditedBy(), specialDeal.getLastEditedWhen());
			ret.add(specialDealsDTO);		
		}		
		return ret;	
	}
	
	public PagingDTO findSpecialDealsWithPaging(int pageNo)
	{
		PagingDTO ret = new PagingDTO(); 
		List<SpecialDealsDTO> content = new ArrayList<SpecialDealsDTO>();		
		SpecialDealsDTO specialDealsDTO = null;
		for(SpecialDeals specialDeal : specialDealsDAO.findSpecialDealsEntitiesWithPaging(pageNo -1))
		{
			specialDealsDTO = new SpecialDealsDTO(specialDeal.getSpecialDealID(), specialDeal.getDealDescription(), specialDeal.getStartDate(), specialDeal.getEndDate(), specialDeal.getLastEditedBy(), specialDeal.getLastEditedWhen());
			content.add(specialDealsDTO);
		}
		ret.setContent(content);
		ret.setTotalElements(specialDealsDAO.getSpecialDealsCount());
		return ret;	
	}

}
