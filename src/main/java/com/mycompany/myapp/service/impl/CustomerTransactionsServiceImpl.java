package com.mycompany.myapp.service.impl;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.CustomerTransactionsDAO;
import com.mycompany.myapp.dao.impl.CustomerTransactionsDAOImpl;
import com.mycompany.myapp.service.CustomerTransactionsService;

public class CustomerTransactionsServiceImpl implements CustomerTransactionsService
{
	private EntityManagerFactory emf;
	CustomerTransactionsDAO customerTransactionsDAO;
	
	public CustomerTransactionsServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		customerTransactionsDAO = new CustomerTransactionsDAOImpl(emf);		
	}
		
	public int count()
	{		
		return customerTransactionsDAO.getCustomerTransactionsCount();
	}
	
	
	
	

}
