package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.CustomersDAO;
import com.mycompany.myapp.dao.impl.CustomersDAOImpl;
import com.mycompany.myapp.dto.CustomersDTO;
import com.mycompany.myapp.entity.Customers;
import com.mycompany.myapp.service.CustomersService;

public class CustomersServiceImpl implements CustomersService 
{	
	private EntityManagerFactory emf;
	CustomersDAO customersDAO;
	
	public CustomersServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		customersDAO = new CustomersDAOImpl(emf);		
	}
	
	public int count()
	{		
		return customersDAO.getCustomersCount();
	}
	
	public CustomersDTO findCustomer(Integer id)
	{
		Customers customer = customersDAO.findCustomers(id);
		CustomersDTO customersDTO = null;
		if(customer != null)
		{
			customersDTO = new CustomersDTO(customer.getCustomerID(), customer.getCustomerName() , customer.getPrimaryContactPersonID(), customer.getDeliveryMethodID() , customer.getDeliveryCityID() , customer.getPostalCityID(), customer.getAccountOpenedDate() , customer.getStandardDiscountPercentage() , customer.getIsStatementSent() , customer.getIsOnCreditHold(), customer.getPaymentDays(), customer.getPhoneNumber(), customer.getFaxNumber(), customer.getWebsiteURL(), customer.getDeliveryAddressLine1(), customer.getDeliveryPostalCode(), customer.getPostalAddressLine1(), customer.getPostalPostalCode(), customer.getLastEditedBy(), customer.getValidFrom() , customer.getValidTo());
		}
		return customersDTO;
	}
	
	public List<CustomersDTO> findCustomersEntities()
	{        		
		List<CustomersDTO> ret = new ArrayList<CustomersDTO>();
		CustomersDTO customersDTO = null;
		for(Customers customer : customersDAO.findCustomersEntities())
		{
			customersDTO = new CustomersDTO(customer.getCustomerID(), customer.getCustomerName() , customer.getPrimaryContactPersonID(), customer.getDeliveryMethodID() , customer.getDeliveryCityID() , customer.getPostalCityID(), customer.getAccountOpenedDate() , customer.getStandardDiscountPercentage() , customer.getIsStatementSent() , customer.getIsOnCreditHold(), customer.getPaymentDays(), customer.getPhoneNumber(), customer.getFaxNumber(), customer.getWebsiteURL(), customer.getDeliveryAddressLine1(), customer.getDeliveryPostalCode(), customer.getPostalAddressLine1(), customer.getPostalPostalCode(), customer.getLastEditedBy(), customer.getValidFrom() , customer.getValidTo());
			ret.add(customersDTO);		
		}		
		return ret;	
    }

}
