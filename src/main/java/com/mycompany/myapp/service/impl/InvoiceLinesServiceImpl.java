package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.mycompany.myapp.dao.InvoiceLinesDAO;
import com.mycompany.myapp.dao.impl.InvoiceLinesDAOImpl;
import com.mycompany.myapp.dto.InvoiceLinesDTO;
import com.mycompany.myapp.entity.InvoiceLines;
import com.mycompany.myapp.service.InvoiceLinesService;

public class InvoiceLinesServiceImpl implements InvoiceLinesService
{	
	private EntityManagerFactory emf;
	InvoiceLinesDAO invoiceLinesDAO;
	
	public InvoiceLinesServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		invoiceLinesDAO = new InvoiceLinesDAOImpl(emf);		
	}
		
	public int count()
	{		
		return invoiceLinesDAO.getInvoiceLinesCount();		
	}
	
	public InvoiceLinesDTO findInvoiceLine(Integer id)
	{
		InvoiceLines invoiceLine = invoiceLinesDAO.findInvoiceLines(id);
		InvoiceLinesDTO invoiceLineDTO = null;
		if(invoiceLine != null)
		{
			invoiceLineDTO = new InvoiceLinesDTO(invoiceLine.getInvoiceLineID(), invoiceLine.getDescription(), invoiceLine.getLastEditedBy(), invoiceLine.getQuantity(), invoiceLine.getUnitPrice(), invoiceLine.getTaxRate(), invoiceLine.getTaxAmount(), invoiceLine.getLineProfit(), invoiceLine.getExtendedPrice(), invoiceLine.getLastEditedBy(), invoiceLine.getLastEditedWhen());
		}
		return invoiceLineDTO;
	}
	
	public List<InvoiceLinesDTO> findInvoiceLinesEntities()
	{		
		List<InvoiceLinesDTO> ret = new ArrayList<InvoiceLinesDTO>();
		InvoiceLinesDTO invoiceLinesDTO = null;
		for(InvoiceLines invoiceLine : invoiceLinesDAO.findInvoiceLinesEntities())
		{
			invoiceLinesDTO = new InvoiceLinesDTO(invoiceLine.getInvoiceLineID(), invoiceLine.getDescription(), invoiceLine.getLastEditedBy(), invoiceLine.getQuantity(), invoiceLine.getUnitPrice(), invoiceLine.getTaxRate(), invoiceLine.getTaxAmount(), invoiceLine.getLineProfit(), invoiceLine.getExtendedPrice(), invoiceLine.getLastEditedBy(), invoiceLine.getLastEditedWhen()); 
			ret.add(invoiceLinesDTO);		
		}		
		return ret;	
	}
	
	public List<InvoiceLinesDTO> findInvoiceLinesEntitiesWithPaging(int maxResults, int firstResult)
	{		
		List<InvoiceLinesDTO> ret = new ArrayList<InvoiceLinesDTO>();
		InvoiceLinesDTO invoiceLinesDTO = null;
		for(InvoiceLines invoiceLine : invoiceLinesDAO.findInvoiceLinesEntities(maxResults, firstResult))
		{
			invoiceLinesDTO = new InvoiceLinesDTO(invoiceLine.getInvoiceLineID(), invoiceLine.getDescription(), invoiceLine.getLastEditedBy(), invoiceLine.getQuantity(), invoiceLine.getUnitPrice(), invoiceLine.getTaxRate(), invoiceLine.getTaxAmount(), invoiceLine.getLineProfit(), invoiceLine.getExtendedPrice(), invoiceLine.getLastEditedBy(), invoiceLine.getLastEditedWhen()); 
			ret.add(invoiceLinesDTO);		
		}		
		return ret;	
	}

}
