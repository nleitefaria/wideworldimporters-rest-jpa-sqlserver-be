package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.CustomerCategoriesDAO;
import com.mycompany.myapp.dao.impl.CustomerCategoriesDAOImpl;
import com.mycompany.myapp.dto.CustomerCategoriesDTO;
import com.mycompany.myapp.entity.CustomerCategories;
import com.mycompany.myapp.service.CustomerCategoriesService;

public class CustomerCategoriesServiceImpl implements CustomerCategoriesService{

	private EntityManagerFactory emf;
	CustomerCategoriesDAO customerCategoriesDAO;
	
	public CustomerCategoriesServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		customerCategoriesDAO = new CustomerCategoriesDAOImpl(emf);		
	}
		
	public int count()
	{		
		return customerCategoriesDAO.getCustomerCategoriesCount();
	}
	
	public List<CustomerCategoriesDTO> findCustomerCategoriesEntities()
	{        		
		List<CustomerCategoriesDTO> ret = new ArrayList<CustomerCategoriesDTO>();
		CustomerCategoriesDTO customerCategoriesDTO = null;
		for(CustomerCategories customerCategories : customerCategoriesDAO.findCustomerCategoriesEntities())
		{
			customerCategoriesDTO = new CustomerCategoriesDTO(customerCategories.getCustomerCategoryID(), customerCategories.getCustomerCategoryName(), customerCategories.getLastEditedBy(), customerCategories.getValidFrom(), customerCategories.getValidTo());
			ret.add(customerCategoriesDTO);		
		}		
		return ret;	
    }
	
	public CustomerCategoriesDTO findCustomerCategories(Integer id)
	{
		CustomerCategories customerCategories = customerCategoriesDAO.findCustomerCategories(id);
		CustomerCategoriesDTO customerCategoriesDTO = null;
		if(customerCategories != null)
		{
			customerCategoriesDTO = new CustomerCategoriesDTO(customerCategories.getCustomerCategoryID(), customerCategories.getCustomerCategoryName(), customerCategories.getLastEditedBy(), customerCategories.getValidFrom(), customerCategories.getValidTo());
		}
		return customerCategoriesDTO;
	}

	
	

}
