package com.mycompany.myapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.BuyingGroupsDAO;
import com.mycompany.myapp.dao.impl.BuyingGroupsDAOImpl;
import com.mycompany.myapp.dto.BuyingGroupsDTO;
import com.mycompany.myapp.dto.PagingDTO;
import com.mycompany.myapp.entity.BuyingGroups;
import com.mycompany.myapp.service.BuyingGroupsService;

public class BuyingGroupsServiceImpl implements BuyingGroupsService 
{
	private EntityManagerFactory emf;
	BuyingGroupsDAO buyingGroupsDAO;
	
	public BuyingGroupsServiceImpl()
	{
		emf = Persistence.createEntityManagerFactory("JPASQLServer4PU"); 
		buyingGroupsDAO = new BuyingGroupsDAOImpl(emf);		
	}
		
	public int count()
	{		
		return buyingGroupsDAO.getBuyingGroupsCount();		
	}
	
	public List<BuyingGroupsDTO> findBuyingGroupsEntities()
	{        		
		List<BuyingGroupsDTO> ret = new ArrayList<BuyingGroupsDTO>();
		BuyingGroupsDTO buyingGroupsDTO = null;
		for(BuyingGroups buyingGroups : buyingGroupsDAO.findBuyingGroupsEntities())
		{
			buyingGroupsDTO = new BuyingGroupsDTO(buyingGroups.getBuyingGroupID(), buyingGroups.getBuyingGroupName(), buyingGroups.getLastEditedBy() , buyingGroups.getValidFrom(), buyingGroups.getValidTo());
			ret.add(buyingGroupsDTO);		
		}		
		return ret;	
    }
	
	
	public BuyingGroupsDTO findBuyingGroups(Integer id)
	{
		BuyingGroups buyingGroups = buyingGroupsDAO.findBuyingGroups(id);
		BuyingGroupsDTO buyingGroupsDTO = null;
		if(buyingGroups != null)
		{
			buyingGroupsDTO = new BuyingGroupsDTO(buyingGroups.getBuyingGroupID(), buyingGroups.getBuyingGroupName(), buyingGroups.getLastEditedBy() , buyingGroups.getValidFrom(), buyingGroups.getValidTo());
		}
		return buyingGroupsDTO;
	}
	
	public PagingDTO findBuyingGroupsWithPaging(int pageNo)
	{
		PagingDTO ret = new PagingDTO(); 
		List<BuyingGroupsDTO> content = new ArrayList<BuyingGroupsDTO>();		
		BuyingGroupsDTO buyingGroupsDTO = null;
		for(BuyingGroups buyingGroups : buyingGroupsDAO.findSpecialDealsEntitiesWithPaging(pageNo -1))
		{
			buyingGroupsDTO = new BuyingGroupsDTO(buyingGroups.getBuyingGroupID(), buyingGroups.getBuyingGroupName(), buyingGroups.getLastEditedBy() , buyingGroups.getValidFrom(), buyingGroups.getValidTo());
			content.add(buyingGroupsDTO);
		}
		ret.setContent(content);
		ret.setTotalElements(buyingGroupsDAO.getBuyingGroupsCount());
		return ret;	
	}
}
