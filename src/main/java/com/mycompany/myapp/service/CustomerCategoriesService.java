package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.CustomerCategoriesDTO;

public interface CustomerCategoriesService {
	
	int count();
	CustomerCategoriesDTO findCustomerCategories(Integer id);
	List<CustomerCategoriesDTO> findCustomerCategoriesEntities();

}
