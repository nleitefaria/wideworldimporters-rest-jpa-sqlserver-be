package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.InvoiceLinesDTO;

public interface InvoiceLinesService 
{
	int count();
	InvoiceLinesDTO findInvoiceLine(Integer id);
	List<InvoiceLinesDTO> findInvoiceLinesEntities();
	List<InvoiceLinesDTO> findInvoiceLinesEntitiesWithPaging(int maxResults, int firstResult);

}
