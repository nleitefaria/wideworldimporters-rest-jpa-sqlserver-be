package com.mycompany.myapp.service;

import com.mycompany.myapp.dto.OrderLinesDTO;
import com.mycompany.myapp.dto.PagingDTO;

public interface OrderLinesService
{
	int getOrderLinesCount();
	OrderLinesDTO findOrderLines(Integer id);
	PagingDTO findOrderLinesEntitiesWithPaging(int pageNo);

}
