package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.BuyingGroupsDTO;
import com.mycompany.myapp.dto.PagingDTO;

public interface BuyingGroupsService 
{
	int count();
	BuyingGroupsDTO findBuyingGroups(Integer id);
	List<BuyingGroupsDTO> findBuyingGroupsEntities();
	PagingDTO findBuyingGroupsWithPaging(int pageNo);
}
