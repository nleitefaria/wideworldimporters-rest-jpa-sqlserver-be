package com.mycompany.myapp.service;

import com.mycompany.myapp.dto.InvoicesDTO;
import com.mycompany.myapp.dto.PagingDTO;

public interface InvoicesService {
	
	int count();
	InvoicesDTO findInvoice(Integer id);
	PagingDTO findInvoicesWithPaging(int pageNo);

}
