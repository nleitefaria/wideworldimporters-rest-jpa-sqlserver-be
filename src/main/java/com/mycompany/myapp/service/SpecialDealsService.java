package com.mycompany.myapp.service;

import java.util.List;

import com.mycompany.myapp.dto.PagingDTO;
import com.mycompany.myapp.dto.SpecialDealsDTO;

public interface SpecialDealsService
{
	int getSpecialDealsCount();
	SpecialDealsDTO findSpecialDeals(Integer id);
	List<SpecialDealsDTO> findSpecialDealsEntities();
	List<SpecialDealsDTO> findSpecialDealsWithPaging(int maxResults, int firstResult);
	//List<SpecialDealsDTO> findSpecialDealsWithPaging(int pageNo);
	PagingDTO findSpecialDealsWithPaging(int pageNo);

}
