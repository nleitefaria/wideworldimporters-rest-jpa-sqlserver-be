package com.mycompany.myapp.service;

import com.mycompany.myapp.dto.OrdersDTO;
import com.mycompany.myapp.dto.PagingDTO;

public interface OrdersService {
	
	int getOrdersCount();
	OrdersDTO findOrders(Integer id);
	PagingDTO findOrderLinesEntitiesWithPaging(int pageNo);

}
