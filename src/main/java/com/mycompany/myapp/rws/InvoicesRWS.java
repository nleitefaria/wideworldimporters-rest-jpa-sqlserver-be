package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.InvoicesDTO;
import com.mycompany.myapp.service.InvoicesService;
import com.mycompany.myapp.service.impl.InvoicesServiceImpl;

@Path("/invoices")
public class InvoicesRWS 
{
	InvoicesService invoicesService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		invoicesService = new InvoicesServiceImpl();
		return Response.status(200).entity(Integer.toString(invoicesService.count())).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		invoicesService = new InvoicesServiceImpl();	
		InvoicesDTO invoicesDTO = invoicesService.findInvoice(Integer.parseInt(id));
		if(invoicesDTO != null)
		{
			return Response.status(200).entity(invoicesDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/page/{pageNo}")
	@Produces("application/json")
	public Response findAllWithPaging(@PathParam("pageNo") String pageNo)
	{		
		invoicesService = new InvoicesServiceImpl();	
		return Response.status(200).entity(invoicesService.findInvoicesWithPaging(new Integer(pageNo))).build();
	}
	

}
