package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.OrderLinesDTO;
import com.mycompany.myapp.service.OrderLinesService;
import com.mycompany.myapp.service.impl.OrderLinesServiceImpl;

@Path("/orderlines")
public class OrderLinesRWS 
{
	OrderLinesService orderLinesService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		orderLinesService = new OrderLinesServiceImpl();		
		return Response.status(200).entity(Integer.toString(orderLinesService.getOrderLinesCount())).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		orderLinesService = new OrderLinesServiceImpl();	
		OrderLinesDTO orderLinesDTO = orderLinesService.findOrderLines(Integer.parseInt(id));
		if(orderLinesDTO != null)
		{
			return Response.status(200).entity(orderLinesDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/page/{pageNo}")
	@Produces("application/json")
	public Response findAllWithPaging(@PathParam("pageNo") String pageNo)
	{		
		orderLinesService = new OrderLinesServiceImpl();	
		return Response.status(200).entity(orderLinesService.findOrderLinesEntitiesWithPaging(new Integer(pageNo))).build();
	}
	
	

}
