package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.CustomerCategoriesDTO;
import com.mycompany.myapp.service.CustomerCategoriesService;
import com.mycompany.myapp.service.impl.CustomerCategoriesServiceImpl;

@Path("/customercategories")
public class CustomerCategoriesRWS 
{
	@GET
	@Path("/")
	@Produces("application/json")
	public Response findAll()
	{		
		CustomerCategoriesService customerCategoriesService = new CustomerCategoriesServiceImpl();
		return Response.status(200).entity(customerCategoriesService.findCustomerCategoriesEntities()).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		CustomerCategoriesService customerCategoriesService = new CustomerCategoriesServiceImpl();		
		CustomerCategoriesDTO customerCategoriesDTO = customerCategoriesService.findCustomerCategories(Integer.parseInt(id));
		if(customerCategoriesDTO != null)
		{
			return Response.status(200).entity(customerCategoriesDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/count")
	public Response count()
	{		
		CustomerCategoriesService customerCategoriesService = new CustomerCategoriesServiceImpl();
		return Response.status(200).entity(Integer.toString(customerCategoriesService.count())).build();
	}

}
