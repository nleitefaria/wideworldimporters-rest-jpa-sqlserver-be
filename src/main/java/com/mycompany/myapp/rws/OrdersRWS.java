package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.OrdersDTO;
import com.mycompany.myapp.service.OrdersService;
import com.mycompany.myapp.service.impl.OrdersServiceImpl;

@Path("/orders")
public class OrdersRWS {
	
	OrdersService ordersService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		ordersService = new OrdersServiceImpl();		
		return Response.status(200).entity(Integer.toString(ordersService.getOrdersCount())).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		ordersService = new OrdersServiceImpl();	
		OrdersDTO ordersDTO = ordersService.findOrders(Integer.parseInt(id));
		if(ordersDTO != null)
		{
			return Response.status(200).entity(ordersDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/page/{pageNo}")
	@Produces("application/json")
	public Response findAllWithPaging(@PathParam("pageNo") String pageNo)
	{		
		ordersService = new OrdersServiceImpl();	
		return Response.status(200).entity(ordersService.findOrderLinesEntitiesWithPaging(new Integer(pageNo))).build();
	}

}
