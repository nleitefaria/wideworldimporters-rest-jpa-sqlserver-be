package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.service.CustomerTransactionsService;
import com.mycompany.myapp.service.impl.CustomerTransactionsServiceImpl;

@Path("/customertransactions")
public class CustomerTransactionsRWS 
{
	CustomerTransactionsService customerTransactionsService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		customerTransactionsService = new CustomerTransactionsServiceImpl();
		return Response.status(200).entity(Integer.toString(customerTransactionsService.count())).build();
	}
	
	

}
