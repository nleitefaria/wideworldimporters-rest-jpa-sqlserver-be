package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.BuyingGroupsDTO;
import com.mycompany.myapp.service.BuyingGroupsService;
import com.mycompany.myapp.service.impl.BuyingGroupsServiceImpl;

@Path("/buyinggroups")
public class BuyingGroupsRWS
{
	BuyingGroupsService buyingGroupsService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		buyingGroupsService = new BuyingGroupsServiceImpl();
		return Response.status(200).entity(Integer.toString(buyingGroupsService.count())).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		buyingGroupsService = new BuyingGroupsServiceImpl();		
		BuyingGroupsDTO buyingGroupsDTO = buyingGroupsService.findBuyingGroups(Integer.parseInt(id));		
		if(buyingGroupsDTO != null)
		{
			return Response.status(200).entity(buyingGroupsDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public Response findAll()
	{		
		buyingGroupsService = new BuyingGroupsServiceImpl();
		return Response.status(200).entity(buyingGroupsService.findBuyingGroupsEntities()).build();
	}
	
	@GET
	@Path("/page/{pageNo}")
	@Produces("application/json")
	public Response findAllWithPaging(@PathParam("pageNo") String pageNo)
	{		
		buyingGroupsService = new BuyingGroupsServiceImpl();
		return Response.status(200).entity(buyingGroupsService.findBuyingGroupsWithPaging(new Integer(pageNo))).build();
	}
	
	
	
	
}