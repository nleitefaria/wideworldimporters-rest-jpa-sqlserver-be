package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.InvoiceLinesDTO;
import com.mycompany.myapp.service.InvoiceLinesService;
import com.mycompany.myapp.service.impl.InvoiceLinesServiceImpl;

@Path("/invoicelines")
public class InvoiceLinesRWS 
{
	InvoiceLinesService invoiceLinesService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		invoiceLinesService = new InvoiceLinesServiceImpl();
		return Response.status(200).entity(Integer.toString(invoiceLinesService.count())).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		invoiceLinesService = new InvoiceLinesServiceImpl();	
		InvoiceLinesDTO invoiceLinesDTO = invoiceLinesService.findInvoiceLine(Integer.parseInt(id));
		if(invoiceLinesDTO != null)
		{
			return Response.status(200).entity(invoiceLinesDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public Response findAll()
	{		
		invoiceLinesService = new InvoiceLinesServiceImpl();
		return Response.status(200).entity(invoiceLinesService.findInvoiceLinesEntities()).build();
	}
	
	@GET
	@Path("/{maxResults}/{firstResult}")
	@Produces("application/json")
	public Response findAllWithPaging(@PathParam("maxResults") String maxResults, @PathParam("firstResult") String firstResult)
	{		
		invoiceLinesService = new InvoiceLinesServiceImpl();	
		return Response.status(200).entity(invoiceLinesService.findInvoiceLinesEntitiesWithPaging(new Integer(maxResults), new Integer(firstResult))).build();
	}
	
}
