package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.SpecialDealsDTO;
import com.mycompany.myapp.service.SpecialDealsService;
import com.mycompany.myapp.service.impl.SpecialDealsServiceImpl;

@Path("/specialdeals")
public class SpecialDealsRWS 
{
	SpecialDealsService specialDealsService;
	
	@GET
	@Path("/count")
	public Response count()
	{		
		specialDealsService = new SpecialDealsServiceImpl();		
		return Response.status(200).entity(Integer.toString(specialDealsService.getSpecialDealsCount())).build();
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public Response findAll()
	{		
		specialDealsService = new SpecialDealsServiceImpl();
		return Response.status(200).entity(specialDealsService.findSpecialDealsEntities()).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		specialDealsService = new SpecialDealsServiceImpl();	
		SpecialDealsDTO specialDealDTO = specialDealsService.findSpecialDeals(Integer.parseInt(id));
		if(specialDealDTO != null)
		{
			return Response.status(200).entity(specialDealDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/page/{pageNo}")
	@Produces("application/json")
	public Response findAllWithPaging(@PathParam("pageNo") String pageNo)
	{		
		specialDealsService = new SpecialDealsServiceImpl();	
		return Response.status(200).entity(specialDealsService.findSpecialDealsWithPaging(new Integer(pageNo))).build();
	}
	
}
