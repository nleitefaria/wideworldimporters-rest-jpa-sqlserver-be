package com.mycompany.myapp.rws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mycompany.myapp.dto.CustomersDTO;
import com.mycompany.myapp.service.CustomersService;
import com.mycompany.myapp.service.impl.CustomersServiceImpl;

@Path("/customers")
public class CustomersRWS 
{
	CustomersService customersService;
	
	@GET
	@Path("/")
	@Produces("application/json")
	public Response findAll()
	{		
		customersService = new CustomersServiceImpl();
		return Response.status(200).entity(customersService.findCustomersEntities()).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public Response findOne(@PathParam("id") String id) 
	{
		customersService = new CustomersServiceImpl();		
		CustomersDTO customerDTO = customersService.findCustomer(Integer.parseInt(id));	
		if(customerDTO != null)
		{
			return Response.status(200).entity(customerDTO).build();			
		}
		else
		{
			return Response.status(204).entity("No entity for id: " + id).build();
		}	
	}
	
	@GET
	@Path("/count")
	public Response count()
	{		
		customersService = new CustomersServiceImpl();		
		return Response.status(200).entity(Integer.toString(customersService.count())).build();
	}

}
